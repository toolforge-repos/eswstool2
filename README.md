# README

## En español
En este directorio se encuentran (cada vez menos) desordenadas, una serie de aplicaciones basadas en [pywikibot](https://doc.wikimedia.org/pywikibot/stable/) para trabajar y transformar metadatos bibliográficos, en [Wikisource](https://wikisource.org), [Wikimedia Commons](https://commons.wikimedia.org) y [Wikidata](https://www.wikidata.org). Al día de hoy 20 de noviembre de 2024, tanto el código como los comentarios están en español, y están altamente adaptados a los formatos y convenciones de la Wikisource española, pero estoy abierto a colaborar en hacer una versión con i18n en mente. 

En este momento las aplicaciones más importantes son:

### crawler.py
Recibe una página en Commons y crea un elemento en Wikidata

### bdh.py, memoriachilena.py 
Dos ejemplos de aplicaciones de *scrapping* de bibliotecas digitales. 

### UTILS.py 
Un cajón de sastre con una cierta cantidad de funciones que manipulan los metadatos bibliográficos. 

---
Todas las aplicaciones representan una [*edición*](https://www.wikidata.org/wiki/Wikidata:WikiProject_Books) en el mismo formato, por lo que los flujos pueden unirse entre sí y generar un programa que:

1. Extraiga los metadatos de una biblioteca digital
2. Descargue el PDF o DJVU 
3. Suba el archivo a Commons llenando la plantilla:Book
4. Cree un elemento en Wikidata
5. Cree un índice en Wikisource
6. OCR

Con mínima intervención manual

## En inglés
On this folder you will find a (hopefuly decreasingly) messy series of scripts based on [pywikibot](https://doc.wikimedia.org/pywikibot/stable/), designed to work with and transform bibliographical metadata, on [Wikisource](https://wikisource.org), [Wikimedia Commons](https://commons.wikimedia.org) and [Wikidata](https://www.wikidata.org). As today, 18 of march, 2024, both the code itself and the comments are mostly in spanish, and they're highly adapted to formats and conventions of the Spanish Wikisource. I am happy to collaborate on making i18nized version of this app.

As today the biggest scripts are:

### crawler.py
It recieves a Commons file page and returns a Wikidata element

### bdh.py, memoriachilena.py 
To examples of *scrapping* tools from digital libraries. 

### UTILS.py 
Un cajón de sastre con una cierta cantidad de funciones que manipulan la información bibliográfica. 

---
*Most* apps here follow the same [*edition*](https://www.wikidata.org/wiki/Wikidata:WikiProject_Books) format, so every flow can be integrated easily and get a script that:

1. Scraps a website for the book's metadata
2. Downloads the PDF or DJVU 
3. Uploads it to Commons, filling the template:Book
4. Creates an element on Wikidata
5. Creates the Wikisource Index
6. OCR

With minimal manual intervention