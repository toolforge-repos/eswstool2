#Las cortes de la muerte
<poem>
{|
'''Sale el que hace la figura del TIEMPO, con el mismo vestido que ha de salir al auto, y representa.'''
 {{Pt|TIEMPO:|	
   Por las cumbres de los montes, 	
derramando blanco aljófar, 	
viene el alba dando nuevas 	
que sale el sol de las ondas. 	
Ya se descubren los campos: 	
montes son los que antes sombras; 	
donde ellas no aparecían 	
ya se ven cavernas hondas. 	
Ya cantan los pajarillos 	
saliendo de entre las hojas; 	
las aguas que susurraban, 	
al parecer ya son sordas. 	}}
|}
</poem><poem>
{|
{{Pt|TIEMPO:|
Cuál y cuál estrella queda, 	
vanse escondiendo las otras, 	
y sin luz, aunque están cerca 	
los rayos de quien la toman. 	
A los montes del Poniente 	
las puntas más altas dora 	
quien por los montes frondosos 	
poco a poco alegre asoma. 	
Ya de los húmidos troncos 	
se distinguen las personas; 	
que pastores, mal despiertos, 	
saliendo van de las chozas. 	
Vanse a las hierbas las vacas 	
ya sus cuevas las leonas; 	
agora descansan éstas, 	
aquéllas pasan agora. 	
Dejan los húmidos peces 	
sus cavernas peñascosas; 	
cortan el agua, buscando 	
sustento, abiertas las bocas.}}
|}
</poem>#Las cortes de la muerte
<poem>
{|
'''Sale el que hace la figura del TIEMPO, con el mismo vestido que ha de salir al auto, y representa.'''
 {{Pt|TIEMPO:|	
   Por las cumbres de los montes, 	
derramando blanco aljófar, 	
viene el alba dando nuevas 	
que sale el sol de las ondas. 	
Ya se descubren los campos: 	
montes son los que antes sombras; 	
donde ellas no aparecían 	
ya se ven cavernas hondas. 	
Ya cantan los pajarillos 	
saliendo de entre las hojas; 	
las aguas que susurraban, 	
al parecer ya son sordas. 	}}
|}
</poem><poem>
{|
{{Pt|TIEMPO:|
Cuál y cuál estrella queda, 	
vanse escondiendo las otras, 	
y sin luz, aunque están cerca 	
los rayos de quien la toman. 	
A los montes del Poniente 	
las puntas más altas dora 	
quien por los montes frondosos 	
poco a poco alegre asoma. 	
Ya de los húmidos troncos 	
se distinguen las personas; 	
que pastores, mal despiertos, 	
saliendo van de las chozas. 	
Vanse a las hierbas las vacas 	
ya sus cuevas las leonas; 	
agora descansan éstas, 	
aquéllas pasan agora. 	
Dejan los húmidos peces 	
sus cavernas peñascosas; 	
cortan el agua, buscando 	
sustento, abiertas las bocas.}}
|}
</poem>#Las cortes de la muerte/Acto II
<poem>
{|
{{Pt|TIEMPO:|
Dejan los hombres sus lechos; 	
cuál trabaja, cuál negocia, 	
cuál con cuidadosas ansias 	
y cuál con ansias devotas. 	
Va midiendo el sol los cielos 	
con carrera presurosa, 	
mientras más sube, más quema, 	
sombras crecen y se acortan. 	
Vase acabando la tarde; 	
vanse acabando las horas; 	
el día acaba, que el Tiempo 	
acaba todas las cosas. 	
. . . . . . . . . . . . . . . . . . . 	}}
|}
</poem><poem>
{|
{{Pt|TIEMPO:|
   El gran tesoro de Creso, 	
de Alejandro las victorias, 	
la gran armada de Jerjes, 	
larga en gente, en dicha corta; 	
las invenciones de Ulises, 	
de Nerón las fuerzas locas, 	
las liviandades de Numa, 	
de Julio César la pompa, 	
los Tolomeos de Egipto, 	
Filipo de Macedonia. 	
los romanos Escipiones, 	
las invictas Amazonas, 	
el sepulcro de Artemisa. 	
los huertos de Babilonia, 	
las imágenes de Frigia, 	
el rico templo de Jonia, 	
las pirámides de Egipto, 	
el gran coloso de Rodas, 	
el obelisco de Armenia, 	
el Faro, torre copiosa; 	}}
|}
</poem>— 3 ADVERTENCIA DE LOS EDITORES.
— 8 INTRODUCCION.
— 15 DISCURSO I.
5 19 CAPITULO II.
10 24 CAPITULO III.
13 27 CAPITULO IV.
— 3 ADVERTENCIA DE LOS EDITORES.
— 8 INTRODUCCION.
— 3 ADVERTENCIA DE LOS EDITORES.
— 8 INTRODUCCION.
