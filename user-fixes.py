# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import pywikibot, re

DICT = {}

def jeograbot(match):
    global DICT

    text = match.string
    grup = match.group(1)
    f = open('JEOGRABOT', 'a', encoding='utf8')
    lugares = re.findall(r"'''(.*?)'''", grup)    
    calific = re.findall(r"\((.*?)\)", grup)

    out = '{{anclaje'
    
    for lugar in lugares:
        DICT[lugar] = DICT.get(lugar,0)+1

        if DICT[lugar] == 1:
            out = out + '|' + lugar  #lugar solo, solo primerizos
        if calific[-1][0] != "'":
            out = out + '|' + calific[-1]+' '+lugar  #calififactivo lugar
        #pywikibot.output('boop')
    out += "}}"
    
    if len(out) == 11 :
            out = ''    
#    f.write(out)
    return out + grup 
#    return grup

def jeograbot2(match):
    text = match.string
    grup = match.group(1)
    f = open('JEOGRABOT', 'a', encoding='utf8')
#    pywikibot.output('boop')
#    f.write(match.group(1)+'\n')
    out = re.sub(r"'''(.*?)'''", r'{{enlaceDJC|\1}}',grup)
    
    return out

def f1(match):
	text = match.string
	start = match.start()
	end = match.end()
	minus = min(start,50) #Don't go under zero
	pywikibot.output(text + r'%s' % 
	    (match.group(1)))
	choice = pywikibot.inputChoice(
	        match.group(1) + '?',
	        ['Yes', 'No'], ['y', 'N'], 'y')
	if choice == 'y':
	    return r'{{categoría de autor|'+match.group(1)+r'}}'
	else:
	    return '{{categoría de autor|'+input()+'}}'
def f2(match):
	text = match.string
	pywikibot.output(text)
	return '{{categoría de autor|'+input('\n')+'}}'
def brechi(match):
	text = match.group(1)
	return ':'*int(text)
def brechi2(match):
	text = match.group(1)
	return '</noinclude><poem>'+(':'*int(text))
def brechi3(match):
	text = match.group(1)
	t = text.split('}}')
	return '::'*(len(t)-1)
def brechi4(match):
	text = match.group(1)
	t = text.split('}}')
	return '</noinclude><poem>'+('::'*(len(t)-1))
def brechi5(match):
	text = match.group(1)
	t = text.split('}}')
	return '<poem>'+('::'*(len(t)-1))
def brechi6(match):
	text = match.group(1)
	return '<poem>'+(':'*int(text))
def humb(match):
    text = match.string
    n = match.group(1)
    t = int(match.group(1))%100
    if n==0:
        n=100
    pywikibot.output(text)
    return '{{ref|'+n+'|('+str(t)+')}}'


fixes['catdoc'] = {
    'regex': True,
    'nocase': False,
    'dotall': True,
    'recursive': False,
    'msg': {
        '_default': 'Bot - Añadiendo plantilla a categorías',
    },
    'replacements': [
        #(r'.*\|(.*?)\]\].*', f1),
        (r'.+', f2),
    ]
}

fixes['scannos'] = {
    'regex': True,
    'nocase': False,
    'dotall': True,
    'recursive': False,
    'msg': {
        '_default': 'Bot - Corrección de "scannos" comunes',
    },
    'replacements': [
        (r'(secci[óo]n) (\d+)a', r'\1 \2ª'), #seccion 1a
        (r'(\d+)a (secci[óo]n|serie)', r'\1ª \2'),
        (r'(culo|rt\.) (\d+)o', r'\1 \2º'), #artículo 1o
        (r'de(?=1\d\d\d)', r'de '),
        (r'(\d+)o(?= de (?:[Ee]nero|[Ff]ebrero|[Mm]arzo|[Aa]bril|[Mm]ayo|[Jj]u[nl]io|[Aa]gosto|[Ss]ep?tiembre|[Oo]c?tubre|[Nn]oviembre|[Dd]iciembre))', r'\1º'), #artículo 1o
        (r' 1(as*|os*) ', r' l\1 '),
        (r' (d?e)1 ', r' \1l '),
        (r'(á|é)1s', r'\1is'), #áis, éis

        (r'([aeiouáéíóúüAEIOU])1n(?=[bp])', r'\1m'),        

# MODO EXPLORER
#        (r'(?<!s|part)([a-z])1(?!\d)', r'\1l'),
        (r'(?<=[A-ZÑ])1(?=[A-ZÑ])', r'I'),
        (r'([aeiouáéíóúüAEIOUl])1(?!\d)(?=\w)', r'\1l'),
    ],
    'exceptions': {
            'inside-tags': [
                'comment',
                'nowiki',
                'hyperlink',
                'link',
                'pages',
#                'div',
#                'poem',
                'table',
                'ref',
#                'div',
                'template'
                ],

        },
}
fixes['brecha'] = {
    'regex': True,
    'nocase': False,
    'multiline': True,
    'recursive': True,
    'msg': {
        '_default': 'Bot - Desbrechizando poemas',
    },
    'replacements': [
        #(r'<br */? *>\n<br */? *>$', '\n\n'),
        #(r'<br */? *>\n\s*$', '\n\n'),
        #(r'<br */? *>\n', '\n\n'), 
        #(r'</noinclude>{{(?:brecha|gap)}}\s*(?!{{(brecha|gap))', '</noinclude>'), 
        #(r'^{{(?:brecha|gap)}}\s*(?!{{(brecha|gap))', ''), 
        #(r'^{{(?:brecha|gap)}}\s*{{(?:brecha|gap)}}', '{{brecha}}'),
        # (r'{{--}}', '—'),
        # (r'{{SUBPAGENAME}}', '{{subst:SUBPAGENAME}}'),
        # (r'</noinclude><poem>((?:{{(?:[Bb]recha|[Gg]ap)}})+)', brechi4),        
        # (r'</noinclude><poem>{{(?:[Bb]recha|[Gg]ap)\|(\d)em}}', brechi2), 
        # (r'^((?:{{(?:[Bb]recha|[Gg]ap)}})+)', brechi3),
        # (r'^{{(?:[Bb]recha|[Gg]ap)\|(\d)em}}', brechi),
        # (r'<poem>((?:{{(?:[Bb]recha|[Gg]ap)}})+)', brechi5),        
        # (r'<poem>{{(?:[Bb]recha|[Gg]ap)\|(\d)em}}', brechi6), 
    	(r'<ref>{{brecha}}', '<ref>'),
    ],
    'exceptions': {
            'inside-tags': [
                'nowiki',
#                'poem',
                'table',
                'div',
#                'template'
                ],

        }
}
fixes['menor'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'msg': {
        '_default': 'Bot - [[Especial:Errores_de_sintaxis/misc-tidy-replacement-issues|Arreglando Tidy]]',
    },
    'replacements': [

        (r'{{menor\|{{(?:[Cc][Pp]|[Rr][Hh])\|(.*?)}}}}', r'{{cp|\1|menor=sí}}'), 

    ],
    'exceptions': {
            'inside-tags': [
                'nowiki',
                'poem',
                'table',
                'div'
                ],

        }
}
fixes['cajita'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'dotall': True,
    'msg': {
        '_default': 'Bot - cambio de formato',
    },
    'replacements': [

        (r'(\d+)\s*[—\-]<noinclude>', r'<noinclude>\1 —'), 
        (r'[—\-]\s*(\d+)<noinclude>', r'<noinclude>{{d|— \1}}'),
        (r' /></noinclude>', r' /></noinclude><poem>'),
        (r'(.)<noinclude>', r'\1\n</poem><noinclude>'),
        (r'<br ?/>', r''),

    ],
    'exceptions': {
        }
}
fixes['voces'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'dotall': True,
    'multiline': True,
    'msg': {
        '_default': 'Bot - añadiendo anclajes',
    },
    'replacements': [

        (r"^'''([\w ]+)'''", r"'''{{anclaje+|\1}}'''"),
        (r"(<section begin=\".\"/>)'''([\w ]+)'''", r"\1'''{{anclaje+|\2}}'''"),        
        (r"(</noinclude>)'''([\w ]+)'''", r"\1'''{{anclaje+|\2}}'''"),

    ],
    'exceptions': {
        }
}
fixes['humboldt'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'dotall': True,
    'msg': {
        '_default': 'Bot - cambio de formato',
    },
    'replacements': [
        (r'<ref follow="?nota(\d+)"?>\((\d+)\)(.*?)</ref>', r'{{nota|\1|(\2)}}\3'),
        (r'<ref name="?nota(\d+)"?> *{{zw}} *</ref>', humb),
        (r' *<!-+[\s\n\*]*-+> *','\n\n'),
        (r'<ref follow="?nota\d+"?>(.*?)</ref>', r'\1'),


    ],
    'exceptions': {
        }
}
fixes['goog'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'dotall': True,
    'msg': {
        '_default': 'Bot - reemplazo de plantilla',
    },
    'replacements': [
        (r'({{[Ee]ncabe(?:zado)?(?:{{[^}]*?}}|[^}])*)}}(.*?){{[Cc]apítulos\s*\|\s*(\[\[[^\|]+\|[^\|]+\]\]|[^\[\]\|]+|\[\[[^\|]+\]\]|)\s*\|\s*(\[\[[^\|]+\|[^\|]+\]\]|[^\[\]\|]+|\[\[[^\|]+\]\]|)\s*\|\s*(\[\[[^\|]+\|[^\|]+\]\]|[^\[\]\|]+|\[\[[^\|]+\]\]|)\s*}}', r'\1\n|anterior=\3\n|sección=\4\n|próximo=\5}}\n\2'),



    ],  
    'exceptions': {
        }
}
fixes['jeogr'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'dotall': False,
    'msg': {
        '_default': 'Bot - Ajustes en Diccionario Jeográfico',
    },
    'replacements': [
        (r"(?:(?<=^)|(?<=>))('''[^\n,]*?\([^\n,]*?\))", jeograbot),
        (r"(Véase [^\n{]*?\.)", jeograbot2),


    ],  
    'exceptions': {
        }
}
fixes['blank'] = {
    'regex': True,
    'nocase': False,
    'recursive': False,
    'dotall': True,
    'msg': {
        '_default': 'Bot - NO DEJAR ASÍ',
    },
    'replacements': [
        (r' ', r' '),



    ],  
    'exceptions': {
        }
}