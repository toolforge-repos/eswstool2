from .bdcyl import bdcyl


class bdandalucia(bdcyl):
    URL = "https://www.bibliotecadigitaldeandalucia.es/catalogo/es/consulta/registro.do?id="
    PREFIX = ''
    SOURCE = "{{{{BDAndalucía|1={}}}}}"
    CODE = "bda"

    REXP = r"{{ *(?:b[vd]andalucía) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"
    BLACKLIST = {}
    IDBLACKLIST = {}
    FULLNAME = 'Biblioteca Digital de Andalucía'
    TEMPLATE = 'BDAndalucía'
