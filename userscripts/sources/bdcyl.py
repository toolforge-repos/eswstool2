import re
from .source import Source
import requests
from bs4 import BeautifulSoup
import Data
from utils.string_utils import bad_chars


class bdcyl(Source):
    URL = "https://bibliotecadigital.jcyl.es/es/consulta/registro.do?id="
    PREFIX = 'https://bibliotecadigital.jcyl.es/es'
    SOURCE = '{{{{BDCYL|1={}}}}}'
    CODE = 'bdcyl'
    TEMPLATE = 'BDCYL'

    def __init__(self, ID):
        self.ID = ID
        self.URL += ID
        self.SOURCE = self.SOURCE.format(self.URL)

    def do(self):
        D = self.get_dict()

        if not self.PDSTATUS:
            return

        E = Data.Edition(data=D)

        E.process_data()

        title = bad_chars(D['title'])[:150]
        an = D["accession_number"]

        self.filename = f'{title} - {self.CODE}{an}.{self.FMT}'

        pages = []
        if len(self._PDFURL) == 1:
            self.PDFURL = self._PDFURL[0]
            paths = self.download()
            _, filename = self.upload(
                E, paths[0], self.filename, query=self.CODE + self.ID)
            if filename:
                E.filename = filename
                pages.append(E)
        elif len(self._PDFURL) > 1:
            for i in range(len(self._PDFURL)):
                copy = E.copy()
                copy["volume"] = str(i + 1)
                filename = re.sub(
                    r"(?: - Volumen \d+)?\.pdf",
                    f" - Volumen {i+1}.pdf", self.filename)

                self.PDFURL = self._PDFURL[i]
                paths = self.download(volume=i + 1)

                _, filename = self.upload(
                    copy, paths[0], filename, query=self.CODE + self.ID)

                if not filename:
                    continue

                copy.filename = filename
                pages.append(copy)
        return pages

    def get_dict(self):
        def get_bdcyl_prop(soup, text):
            child_soup = soup.find_all('span', class_="etiqueta")
            sibling = ''
            for t in child_soup:
                t.string = t.string or ''
                if t.string.strip() == text:
                    sibling = t.find_next()
                    if sibling.a:
                        sibling = sibling.a
            return sibling and ' '.join(sibling.text.split())

        def get_bdcyl_props_a(soup, text):
            child_soup = soup.find_all('span', class_="etiqueta")
            sibling = []
            for t in child_soup:
                t.string = t.string or ''
                if t.string.strip() == text:
                    sibling = t.find_next().find_all('a')
            return [a.text for a in sibling if a.text]

        def get_bdcyl_pdf(soup):
            t = soup.find_all('a', attrs={'data-modal': "0"})
            if t:
                return sorted(list({self.PREFIX + a['href'].strip('.') for a in t}))
            else:
                return []

        d = {}
        page = requests.get(self.URL,
                            headers={'User-agent': 'SpanishWikisourceBot 0.1'})
        soup = BeautifulSoup(page.content, "html.parser")
        d["accession_number"] = self.ID
        title = re.sub(r"\[[\w ]+\]", "", get_bdcyl_prop(soup, 'Título:'))
        d["title"] = title.split("/")[0].strip()

        autor = get_bdcyl_props_a(soup, 'Autor:')
        autores_sec = get_bdcyl_props_a(soup, 'Autores secundarios:')
        autores = autor + autores_sec

        d["author"] = '\n'.join(autores)

        d["publisher"] = get_bdcyl_prop(soup, 'Publicación:')
        d["edition"] = get_bdcyl_prop(soup, 'Edición:')
        d["date"] = ''
        if m := re.search(r", (\d\d\d\d)", d["publisher"]):
            d["date"] = m[1]
            d['publisher'] = d['publisher'][:-6]

        d["description"] = get_bdcyl_prop(soup, 'Descripción física:')

        pdstat = get_bdcyl_prop(soup, 'Derechos:')
        if 'no se encuentra' not in pdstat:
            self.PDSTATUS = True

        self._PDFURL = get_bdcyl_pdf(soup)

        d['source'] = self.SOURCE

        return d
