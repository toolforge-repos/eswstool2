from .bdcyl import bdcyl


class bivaldi(bdcyl):
    URL = "https://bivaldi.gva.es/es/consulta/registro.cmd?id="
    PREFIX = 'https://bivaldi.gva.es/es'
    SOURCE = "{{{{BIVALDI|1={}}}}}"
    CODE = "bivaldi"

    REXP = r"{{ *(?:bivaldi) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"
    BLACKLIST = {}
    IDBLACKLIST = {}
    FULLNAME = 'Biblioteca Valenciana Digital'
