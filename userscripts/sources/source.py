import re
import Data
from utils.string_utils import bad_chars
from utils.file_utils import download_pdf_file, upload_to_commons
import pywikibot
from pywikibot import Page, Site


class Source:
    # Source defaults
    FMT = 'pdf'
    PDSTATUS = False

    def __init__(self, ID=None, URL=None, PDFURL=None, CODE=None):
        self.ID = ID
        self.URL = URL
        self.PDFURL = PDFURL
        self.CODE = CODE

    def get_dict(self):
        raise NotImplementedError(
            'Cada módulo requiere como mínimo la funcion get_dict()')

    def get_desc(self, E):

        CATS = E.get_dict_cats()
        commonsText = f'''== {{{{int:filedesc}}}} ==

{E.to_commons()}

== {{{{int:license-header}}}} ==
{{{{PD-scan|PD-old-70-expired}}}}

[[Category:PDF books in Spanish]]
{CATS}
'''
        return commonsText

    def download(self, verify=True, vol=None):
        pdfurl = self.PDFURL.format(self.ID)
        filename = self.CODE + self.ID
        if vol:
            filename += f'vol{vol}'
        filename += f'.{self.FMT}'

        filepath = download_pdf_file(
            pdfurl, filename, verify=verify)
        return [filepath]

    def upload(self, E, filepath, filename, query=None):
        if not query:
            query = self.ID

        filepage = Page(Site('commons', 'commons'), 'File:' + filename)

        if filepage.exists():
            pywikibot.info(f'{filename} ya existe en Commons. Saltando.')
            return filepath, filename

        if not filepath:
            return None, None

        commonsText = self.get_desc(E)

        filename = upload_to_commons(
            filepath, filename, commonsText, query=query)

        return filepath, filename

    def do(self):
        D = self.get_dict()
        
        if not self.PDSTATUS:
            return

        E = Data.Edition(data=D)

        E.process_data()

        title = bad_chars(D['title'])[:150]
        an = D["accession_number"]

        filename = f'{title} - {self.CODE}{an}.{self.FMT}'

        paths = self.download()
        if not paths:
            return []

        pages = []

        if len(paths) == 1:
            _, filename = self.upload(E, paths[0], filename)
            if filename:
                E.filename = filename
                pages.append(E)

        elif len(paths) > 1:
            for i in range(len(paths)):
                copy = E.copy()
                copy["volume"] = i + 1

                filename = re.sub(r"(?: - Volumen \d+)?\.pdf",
                                  f" - Volumen {i+1}.pdf", filename)
                _, filename = self.upload(E, paths[i], filename)
                if filename:
                    copy.filename = filename
                    pages.append(copy)

        return pages
