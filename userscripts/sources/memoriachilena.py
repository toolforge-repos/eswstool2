import re
from .source import Source
import requests
from bs4 import BeautifulSoup


class memoriachilena(Source):

    REXP = r"{{ *(?:[Mm]emoria [Cc]hilena) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"
    BLACKLIST = {}
    IDBLACKLIST = {'67187', '67189', '8373',
                   # stashfailed
                   '336408', '8313', '600735'}
    FULLNAME = 'Memoria Chilena'
    TEMPLATE = 'Memoria Chilena'

    def __init__(self, ID):
        self.ID = ID
        self._URL = "https://www.memoriachilena.gob.cl/602/w3-article-{}.html"
        self.URL = self._URL.format(self.ID)
        self.PDFURL = "https://www.memoriachilena.gob.cl/archivos2/pdfs/{}.pdf"
        self.SOURCE = f'{{{{IAI|{self.ID}}}}}'
        self.CODE = ''
        self.FMT = 'pdf'

    def get_dict(self):
        def get_title(soup):
            t = soup.find("h1", class_="titulo")
            return t.string

        def span(soup, clase):
            t = soup.find_all("span", class_=clase)
            return '\n'.join(list({i.string for i in t}))

        def div(soup, clase):
            t = soup.find_all("div", class_=clase)
            for i in t:
                return '\n'.join(list({p.text for p in i.find_all("p")}))
            return ''

        def div_nop(soup, clase):
            t = soup.find("div", class_=clase)
            if t:
                return t.string.split(':', 1)[-1].strip()

        def get_vol(s):
            if m := re.search(r'(?:Tomo|Volumen|Vol.?) ([IVXLC]+|\d+)', s):
                return m[1]
            return ''

        d = {}
        page = requests.get(self.URL)
        soup = BeautifulSoup(page.content, "html.parser")

        download = span(soup, "descargar_recurso")
        if not download:
            return

        d["title"] = get_title(soup)
        d['subtitle'] = div_nop(soup, 'pnid-637')

        aut = span(soup, "pnid-551")
        autsec = span(soup, "pnid-635")
        d["author"] = f'{aut}\n{autsec}'.strip()

        # NO BORRAR d["propiedad"] = span(soup, "pnid-571")

        d["description"] = div(soup, "pnid-654").strip()
        d["volume"] = get_vol(d["description"])
        d["publisher"] = div(soup, "pnid-655").strip()

        d["publisher"] = re.sub(r", \d+", "", d["publisher"])

        d["date"] = span(soup, "pnid-559")

        DAYEXP = r'0?(\d\d? de (?:enero|febrero|marzo|abril|mayo|junio|julio|agosto|sep?tiembre|octubre|noviembre|diciembre) de \d+)'
        if m := re.search(DAYEXP, d['title'], flags=re.I):
            d["date"] = m[1]
        
        if m := re.search(r'n[úu]mero (\d+)', d['title'], flags=re.I):
            d["num"] = m[1]

        d["accession_number"] = span(soup, "pnid-529")
        self.PDFURL = self.PDFURL.format(d['accession_number'])

        d["source"] = f'{{{{Memoria Chilena|1={self.URL}}}}}'
        d['wikisource'] = r's:es:Índice:{{PAGENAME}}'

        pdstat = span(soup, "pnid-571")

        self.PDSTATUS = not pdstat or pdstat == 'Patrimonio cultural común'

        return d
