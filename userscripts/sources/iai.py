from .source import Source
import requests
from bs4 import BeautifulSoup
from utils.pdf_utils import PDF as PDF

BASEURL = 'https://digital.iai.spk-berlin.de/viewer'


class iai(Source):
    REXP = r"{{\s*(?:iai)\s*\|(?: *id *= *)?\s*(.+?)\s*(\|.+?|) *}}"
    BLACKLIST = {}
    IDBLACKLIST = {
        # uploadstash-file-not-found
        '812469658', }
    FULLNAME = 'Instituto Ibero-Americano'
    TEMPLATE = 'IAI'

    def __init__(self, ID):
        self.ID = ID
        self.URL = BASEURL + '/image/{}'
        self.PDFURL = BASEURL + '/api/v1/records/{}/sections/LOG_0000/pdf/'
        self.SOURCE = f'{{{{IAI|{self.ID}}}}}'
        self.CODE = 'iai'
        self.FMT = 'pdf'

    def download(self):
        pages = super().download()

        if not pages[0]:
            return []
        pdf = PDF(pages[0])
        pdf.crop(bottom=12)

        return [pages[0][:-4] + "_crop.pdf"]

    def get_dict(self):
        def get_title(soup):
            t = soup.find("h1", class_="titulo")
            return t.string

        def get_meta(soup, clase):
            t = soup.find_all("meta", attrs={'name': clase})
            return '\n'.join(list({i['content'] for i in t}))

        d = {}
        url = self.URL.format(self.ID)
        page = requests.get(url)
        soup = BeautifulSoup(page.content, "html.parser")

        d["title"] = get_meta(soup, 'citation_title')
        d["author"] = get_meta(soup, 'citation_author')
        d["date"] = get_meta(soup, 'citation_publication_date')
        d["language"] = get_meta(soup, 'citation_language')
        d["publisher"] = get_meta(soup, 'DC.publisher')
        d["source"] = f'{{{{IAI|{self.ID}}}}}'
        d['wikisource'] = r's:es:Índice:{{PAGENAME}}'
        d["accession_number"] = self.ID

        self.PDSTATUS = True  # TODO: sacarlo realmente

        return d

    def get_desc(self, E):

        CATS = E.get_dict_cats()

        commonsText = f'''== {{{{int:filedesc}}}} ==

{E.to_commons()}

== {{{{int:license-header}}}} ==
{{{{PD-old-70-expired}}}}

[[Category:PDF books in Spanish]]
{CATS}
'''
        return commonsText
