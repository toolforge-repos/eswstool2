from .bdcyl import bdcyl


class bvpb(bdcyl):
    URL = "https://bvpb.mcu.es/es/consulta/registro.do?id="
    PREFIX = "https://bvpb.mcu.es/es"
    SOURCE = "{{{{BVPB|1={}}}}}"
    CODE = "bvpb"

    REXP = r"{{ *(?:bvpb) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"
    BLACKLIST = {}
    IDBLACKLIST = {
        # uploadstash-file-not-found
        '403724'}
    MODULE = BVPB
    FULLNAME = 'Biblioteca Virtual del Patrimonio Bibliográfico'
    TEMPLATE = 'BVPB'
