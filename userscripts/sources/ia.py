import re
from .source import Source
import requests
import internetarchive
import logging

from lxml import etree
from pywikibot.textlib import removeHTMLParts
import os
import utils.pagelist
from utils.string_utils import clean_codecs


class ia(Source):

    REXP = r"{{ *(?:ia|internet archive link|internet archive) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"
    BLACKLIST = {'Autor:Martín Fernández de Navarrete',
                 'Autor:Felisberto Hernández',
                 'Autor:Juan Eusebio Nieremberg',
                 'Autor:Carlos E. Porter',
                 'Portal:Economía', }
    IDBLACKLIST = {  # no recuerdo por qué
        'los-aborijenes-de-chile-jose-toribio-medina-1882',
        'rosettaproject_kuz_vocab-1',
        'lasantabiblia00migugoog',
        'manualpaleografia00riveuoft', 'mujeresdeamrica00soiz',

        # stashfailed: No chunked upload session with this key.
        'obrascompletasyc19ameguoft', 'A253315',
        'tesorodelalengua00covauoft',

        # uploadstash-file-not-found
        'generalynatural01fernrich',

        # copyrighted
        'b29827620_0001', 'b29827620_0002', 'b29827620_0003',
        'b29827620_0004', 'b29827620_0005',
        'gayanaoceanologi01univ',
        # en otro idioma
        'lagrceternel00gmuoft',  # fr
        'inheartoftragedy00gm',  # en
        # en lenguaje pictórico testerino
        'catecismotesteri02cath', 'catecismotesteri01cath',

    }
    FULLNAME = 'Internet Archive'
    TEMPLATE = 'Internet archive'

    def __init__(self, ID):
        self.ID = self._normalise_id(ID)
        self.filelist = None
        self.prefer_pdf = False

        self.image_page = 1
        self.offset = 0

        self._metadata_cache = None
        self._scandata_cache = None
        self._pagelist_cache = None

        self.item = internetarchive.get_item(self.ID)

        self.SOURCE = f'{{{{IA|{self.ID}}}}}'
        self.CODE = 'IA'

    def exists(self):
        return 'files' in self.item.item_metadata

    def _get_download_url(self, filename):

        url = "https://archive.org/download/{iaid}/{fn}".format(
            iaid=self.ID, fn=filename)
        return url

    def _get_files_with_format(self, fmt):

        filelist = self.item.files

        files = []
        for file in filelist:
            if file.get('format') == fmt:
                files.append(file)

        return files

    def _get_best_file(self):

        djvus = self._get_files_with_format("DjVu")
        pdfs = self._get_files_with_format(
            "Text PDF") + self._get_files_with_format("Image Container PDF")

        if pdfs and (not djvus or self.prefer_pdf):
            return pdfs[0]['name']

        if djvus:
            return djvus[0]['name']

        return None

    def get_file_url(self):

        logging.debug(f"Getting IA source URL for ID {self.ID}")

        filename = self._get_best_file()

        if filename is not None:
            url = self._get_download_url(filename)
        else:
            url = None

        return url

    def _get_file_url(self, fmt):
        fileinfo = self._get_files_with_format(fmt)

        if fileinfo is None or len(fileinfo) == 0:
            return None, None

        name = fileinfo[0]['name']
        url = self._get_download_url(name)

        return url, fileinfo

    def __get_file_url(self, fmt):
        url, _ = self._get_file_url(fmt)
        return url

    def get_jp2_zip_name(self):
        _, fileinfo = self._get_file_url("Single Page Processed JP2 TAR")
        return fileinfo[0]['name']

    def get_jp2_list(self):
        urls = []

        zip_name = self.get_jp2_zip_name()
        zip_head, _ = os.path.splitext(zip_name)
        image_prefix = re.sub(r'_jp2$', '', zip_head)

        indexes = self.get_all_output_file_indexes()

        # this is a hack: can we get it from the API?
        for index in indexes:
            url = f'https://archive.org/download/{self.ID}/' + \
                f'{zip_name}/{zip_head}%2F{image_prefix}_{index:04}.jp2'

            urls.append({
                'url': url,
                'index': index,
                'ext': '.jp2'
            })
        return urls

    def get_jpg_list(self):

        # Just defer to the JP2 function
        urls = self.get_jp2_list()

        def transform(jp2_url_item):
            newurl = jp2_url_item.copy()
            newurl['url'] += f'&ext=jpg'
            newurl['ext'] = '.jpg'
            return newurl

        jpg_urls = [transform(url) for url in urls]

        return jpg_urls

    def get_all_output_file_indexes(self):

        sd = self._get_scandata()

        indexes = []

        for page in sd.findall('.//{*}pageData/{*}page'):
            if self.page_in_accessformats(page):
                index = int(page.attrib['leafNum'])
                indexes.append(index)

        return indexes

    def _get_json(self):
        json_name = self._get_files_with_format("Page Numbers JSON")

        if not json_name:
            return

        json_name = json_name[0]['name']
        sdurl = self._get_download_url(json_name)

        try:
            r = requests.get(sdurl)
            r.raise_for_status()
        except Exception:
            return

        return r.json()

    def _get_scandata(self):

        if self._scandata_cache is not None:
            return self._scandata_cache

        scandata_name = self._get_files_with_format("Scandata")

        if scandata_name:
            scandata_name = scandata_name[0]['name']
        else:

            scandata_name = self._get_files_with_format("Scribe Scandata ZIP")

            if scandata_name:
                scandata_name = scandata_name[0]['name'] + \
                    "/scandata.xml"

        logging.debug("Scan data found: {}".format(scandata_name))
        sdurl = self._get_download_url(scandata_name)

        r = requests.get(sdurl)

        r.raise_for_status()
        # print(r.content)

        xml = etree.fromstring(r.content)

        self._scandata_cache = xml

        return self._scandata_cache

    @staticmethod
    def page_in_accessformats(page):
        add_page = page.find(".{*}addToAccessFormats")

        if add_page is not None and add_page.text.lower() == "false":
            return False

        return True

    def get_pagelist(self):

        if self._pagelist_cache:
            return self._pagelist_cache

        try:
            logging.debug("Getting IA pagelist for ID {}".format(self.ID))
            pl = utils.pagelist.PageList()

            json = self._get_json()
            xml = self._get_scandata()

            pages = xml.findall(".//{*}pageData/{*}page")

            if json and len(pages) != len(json["pages"]):
                json = False

            n = 1
            for pg in pages:
                if not self.page_in_accessformats(pg):
                    continue
                pageTypeE = pg.find(".{*}pageType")
                pn = ""
                if pageTypeE is not None:

                    if pageTypeE.text in ["Title", "Title Page"]:
                        pn = "Portada"
                        self.image_page = int(n)
                    elif pageTypeE.text in ["Cover"]:
                        pn = "-"
                    elif pageTypeE.text in ["Contents"]:
                        pn = "Índice"

                if not pn and json:
                    pn = json["pages"][int(n) - 1]["pageNumber"]

                if not pn:
                    pageNumberE = pg.find(".{*}pageNumber")
                    if pageNumberE is None or pageNumberE.text is None:

                        pn = "-"
                    else:
                        pn = pageNumberE.text

                pl.append(pn)
                n += 1
            self._pagelist_cache = pl
            return pl
        except:
            return pl

    def _get_metadata(self):
        meta = self.item.metadata

        for k, v in meta.items():
            if isinstance(v, list):
                meta[k] = '\n'.join(v)

        return meta

    def get_dict(self):
        if not self.exists():
            return {}

        self.PDFURL = self.get_file_url()
        self.FMT = self.PDFURL.split('.')[-1]
        self.PDSTATUS = True  # TODO: sacarlo programáticamente

        meta = self._get_metadata()
        D = {}

        def clean_get(param):
            if x := meta.get(param):
                x = clean_codecs(x)
                x = removeHTMLParts(x, keeptags=['div'])
                return x

        D['author'] = clean_get('creator')
        D['title'] = clean_get('title')
        D['date'] = clean_get('date') or clean_get('year')
        D['description'] = clean_get('description')
        D['language'] = clean_get('language')
        D['accession_number'] = clean_get('identifier')
        D['volume'] = clean_get('volume')
        D['publisher'] = clean_get('publisher')

        # TODO: Sacar de acá, hacer todo más inteligente
        D['_sources'] = {'ia_id': self.ID}
        if olid := clean_get('openlibrary_edition'):
            D['_sources']['ol_id'] = olid

        D['source'] = f'{{{{IA|{self.ID}}}}}'
        D['wikisource'] = r's:es:Índice:{{PAGENAME}}'
        if self.image_page > 1:
            D['image_page'] = str(self.image_page)
        return {k: v for k, v in D.items() if v}

    @staticmethod
    def _normalise_id(id):

        if id.startswith("http"):

            return id.split("/")[-1]

        return id.split('/')[0]

    def get_id(self):
        return self.ID
