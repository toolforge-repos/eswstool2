from .source import Source
import requests
import Data
import re
from bs4 import BeautifulSoup
from utils.string_utils import bad_chars
import pypdf


class bdh(Source):
    URL = "https://bdh.bne.es/bnesearch/detalle/"
    CODE = ''
    FMT = 'pdf'

    REXP = r"{{\s*(?:bdh)\s*\|(?: *id *= *)?\s*(.+?)\s*(\|.+?|) *}}"
    BLACKLIST = {'Autor:Mariano Juderías',
                 'Autor:Cayetano Sixto García',
                 'Discusión:Constitución española de 1869'}
    IDBLACKLIST = {
        # PDF en otro lugar
        'bdh0000001881', 'bdh0000000742',
        # stash-failed
        'bdh0000207265', 'bdh0000102428',
        # separado en miles de PDF pequeños
        'bdh0000137653',
    }
    FULLNAME = 'Biblioteca Digital Hispánica'
    TEMPLATE = 'Biblioteca Digital Hispánica'

    def __init__(self, ID):
        self.ID = ID
        if not self.ID.startswith('bdh'):
            self.ID = 'bdh' + self.ID

        self.PDFURL = "https://bdh-rd.bne.es/high.raw?id={}&name=00000001.original.pdf"
        self.SOURCE = f'{{{{BDH|{self.ID}}}}}'

    def get_dict(self):
        def get_bdh_prop(soup, text):
            child_soup = soup.find_all('div', class_="dato")
            sibling = ''
            for t in child_soup:
                t.string = t.string or ''
                if t.string.strip() == text:
                    sibling = t.find_next()
                    if sibling.a:
                        sibling = sibling.a
            if not sibling:
                return ''
            return ' '.join(sibling.text.split())

        def get_bdh_props_a(soup, text):
            child_soup = soup.find_all('div', class_="dato")
            sibling = []
            for t in child_soup:
                t.string = t.string or ''
                if t.string.strip() == text:
                    sibling = t.find_next().find_all('a')

            return [' '.join(a.text.split()) for a in sibling]

        def get_n_vol(signatura, description):
            if m := re.search(r"^(\d+) v\.", description):
                return int(m[1])
            t = [int(x) for x in re.findall(r"[V][.][ ]*(\d+)", signatura)]
            return max([1] + t)

        d = {}

        page = requests.get(self.URL + self.ID, verify=False)
        soup = BeautifulSoup(page.content, "html.parser")

        # d["titulo uniforme"] = get_bdh_prop(soup, 'Título uniforme')
        d["title"] = re.sub(r"\[[\w ]+\]", "",
                            get_bdh_prop(soup, 'Título'))

        d["title"] = d["title"].split('/')[0].strip()

        if ";" in d["title"]:
            d["subtitle"] = d['title'].split(";", 1)[1].strip()

        if len(d["title"]) > 103:
            d["title"] = d["title"][:100] + '[...]'
        autores = get_bdh_props_a(soup, 'Autor')

        for autor in autores:
            if m := re.search(r"(.*?) - (.*)", autor):
                m, autor, role = m[0], m[1], m[2]

                if re.match(r"trad(?:uctor)?", role):
                    d['translator'] = f"{d.get('translator', '')}{autor}\n"

                elif re.match(r"ilustrador|grabador", role):
                    d['illustrator'] = f"{d.get('illustrator', '')}{autor}\n"

                elif re.match(r"ed(?:itor)?", role):
                    d['editor'] = f"{d.get('editor', '')}{autor}\n"

                elif re.match(r"prologuista", role):
                    d['introducer'] = f"{d.get('introducer', '')}{autor}\n"

                elif re.match(r"imp(?:resor)?", role):
                    d['printer'] = f"{d.get('printer', '')}{autor}\n"
                elif len(role) > 20:
                    d['author'] = f"{d.get('author', '')}{autor}\n"
                else:
                    d['author'] = f"{d.get('author', '')}{m}\n"
            else:
                d['author'] = f"{d.get('author', '')}{autor}\n"

        d["publisher"] = get_bdh_prop(soup, 'Datos de edición')
        d["publisher"] = re.sub(r'\[s\. *[ln]\.\]', '',
                                d['publisher'], flags=re.I).strip(" :()")

        d["edition"] = get_bdh_prop(soup, 'Edición')
        d["date"] = get_bdh_prop(soup, 'Fecha')
        d["description"] = '\n'.join([
            get_bdh_prop(soup, 'Descripción física'),
            get_bdh_prop(soup, 'Descripción y notas')]).strip()
        d['accession_number'] = self.ID
        signatura = get_bdh_prop(soup, 'Signatura')

        d["_n_vol"] = get_n_vol(signatura, d["description"])

        d["source"] = f'{{{{BDH|{self.ID}}}}}'
        d['wikisource'] = r's:es:Índice:{{PAGENAME}}'
        d["_PDSTATUS"] = True  # TODO: sacarlo realmente

        return d

    def get_desc(self, E):

        CATS = E.get_dict_cats()

        commonsText = f'''== {{{{int:filedesc}}}} ==

{E.to_commons()}

== {{{{int:license-header}}}} ==
{{{{PD-old-70-expired}}}}

[[Category:PDF books in Spanish]]
{CATS}
'''
        return commonsText

    def do(self):
        D = self.get_dict()
        if not D["_PDSTATUS"]:
            return

        E = Data.Edition(data=D)
        E.process_data()

        title = bad_chars(D['title'])[:150]
        an = D["accession_number"]

        self.filename = f'{title} - {an}.{self.FMT}'

        self.PDFURL = self.PDFURL.format(self.ID[3:])

        pages = []
        if D["_n_vol"] == 1:
            paths = self.download(verify=False)
            _, filename = self.upload(E, paths[0], self.filename)
            if filename:
                E.filename = filename
                pages.append(E)
        else:
            n = 1
            old_n = 1
            for i in range(1, D["_n_vol"] + 1):
                copy = E.copy()
                copy['volume'] = str(i)

                self.PDFURL = re.sub(rf"00+{old_n}\.",
                                     f"{str(n).zfill(8)}.", self.PDFURL)
                filename = re.sub(r"(?: - Volumen \d+)?\.pdf",
                                  f" - Volumen {i}.pdf", self.filename)

                paths = self.download(verify=False, volume=i)
                filepath, filename = self.upload(copy, paths[0], self.filename)

                if not filename:
                    continue
                copy.filename = filename
                pages.append(copy)

                try:
                    reader = pypdf.PdfReader(filepath)
                    old_n = n
                    n = n + len(reader.pages)
                except pypdf.errors.EmptyFileError:
                    print(
                        'El último archivo estaba vacío, saltando...')
        return pages
