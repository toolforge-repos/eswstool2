import pywikibot
import re
from pywikibot.bot import ExistingPageBot
from pywikibot.proofreadpage import IndexPage
from pywikibot import pagegenerators, Site, Page, Link, ItemPage
import utils.template_utils as t_u


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot - Enlaza',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        index = re.search(r'index *= *["\']([^"\']+)["\']', page.text)
        if not index:
            return
        index = IndexPage(Site('es', 'wikisource'), 'Índice:' + index[1])
        wikidata = t_u.get_index_data(index.text, 'Wikidata')
        if not wikidata:
            return
        title = t_u.get_index_data(index.text, 'Titulo')
        if not title or not re.fullmatch(r'\[*[^\[\]]+\]*', title):
            return

        link = Link(title.strip('[]'), Site('es', 'wikisource'))
        page2 = Page(link)
        if page2.isRedirectPage():
            page2 = page2.getRedirectTarget()
        if page2.title() == page.title():
            item = ItemPage(Site('wikidata', 'wikidata').data_repository(), wikidata)
            item.setSitelink(page)
            self.countitis += 1
            print(f'Agregado sitelink a {page.title()}, {self.countitis}')
            

        

def main(*args: str) -> None:
    gen_factory = pagegenerators.GeneratorFactory()
    gen_factory.handle_arg('-unconnectedpages')

    gen = gen_factory.getCombinedGenerator(preload=True)

    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        bot = MyBot(generator=gen)
        bot.countitis = 0
        bot.run()
    print(f'total: {bot.countitis}')


if __name__ == '__main__':
    main()
