import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot import Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot

import utils.ia_source as IA
import utils.template_utils as t_u

class MyBot(CurrentPageBot):

    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: obteniendo pagelist desde IA.',
        'custom': 'This is the value of a custom -custom:parameter'
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text
        if len(t_u.get_index_data(text, 'Paginas')) > 16:
            text = t_u.set_index_data(
                text, 'Progreso', 'C', overwrite=True)
            self.put_current(text, summary=self.opt.summary)
            return
        text = re.sub(r"{{es\|\s*1\s*=\s*(.*?)}}", r"\1", text)
        source = t_u.get_index_data(text, 'Fuente')
        m = re.search(r"{{(?:IA|Internet Archive link|Internet Archive)\|\s*([^}]+)\s*}}", source, flags=re.I)
        if not m:
            return

        ia = IA.IaSource(m[1])
        pl = ia.get_pagelist()
        tag = pl.to_pagelist_tag(0)
        text = t_u.set_index_data(
            page.text, 'Paginas', tag, overwrite=True)
        if len(t_u.get_index_data(text, 'Paginas')) > 16:
            text = t_u.set_index_data(
                text, 'Progreso', 'C', overwrite=True)

        self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    args += ('-cat:Índices sin pagelist',)
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
