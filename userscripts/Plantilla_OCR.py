from OCRBot import OCRBot

options = {
    # Todo esto es opcional y tiene valores por defecto

    # resumen de edición
    'summary': 'Bot: OCR',

    # un número del 1 al 3. 0 para ver para tipos de encabezados disponibles.
    'tipo': 0,

    # el texto del medio en un encabezado típico, o una lista de dos textos
    
    'head': None,

    # ‘phetools’, ‘wmfOCR’ o ‘googleOCR’. False = no OCR
    'method': 'wmfOCR',

    # una lista de tuplas (r"regex", r"sub") para substituciones para
    # ser aplicadas *al final* de todas las substituciones por defecto
    'tuples': []
}
# Páginas de inicio y fin. fin = None → hasta el final
inicio = 1
fin = None

Índice = 'Archivo.djvu'
OCRBot(Índice, inicio, fin, **options).run()
