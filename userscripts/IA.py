import re
import pywikibot
from pywikibot import Page, Site
import UTILS
import utils.ia_source as IA
import Data

Commons = Site('commons', 'commons')


def get_book_desc(E):

    CATS = UTILS.get_dict_cats(E.data)

    commonsText = f'''== {{{{int:filedesc}}}} ==

{E.to_commons()}

== {{{{int:license-header}}}} ==
{{{{PD-old-70-expired}}}}

{CATS}
'''
    return commonsText


def do_upload(E, pdfurl, filename):
    filepage = Page(Commons, 'File:' + filename)
    
    if filepage.exists():
        pywikibot.info(f'{filename} ya existe en Commons. Saltando.')
        return filename

    filepath = UTILS.download_pdf_file(pdfurl, filename)
    if not filepath:
        return None

    bdhid = E.data['accession_number']
    commonsText = get_book_desc(E)

    filename = UTILS.upload_to_commons(filepath, filename,
                                       commonsText, query=bdhid)
    return filename


def do(id):
    # ids con slashes
    id = id.split('/')[0]
    ia = IA.IaSource(id)
    if not ia.exists() or len(ia.item.metadata) < 1:
        pywikibot.output(f'<<red>>{id} no existe')
        return
    pdfurl = ia.get_file_url()
    if pdfurl:
        d = ia.get_dict()
        pl = ia.get_pagelist()
        tag = pl.to_pagelist_tag(0)
        if len(tag) > 16:
            d["_pagelist"] = tag
            d["_progreso"] = 'C'

        # titleparts
        if not re.search(r'[Dd]\. ', d['title']):  # D(on).
            titleparts = d['title'].split('.', 1)
            d["title"] = titleparts[0]
            if len(titleparts) > 1:
                d["subtitle"] = titleparts[1]

        E = Data.Edition(data=d)
        E.process_data()

        P = E.data

        fmt = pdfurl.split('.')[-1]
        filename = UTILS.bad_chars(f'{P["title"].text.strip()[:100]}')

        if P.get('volume'):
            filename += f' - Volumen {P["volume"]}'

        filename += f' - IA {id}.{fmt}'

        filename = do_upload(E, pdfurl, filename)
        E.filename = filename
        if not filename:
            return
        P["_filename"] = 'File:' + filename
        return E
    else:
        return


def main(*args: str):
    args = pywikibot.handle_args(args)
    for arg in args:
        bdhid = arg

    if len(args) == 0:
        bdhid = input('Internet Archive ID: ')

    do(bdhid)


if __name__ == '__main__':
    main()
