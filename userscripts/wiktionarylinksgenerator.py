import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot


class MyBot(ExistingPageBot):

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        links = self.current_page.langlinks()
        NOT_ES = True
        for link in links:
            if str(link.site) == "wiktionary:es":
                NOT_ES = False
                break
        if NOT_ES:
            print('BOOP')
            with open('LIST', 'a') as f:
                f.write('*[[' + self.current_page.title() + ']]<small>([[:en:' +
                        self.current_page.title() + '|en]])</small>\n')


def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value
    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()


if __name__ == '__main__':
    main()
