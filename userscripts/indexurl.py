import pywikibot
from pywikibot import pagegenerators
from pywikibot import Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot
from utils.template_utils import get_index_data


class MyBot(CurrentPageBot):
    def __init__(self, **kwargs):
        self.repo = pywikibot.Site('wikidata', 'wikidata').data_repository()

        self.badges = {'T': pywikibot.ItemPage(self.repo, 'Q20748093'),
                       'V': pywikibot.ItemPage(self.repo, 'Q20748092'),
                       'C': pywikibot.ItemPage(self.repo, 'Q20748091'),
                       'P': pywikibot.ItemPage(self.repo, 'Q20748091'),}
        super().__init__(**kwargs)

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        prop = 'P1957'
        QID = get_index_data(page.text, 'wikidata')
        P = get_index_data(page.text, 'progreso')

        print(QID)
        if not QID:
            return
        item = pywikibot.ItemPage(self.repo, QID)
        newbadge = self.badges[P]

        if item.exists() and item.isRedirectPage():
            item = item.getRedirectTarget()

        # BADGES
        sls = item.sitelinks
        for sl in sls:
            if sl == 'eswikisource':
                oldsl = sls[sl]
                if newbadge in oldsl.badges:
                    break
                else:
                    oldbadges = [b for b in oldsl.badges
                                 if b not in self.badges.values()]

                    newsl = pywikibot.SiteLink(
                        title=oldsl.title,
                        site=oldsl.site,
                        badges=oldbadges + [newbadge])
                print('setting badge to sitelink')
                item.setSitelink(newsl, summary='Adding badges to validated and proofread Wikisource editions')

        # P1957: index URL
        if prop in item.claims:
            print('already exists')
            return

        claim = pywikibot.Claim(self.repo, prop)
        claim.setTarget(page.full_url())
        item.addClaim(claim, summary='Adding index URL to Wikisource editions')


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    site = Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    gen_factory.handle_arg('-cat:Índices validados')
    gen_factory.handle_arg('-cat:Índices corregidos')

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen).run()  # guess what it does


if __name__ == '__main__':
    main()
