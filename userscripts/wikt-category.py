import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot import Category, Site
from pywikibot.bot import ExistingPageBot  # ExistingPageBot #CurrentPageBot
import time


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Categorizing.', }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""

        REXP = r".+?Términos"
        page = self.current_page
        title = page.title()

        m = re.match(REXP, title)
        if m and page.categoryinfo['pages'] == 0:
            with open('LIST', 'a') as f:
                f.write(title + '\n')


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    # args += ('-commandline:argument',)
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('es', 'wiktionary')
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    Cat = Category(site, 'Categoría:Páginas con errores en secuencias de órdenes')

    gen = pagegenerators.SubCategoriesPageGenerator(Cat)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
