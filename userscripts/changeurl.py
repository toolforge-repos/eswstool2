import re
import pywikibot
from pywikibot import pagegenerators
from pywikibot import Page, Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot
import time


class MyBot(CurrentPageBot):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    update_options = {
        'always': False,
        'summary': 'Bot: standardizing the behaviour of {{BDH}}',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = re.sub(
            r'{{\s*BDH\s*\|\s*(?:1\s*=\s*)?https?://bdh.bne.es/bnesearch/detalle/',
            '{{BDH|1=', page.text)
        text = re.sub(
            r'{{\s*BDH\s*\|\s*(?:1\s*=\s*)?https?://bdh-rd.bne.es/viewer.vm\?id=(\d+)(?:&page=\d+)?',
            r'{{BDH|1=bdh\1', text)

        if text != page.text:
            self.put_current(text, summary=self.opt.summary)
            time.sleep(15)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('commons', "commons")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    gen_factory.handle_arg('-transcludes:BDH')

    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
