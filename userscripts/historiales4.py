import pywikibot
import re
import sys
from datetime import datetime
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot


site = pywikibot.Site('es', fam="wikisource")
site.login()

TALK_MESSAGE = '\n\n== Historial ampliado ==\n<!-- Generado en ' + \
    str(datetime.now()) + '-->\n{{historial ampliado|{{/historial}}}}'
HISTORIALES = ''


def process_pagelist(target, value_array):
    global HISTORIALES
    HISTORIALES = ''
    if len(value_array) > 0:
        for page in value_array:
            treat_page(page)
        target_talk = pywikibot.Page(pywikibot.Site(), title=target, ns="Talk")
        target_talk.text += TALK_MESSAGE
        target_talk.save('Agregando historial ampliado')

        target_talk_history = pywikibot.Page(
            pywikibot.Site(), title=target + '/historial', ns="Talk")
        target_talk_history.text = HISTORIALES
        target_talk_history.save('Agregando historial ampliado')


def treat_page(tit):
    global HISTORIALES

    P = pywikibot.Page(site, title=tit, ns=0)

    result = "'''[[" + P.title() + "]]'''\n<table>\n<tr><th>oldid</th><th>timestamp</th><th>Usuario</th><th>Resumen de edicion</th></tr>"

    for entry in P.revisions():
        result += '<tr>'
        result += ('<td> {r.revid} </td><td> {r.timestamp} </td><td> {r.user} </td><td> '
                   '<nowiki>{r.comment}</nowiki></td></tr>'.format(r=entry))
    result += '</table>\n\n'

    HISTORIALES = HISTORIALES + result

    P.delete(reason="Página fusionada", deletetalk=True, prompt=True)


def process_text(page):
    P = pywikibot.Page(site, title=page, ns=0)
    if not P.exists():
        pywikibot.output('Página: #' + page + '# no existe.')
        raise Exception('Página no existente.')
    text = P.text
    # este monstruo captura todo lo que está dentro de la plantilla {{página}}, en el parámetro Texto
    regexp = r"{{(?:[Pp]lantilla:)?[Pp]ágina.*?Texto\s*=\s*((?:{{(?:{{[^}]+}}|\|}|[^}])+}}|\|}|[^}])+)"
    regexpopt = r"TextOpcional\s*=\s*'*([\w\.\, ]+)'*'"

    core = re.search(regexp, text, flags=re.S)
    optional = re.search(regexpopt, text, flags=re.S)

    pywikibot.output('Texto analizado: ' + page + '.')

    if core and optional:
        return '<!-- ' + P.title() + '-->' + '{{t3|' + optional[1] + '}}\n\n' + core[1]

    return core and '<!-- ' + P.title() + '-->' + core[1]


def process_list(text):
    big = {}
    target = ''
    try:
        for line in text.splitlines():
            if line:
                if line[0] == '#':
                    target = line[1:].strip()
                    big[target] = []

                else:
                    big[target].append(line.strip())
        return big
    except:
        raise Exception('Lista mal formateada')


def main(*args: str):

    for arg in sys.argv[1:]:
        if arg[:7] == '-target':
            target = arg[8:].strip('"')

        if arg[:5] == '-file':
            filename = arg[6:].strip('"')

        if arg[:6] == '-autor':
            autor = arg[7:].strip('"')

    try:
        in_file = open(filename, 'r', encoding='utf8')
    except:
        raise Exception('Se necesita una lista formateada (-file:)')
    FILE = in_file.read()
    big = process_list(FILE)
    targets = list(big.keys())
    pywikibot.output("Lista inicial procesada")
    processed = {}

    for n, target in enumerate(big.keys()):
        # Procesamiento y almacenaje del texto
        sig = ''
        if n != 0:
            prev = '[[../{}/]]'.format(targets[n - 1].split("/")[-1])
        else:
            prev = '[[../|Portada]]'

        if n < len(targets) - 1:
            sig = '[[../{}/]]'.format(targets[n + 1].split("/")[-1])

        processed = ''
        for page in big[target]:
            core = process_text(page)
            if core:
                processed += core.strip()
            else:
                pywikibot.output(
                    'ERROR! Página #' + page + '# no se ajusta al formato esperado. Verificar por favor')
                raise Exception('Página mal formateada')

        # eliminar típica basurita
        basurita = r"\|}\n</[Pp]oem>((?:<!--.*?-->)?)<[Pp]oem>\n{\|"
        processed = re.sub(basurita, r'\1', processed)

        # Respaldo texto copiado
        if len(processed) > 0:
            T = pywikibot.Page(site, title=target, ns=0)

            PartesT = T.title().split("/")
            Encabe = '{{{{encabezado\n|título = {0} \n|autor = {1} \n|anterior = {2} \n|sección = {3} \n|próximo= {4} }}}}\n\n{{{{t2|{3}}}}}\n\n'.format(PartesT[0],
                                                                                                                                                         autor, prev, PartesT[-1], sig)

            T.text = T.text + Encabe + \
                '{{bloque centro/c}}\n' + processed + '\n{{bloque centro/f}}'
            T.save('Copiando texto fusionado')

        # Procesamiento de las páginas
        process_pagelist(target, big[target])


if __name__ == '__main__':
    main()
