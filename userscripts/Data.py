import re
import pywikibot
from pywikibot import ItemPage, FilePage, Page, Site, WbMonolingualText, WbTime
from pytictoc import TicToc
from utils.date_utils import text_to_WbTime
from utils.string_utils import bad_chars
from utils import bib_utils

from VARIABLES import MAP
t = TicToc()

TRANSLATED = {'Titulo': 'title', 'Subtitulo': 'subtitle',
              'Volumen': 'volume', 'Autor': 'author', 'Editor': 'editor',
              'Prologuista': 'introducer', 'Traductor': 'translator',
              'Imprenta': 'printer', 'Editorial': 'publisher',
              'Ilustrador': 'illustrator', 'Ano': 'date', 'Lugar': 'city',
              'Fuente': 'source', 'Imagen': 'image_page',
              'Wikidata': 'wikidata',
              'Paginas': '_pagelist', 'Progreso': '_progreso'}

BOOK_PARAMS = 'Author=|Translator=|Editor|Illustrator|Introducer|Title=|'\
    'Subtitle|Series_title|Volume=|Edition|Publisher=|Printer|Date=|'\
    'Publication_date|City=|Language=es|Description=|Source=|Permission=|'\
    'Image|Image_page=|Pageoverview|Wikisource=s:es:Índice:{{PAGENAME}}|'\
    'Homecat|Other_versions|'\
    'ISBN|LCCN|OCLC|References|Linkback|Wikidata=|noimage'


ROLETYPE = {'author', 'translator', 'editor', 'illustrator', 'introducer'}
MONOTYPE = {'title', 'subtitle'}
INSTTYPE = {'publisher', 'printer'}
DATATYPE = {'city'}

P_MAP = {y: x for x, y in MAP.items()}


class Newtype:
    pass


class Date(Newtype):
    def __init__(self, text=None):
        self.text = text.strip()

    @property
    def WbTime(self):
        return text_to_WbTime(self.text)

    def to_commons(self):
        return self.text

    def to_wikisource(self):
        return self.text

    def __str__(self):
        return str(self.text)

class Text(Newtype):
    pass


class Multilingual(Text):
    def __init__(self, langs: dict = dict(), default='es'):
        self._langs = self.to_mono(langs)
        self.default = default

    @property
    def langs(self):
        return self._langs

    @langs.setter
    def langs(self, value: dict):
        assert isinstance(value, dict), TypeError
        self._langs = self.to_mono(value)

    @property
    def text(self):
        if self.default in self._langs:
            return self._langs[self.default]
        else:
            for lang in self._langs:
                return self._langs[lang]

    @property
    def lang(self):
        if self.default in self._langs:
            return self.default
        else:
            for lang in self._langs:
                return lang

    def to_mono(self, orig):
        d = {}
        for lang, value in orig.items():
            if isinstance(value, str):
                d[lang] = Monolingual(value, lang)
            elif isinstance(value, Monolingual):
                d[lang] = value
            else:
                raise TypeError
        return d

    def __getitem__(self, value):
        return self._langs.get(value)

    def __setitem__(self, index, value):
        self._langs |= self.to_mono({index: value})

    def __iter__(self):
        for each in self._langs.values():
            yield each

    def __str__(self):
        return str(self.text)

    def __repr__(self):
        return str({k: v for k, v in self._lang.items() if v})


class Monolingual(Text):
    def __init__(self, text: str = '', lang: str = 'es'):
        if isinstance(text, Monolingual):
            self.text = text.text
            self.lang = text.lang
            self.oldstring = text.oldstring
        elif isinstance(text, str):
            self.text = text.strip()
            self.lang = lang.strip()
            self.oldstring = text

            if m := re.match(r'{{ *([a-z][a-z]+) *\|(?: *1 *=)? *([^}]+) *}}', self.text):
                self.text = m[2].strip()
                self.lang = m[1].strip()
        else:
            raise TypeError

    def __str__(self):
        return self.text

    def __repr__(self):
        return f'Monolingual({self.lang}, {self.text})'

    def __getitem__(self, index):
        return (self.text, self.lang)[index]

    def __len__(self):
        return len(self.text)


class Data(Newtype):
    def __init__(self,
                 name: str = None,
                 QID: str = None,
                 wikisource: str = None,
                 oldstring: str = None):
        self.name = name
        self.oldstring = oldstring or self.name
        if re.search(r'[{}]', self.oldstring or ''):
            self.oldstring = None

        self.wikisource = wikisource
        self.QID = QID

        self.item_page_cache = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return str({k: v for k, v in self.__dict__.items() if v})

    @property
    def item_page(self):
        if self.QID:
            if self.item_page_cache:
                return self.item_page_cache
            else:
                self.item_page_cache = ItemPage(
                    Site('wikidata', 'wikidata').data_repository(), self.QID)
            return self.item_page_cache

    def get_label(self):
        if self.item_page:
            return self.item_page.labels.get('es')


class Inst(Data):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def to_commons(self):
        if self.name == 'UNKNOWN':
            return '{{Anonymous}}'

        if self.QID:
            return f'{{{{Institution|wikidata={self.QID}}}}}'
        else:
            return self.oldstring or 's. n.'

    def to_wikisource(self):
        if self.name == 'UNKNOWN':
            return '[[Portal:Anónimo|]]'

        value = ''
        if self.QID:
            if sitelink := self.item_page.sitelinks.get('eswikisource'):
                value = sitelink.canonical_title().split(':')[-1]
            elif label := self.get_label():
                value = label
        elif self.name:
            value = self.name

        if value:
            return f'[[Portal:{value}|]]'
        else:
            return self.oldstring


class Person(Data):
    def __init__(self, role: str = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        proc = bib_utils.process_human(self.name)

        self.name = proc[0]
        self.role = proc[1] if proc[1] else role
        self.QID = proc[2] if proc[2] else self.QID

    def make_inferences(self):
        if self.QID:
            return
        if not self.name:
            return
        self.QID = bib_utils.search_QID(self.name)

    def to_commons(self):
        if self.name == 'UNKNOWN':
            return '{{Anonymous}}'

        self.make_inferences()
        if self.QID:
            return f'{{{{creator|wikidata={self.QID}}}}}'
        else:
            return self.oldstring

    def to_wikisource(self):
        if self.name == 'UNKNOWN':
            return '[[Portal:Anónimo|]]'

        self.make_inferences()
        value = ''
        if self.QID:
            if sitelink := self.item_page.sitelinks.get('eswikisource'):
                value = sitelink.canonical_title().split(':')[-1]
            elif label := self.get_label():
                value = label
        elif self.name:
            value = self.name

        if value:
            return f'[[Autor:{value}|]]'
        else:
            return self.oldstring


class Edition(Data):

    def __init__(self,
                 data: dict = dict(),
                 filename: str = None,
                 commons_page: Page = None,
                 index_page: Page = None,
                 *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.data = data

        self._filename = filename
        self.commons_page = commons_page
        self.index_page = index_page

        self.sources = {}

    def __str__(self):
        return repr(self)

    def __getitem__(self, index):
        return self.data.get(index)

    def __setitem__(self, index, value):
        self.data[index] = value

    def copy(self):
        return Edition(data=self.data.copy())

    def get(self, value, default=None):
        return self.data.get(value, default)

    @property
    def filename(self):
        return self.get_filename(self._filename)

    @filename.setter
    def filename(self, value):
        self._filename = self.get_filename(value)

    def get_filename(self, filename):
        if filename:
            link = pywikibot.Link(filename)
            return link.title
        if self.data.get('_filename'):
            filename = self.data.get('_filename')
        return filename

    def get_empty_dict(self):
        return {prop: '' for prop in MAP}

    def get_proc_dict(self, data=None):
        proc_data = {}
        if not data:
            data = self.data
        data = bib_utils.process_dict(data)
        for k, v in data.items():
            # Freepass para Newtypes
            if isinstance(v, Newtype):
                proc_data[k] = v
                continue

            # Restricciones de formato
            if k in MONOTYPE:
                if isinstance(v, str):
                    proc_data[k] = Monolingual(v)
                else:
                    raise TypeError(type(v))

            elif k in INSTTYPE:
                if isinstance(v, str):
                    proc_data[k] = Inst(name=v)
                else:
                    raise TypeError(type(v))

            elif k in DATATYPE:
                if isinstance(v, str):
                    proc_data[k] = Data(name=v)
                else:
                    raise TypeError(type(v))
            elif k == 'date':
                if isinstance(v, str):
                    proc_data[k] = Date(v)
                else:
                    raise TypeError(type(v))
            elif k in ROLETYPE:
                # str = de Commons o flojitos
                if isinstance(v, str):
                    # 1: separar plantillas
                    v = re.sub(r'}}\s*{{', r'}}\n{{', v)
                    # 2: separar x saltos de línea
                    v = [i.strip(" :*#\u200c") for i in v.split('\n')]

                    for i in v:
                        if i.strip():
                            p = Person(name=i, role=k)
                            p.make_inferences()
                            proc_data[p.role] = proc_data.get(
                                p.role, list()) + [p]
                # lista = lista formateada de Persons
                elif isinstance(v, list):
                    for p in v:
                        proc_data[p.role or k] = proc_data.get(
                            p.role, list()) + [p]

        return proc_data

    def process_data(self, data=None):
        self.data |= self.get_proc_dict(data)
        for key, ID in self.data.get('_sources', {}).items():
            self.sources[key] = ID
        return self.data

    def get_commons_dict(self):
        if self.filename:
            if not self.filename.startswith('File:'):
                agnostic_fn = self.filename.split(':', 1)[-1]
                self.filename = f'File:{agnostic_fn}'
            if not self.commons_page:
                self.commons_page = Page(
                    Site('commons', 'commons'), self.filename)

            new_data = bib_utils.commons_to_dict(self.commons_page.text)
            if new_data:
                new_data |= self.get_proc_dict(new_data)
                return new_data
            return {}

    def set_commons_dict(self):
        new_data = self.get_commons_dict()
        if new_data:
            self.data |= new_data

    def get_wikidata_dict(self):
        new_data = {}
        for P in self.item_page.claims:
            p = P_MAP.get(P)
            if p:
                C = self.item_page.claims[P]
                for c in C:
                    if c.getSnakType() == 'value':
                        t = c.getTarget()
                        if isinstance(t, str):
                            new_data[p] = t
                        elif isinstance(t, FilePage):
                            new_data[p] = t.title()
                        elif isinstance(t, WbMonolingualText):
                            new_data[p] = Monolingual(t.text, 'es')
                        elif isinstance(t, WbTime):
                            new_data[p] = t
                        elif isinstance(t, ItemPage):
                            if p in ROLETYPE:
                                new_data[p] = new_data.get(p, list())
                                new_data[p].append(
                                    Person(role=p, QID=t.title()))
                            elif p in INSTTYPE:
                                new_data[p] = Inst(QID=t.title())
                            else:
                                new_data[p] = Data(QID=t.title())
                        else:
                            print('INPUT NO RECONOCIDO', t, type(t))
        return new_data

    def to_commons(self, PARAMS=BOOK_PARAMS):
        '''Formato BOOK_PARAMS:  Param1|Param2|Param3|...
        # Param=Valor:  Valor obligatorio
        # Param=:       Parámetro obligatorio (lo crea aunque no tenga valor)
        # Param= :      Borrar valor obligatorio.
        '''
        d = {u[0].lower().strip(): u[2] for u in [t.partition('=')
                                                  for t in PARAMS.split('|')]}
        mandatory = {u[0].lower().strip(): u[1]
                     for u in [t.partition('=') for t in PARAMS.split('|')]}
        D = self.data
        # Formatos especiales para Commons
        # Formato monolingual
        for x in MONOTYPE:
            if D.get(x):
                text = D[x].text
                lang = D[x].lang

                d[x] = rf'{{{{{lang}|1={text}}}}}'
        # Formato roles
        for x in ROLETYPE:
            if D.get(x):
                P_LIST = []
                for p in D[x]:
                    p = p.to_commons()
                    if p:
                        P_LIST.append(p)
                d[x] = '\n:'.join(P_LIST)
        # Formato otros tipo Data
        for x in INSTTYPE:
            if D.get(x):
                # TODO: otro formato para Datas con QID?
                d[x] = D[x].to_commons()
        for x in DATATYPE:
            if D.get(x):
                # TODO: otro formato para Datas con QID?
                d[x] = D[x].name
        if not D.get('wikidata') and self.QID:
            D['wikidata'] = self.QID

        s = "{{Book\n"
        # Prioridades
        for k in d:
            if k.startswith('_'):
                continue
            # Fixes específicos para Commons
            if k in ['publisher', 'author'] and D.get(k) == 'UNKNOWN':
                D[k] = '{{Anonymous}}'
            if d.get(k):            # 1. Si d tiene un valor preasignado
                s += f'| {k[0].upper() + k[1:]:12} = {d[k]}\n'
            elif D.get(k):          # 2. si D tiene un valor asignado
                s += f'| {k[0].upper() + k[1:]:12} = {D[k]}\n'
            elif mandatory.get(k):  # 3. si es un parámetro obligatorio
                s += f'| {k[0].upper() + k[1:]:12} = \n'

        for k in D:                 # 4. Resto de los parámetros que estén en D
            if k not in d and not k.startswith('_'):
                s += f'| {k[0].upper() + k[1:]:12} = {D[k]}\n'
        s += "}}"

        return s

    def to_wikisource(self, local=False):
        INDEX_PARAMS = 'Titulo=|Subtitulo=|Volumen=|Autor=|Editor=|Traductor=|Prologuista=|'\
            'Imprenta=|Editorial=|Ilustrador=|Ano=|Lugar=|derechos=|Fuente=|'\
            'Imagen=1|Progreso=P|Paginas=<pagelist />|Notas=|Wikidata=|'\
            'Serie=|Header=|Footer=|Modernizacion=default|Dict=|ultima-muerte='
        D = self.data
        d = {u[0].strip(): u[2] for u in [t.partition('=')
                                          for t in INDEX_PARAMS.split('|')]}
        translate = TRANSLATED

        # Formato monolingual
        for x in MONOTYPE:
            if D.get(x):
                D[x] = D[x].text
        # Formato roles
        for x in ROLETYPE:
            if D.get(x):
                P_LIST = [p.to_wikisource() for p in D[x]]
                D[x] = ', '.join(P_LIST)
                if not local:
                    D[x] = ''
        # Formato otros tipo Data
        for x in INSTTYPE:
            if D.get(x):
                # TODO: otro formato para Datas con QID?
                D[x] = D[x].to_wikisource()
                if not local:
                    D[x] = ''
        for x in DATATYPE:
            if D.get(x):
                # TODO: otro formato para Datas con QID?
                D[x] = D[x].name
                if not local:
                    D[x] = ''
        # sanidad general: eliminar {{es|1=}}
        for k, v in D.items():
            if isinstance(D[k], str):
                D[k] = re.sub(r'{{ *[a-z][a-z] *\|(?: *1 *= *)? *'
                              r'(.+?) *}}', r'\1', D[k])

        D['title'] = f'[[{D["title"]}]]'

        if D.get('source'):
            D['source'] = D['source'].strip(': ').split('\n')[0]

        if D.get('_publisher'):
            D['publisher'] = f'[[Portal:{get_link(D["_publisher"])}|]]'
        # Otra sanidad general
        D['source'] = re.sub(r'internet archive(?: link)?', 'IA',
                             D['source'], flags=re.I)
        D['source'] = re.sub(
            r'{{ *Google Books?(?: Search)? link?', 'GB', D['source'])
        D['source'] = re.sub(
            r'https?://bdh.bne.es/bnesearch/detalle/', '', D['source'])
        D['source'] = re.sub(
            r'https?://www.memoriachilena.gob.cl/602/w3-article-(\d+).html?', r'\1', D['source'])
        # Construcción de la plantilla

        if not D.get('wikidata') and self.QID:
            D['wikidata'] = self.QID

        s = "{{:MediaWiki:Proofreadpage_index_template\n"

        for k in d:
            # Fixes específicos para Commons
            if k in ['publisher', 'author'] and D.get(k) == 'UNKNOWN':
                D[k] = ''
            # Prioridades
            # 1. si D tiene un valor asignado
            if v2 := D.get(translate.get(k)):
                s += f'|{k}={v2}\n'
            # 2. Si d tiene un valor preasignado
            elif d.get(k):
                s += f'|{k}={d[k]}\n'
            # 3. todo INDEX_PARAMS
            else:
                s += f'|{k}=\n'
        s += "}}"
        return s

    def get_dict_cats(self):
        P = self.data

        def is_cat(s):
            if s and bad_chars(s) == s:
                return Page(Site('commons', 'commons'), f'Category:{s}').exists()

        cats = []

        AUTHORS = P.get("author", list())

        for author in AUTHORS:
            author = author.name
            if is_cat(f'Books by {author}'):
                cats.append(f'Books by {author}')
            elif is_cat(f'Works by {author}'):
                cats.append(f'Works by {author}')
            elif is_cat(f'{author}'):
                cats.append(f'{author}')

        if P.get('publisher') and is_cat(P['publisher'].name):
            cats.append(P['publisher'].name)

        if P.get('city') and is_cat(f'Published in {P["city"].name}'):
            cats.append(f'Published in {P["city"].name}')

        if str(P.get("date", '')).isdigit():
            cats.append(f'{P["date"]} books')

        return '\n'.join([f'[[Category:{x}]]' for x in cats])


def main(*args: str):
    t.tic()
    Eddie = Edition({'title': 'tasdasdas'})
    print(Eddie['title'])
    print(Eddie['titledasd'])
    t.toc()


if __name__ == '__main__':
    main()
