import pywikibot
import webbrowser
import re
from pywikibot import pagegenerators
from BASEINDEXBOT import IndexBot
from pywikibot.proofreadpage import PagesTagParser
import roman


class FormatError(Exception):
    pass


def title(s):
    def title_case(s):
        try:
            roman.fromRoman(s)
            return s
        except Exception:
            if s == s.upper():
                return x[0] + x[1:].lower()
            else:
                return s

    s = s.strip(" .:;\n")

    t = []
    conj = {'el', 'la', 'las', 'los', 'un', 'una',
            'uno', 'unos', 'unas', 'de', 'menor'}
    for i, x in enumerate(s.split()):
        if i == 0:
            t.append(title_case(x))
        elif x.lower() in conj or len(x) == 1:
            t.append(x.lower())
        else:
            t.append(title_case(x))

    return ' '.join(t)


def decompose_sections(text):
    if len(text) == 0:
        return [(None, '', 0)]

    BEGIN = r'< *section *begin *= *"([^"]*?)"(?:[^/>]*) */>'
    END = r'< *section *end *= *"\1"(?:[^/>]*) */>'

    t = []
    first = 0
    last = 0

    for m in re.finditer(fr'{BEGIN}(.*?){END}', text, flags=re.DOTALL):
        start = m.start()
        last = m.end()
        if start > first:
            t.append([None, text[first:start], first])
        first = last
        t.append([m[1], m[2], start])

    if last < len(text):
        t.append([None, text[last:], last])

    if recompose_sections(t) == text:
        return t

    raise FormatError('Secciones del texto no tienen un formato regular')


def recompose_sections(t):
    result = ''
    for e in t:
        if e[0] and e[1]:
            result += f'<section begin="{e[0]}"/>{e[1]}<section end="{e[0]}"/>'
        else:
            result += e[1]
    return result


class TOCBot(IndexBot):

    update_options = {
        'always': False,
        'summary': 'Bot: segmentando secciones para transclusión',
    }

    def __init__(self, index, MIN=2, MAX=3, *args, **kwargs):
        super().__init__(index, *args, **kwargs)

        self.TOC = {}
        self.current_chapter = None
        
        self.RANGE = ''.join(str(i) for i in range(MIN, MAX + 1))
        self.REXP = rf'{{{{ *t[{self.RANGE}] *\| *(?:cap[ií]tulo)? *([^|}}]+?) *[.,]? *[}}|]'
        print(self.RANGE, self.REXP)

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        old_text = page.body
        n = int(page.title().split('/')[1])
        decomp = decompose_sections(page.body)

        save = True
        if len(decomp) > 1 or decomp[0][0]:
            print('ya segmentada:', n)
            save = False
        for m in re.finditer(self.REXP, page.body, flags=re.I):
            cur = self.current_chapter
            chapter = m[1]
            segment = True
            start = m.start()
            if start < 10:
                segment = False

            if not self.TOC.get(chapter):
                # Segmentar Página
                if segment:
                    cur_start = decomp[-1][2]
                    decomp[-1][1] = page.body[cur_start:start]
                    self.TOC[chapter] = {'from': n, 'fromsection': chapter}
                    if cur:
                        decomp[-1][0] = cur
                        self.TOC[cur]['to'] = n
                        self.TOC[cur]['tosection'] = cur
                    decomp.append([chapter, page.body[start:], start])
                else:
                    self.TOC[chapter] = {'from': n}
                    if cur:
                        self.TOC[cur]['to'] = n - 1

                self.current_chapter = chapter
            else:
                print(self.TOC)
                webbrowser.open(
                    "https://es.wikisource.org/w/index.php?title=" + page.title(as_url=True) + "&action=edit")

        page.body = recompose_sections(decomp)
        # decomp = decompose_sections(page.body)

        if save and old_text != page.body:
            pywikibot.showDiff(old_text, page.body)
            if self.opt.always:
                choice = 'a'
            else:
                choice = pywikibot.input_choice(
                    'Do you want to accept these changes?',
                    [('Yes', 'y'), ('No', 'n'), ('all', 'a')],
                    default='N')

            if choice == 'y' or choice == 'a':
                page.save(self.opt.summary)

                if choice == 'a':
                    self.opt.always = True


def main(*args: str):
    '''Plantilla para bots que trabajan en ProofreadPage'''
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value

    index = 'Inquietudes sentimentales - MC0075728.pdf'
    ns0 = 'Inquietudes sentimentales'

    start = 1
    end = None

    BOT = TOCBot(index=index, start=start, end=end, **options)
    BOT.run()
    BOT.TOC[BOT.current_chapter]['to'] = end

    site = pywikibot.Site()
    always = False
    for k, v in BOT.TOC.items():
        print(k, v)
    for k, v in BOT.TOC.items():
        if k.lower() == 'índice' or k.lower() == 'indice':
            continue
        tag = PagesTagParser()

        if v.get('from'):
            tag.ffrom = v.get('from')
        if v.get('to'):
            tag.to = v.get('to')
        if v.get('fromsection'):
            tag.fromsection = v.get('fromsection')
        if v.get('tosection'):
            tag.tosection = v.get('tosection')
        tag.header = 1
        tag.index = f'"{index}"'

        titulo = ''
        if ns0:
            titulo = f'{ns0}/{title(k)}'
        else:
            titulo = title(k)

        page = pywikibot.Page(site, titulo)
        page.text = str(tag)
        print(page.title(), tag)
        if always:
            choice = 'a'
        else:
            choice = pywikibot.input_choice(
                'Do you want to accept these changes?',
                [('Yes', 'y'), ('No', 'n'), ('all', 'a')],
                default='N')
        if choice == 'y' or choice == 'a':
            page.save('Bot - creando subpáginas en base a secciones del texto')
            if choice == 'a':
                always = True


if __name__ == '__main__':
    main()
