import pywikibot
import re
import roman
import time
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

def tc(s):
    def title_case(s):
        try:
            roman.fromRoman(s)
            return s
        except Exception:
            if s == s.upper():
                return x[0] + x[1:].lower()
            else:
                return s

    s = s.strip(" .:;\n")

    t = []
    conj = {'el', 'la', 'las', 'los', 'un', 'una',
            'uno', 'unos', 'unas', 'de', 'menor',
            'y', 'o', 'ó', 'a', 'á'}
    for i, x in enumerate(s.split()):
        if i == 0:
            t.append(title_case(x))
        elif x.lower() in conj:
            t.append(x.lower())
        else:
            t.append(title_case(x))

    return ' '.join(t)

class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: trasladando a subpáginas, otros cambios',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""

        def transform(titles, newt, s):
            s = re.sub(rf'{re.escape(titles)} *: *', f'{newt}/', s)
            # s = re.sub(rf'{newt}/([A-ZÁÉÍÓÚ ]+)', lambda m: rf'{newt}/{tc(m[1])}', s)
            # s = re.sub(rf'{titles}/(?![IV]+)([A-ZÁÉÍÓÚ\d ]+)', lambda m: f'{titles}/{m[1][0]}{m[1][1:].lower()}', s)
            return s

        page = self.current_page
        old_text = page.text
        title = page.title()
        titles = self.titles
        newt = self.new_title or titles

        new_title = transform(titles, newt, title)

        page.text = transform(titles, newt, page.text)

        if old_text != page.text:
            page.save(self.opt.summary)

        if new_title != title:
            print(f'Moviendo "{title}" a "{new_title}"')
            try:
                page.move(new_title, self.opt.summary)
            except pywikibot.exceptions.ArticleExistsConflictError:
                pass

def main(titles, new_title, *args: str):
    """Parse command line arguments and invoke bot."""
    gen_factory = pagegenerators.GeneratorFactory()
    #gen_factory.handle_arg(f'-cat:{new_title or titles}')
    gen_factory.handle_arg(f'-prefixindex:{titles}')
    gen_factory.handle_arg(f'-page:{new_title or titles}')
    bot = MyBot(generator=gen_factory.getCombinedGenerator())
    bot.titles = titles
    bot.new_title = new_title

    bot.run()


if __name__ == '__main__':

    tits = ['Noches lúgubres']
    for titles in tits:
        main(titles=titles,
             new_title=None
             )
