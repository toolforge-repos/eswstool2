from OCRBot import OCRBot
import re


options = {
    # Todo esto es opcional y tiene valores por defecto
    'always': False,
    # resumen de edición
    'summary': 'Bot: arreglos varios',
    # un número del 1 al 3. 0 para ver para tipos de encabezados disponibles.
    'tipo': 4,
    # el texto del medio en un encabezado típico, o una lista de dos textos
    'head': '',
    # ‘phetools’, ‘wmfOCR’ o ‘googleOCR’. False = no OCR
    'method': False,
    # una lista de tuplas (r"regex", r"sub") para substituciones para
    'tuples': [
    (r'(N\.?|\d+\.?)["*%?+\']+', r'\1º'),
    (r'^\s*[—\-]+ *\d+ *[—\-]+\s*', r''),

    ],
    # ser aplicadas *al final* de todas las substituciones por defecto
}
start = 1
end = None

Índice = 'Padro Derechos Civiles de la Mujer 1926.djvu'
OCRBot(Índice, start, end, **options).run()
