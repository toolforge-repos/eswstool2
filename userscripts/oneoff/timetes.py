import pywikibot
from pywikibot import proofreadpage
from pywikibot import pagegenerators

import time

site=pywikibot.Site('es', fam="wikisource")

def treat_page(page):
    page.body = page.body + ""
    pass
        
time1 = time.time()

index = "Cautiverio feliz, y razón de las guerras dilatadas de Chile.pdf"
INDEX = proofreadpage.IndexPage(site, title="Index:"+index)


gen = INDEX.page_gen(end=100, content=False)

for page in gen:
    treat_page(page)

time1 = time.time() - time1

print('content=False: ', time1)
