from pywikibot import pagegenerators
from pywikibot import Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot
import re


class MyBot(CurrentPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot: cambio funcionamiento plantilla',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text
        text = re.sub(r'{{ *?ea *?\| *?([^}\|]*?)\| *?([^}\|]*?)\| *?([^}\|]*?)}}', r'{{ea|\1|\3|\2}}', text)
        self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:

    site = Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    gen_factory.handle_arg('-transcludes:enlace anclaje')
    gen = gen_factory.getCombinedGenerator()
    MyBot(generator=gen).run()  # guess what it does


if __name__ == '__main__':
    main()
