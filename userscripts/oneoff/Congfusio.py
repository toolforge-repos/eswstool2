import pywikibot, re
from pywikibot import proofreadpage
from pywikibot import pagegenerators
from pywikibot.bot import CurrentPageBot

site=pywikibot.Site('es', fam="wikisource")
site.login()

class MyBot(CurrentPageBot):

    
    update_options = {
        'always': False,
        'summary': 'Bot: OCR + arreglos varios',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        ppage = self.current_page
        
        only_non = False
        if only_non and ppage.exists():
            return ''
        
        ppage.body = ppage.ocr(ocr_tool='wmfOCR') # ‘phetools’, ‘wmfOCR’ or ‘googleOCR’
        N = ppage._parse_title()[2]

        HEADER = get_header(N)
        if HEADER:
            ppage.header = HEADER
        
        ppage.body = transform_body(ppage.body)

        #print(ppage.header)
        #print(ppage.body)
        ppage.save(summary=self.opt.summary)
        
def get_header(N, S = False):
        return "{{{{cp||[{}]|[{}]|}}}}".format(str(N*2-4), str(N*2-3))
        
def transform_body(body):    
    REPLACE_TUPLES = [  (r"-\n",""), #elimina crudamente guion-salto de línea
                        (r"(?<=\S)\n(?=\S)", " "), #elimina saltos de línea simples
                        (r"\b4\b", "á"),
                        (r"\bÓ\b", "ó"),
                        (r"[\(\)\[\]]*\d+[\(\)\[\]]*", ""),
                        (r"\s*([CLXVIT]+)[.,]*\s*\n+", r"\n\n{{t3|\1.}}\n\n")]

    for x in REPLACE_TUPLES:
        body = re.sub(x[0], x[1], body)
    return body

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    index = "Vida y pensamientos morales de Confucio (IA BRes1115932).pdf"

    INDEX = proofreadpage.IndexPage(site, title="Index:"+index)

    gen_factory = INDEX.page_gen(filter_ql=[1])

    MyBot(generator=gen_factory).run()

if __name__ == '__main__':
    main()