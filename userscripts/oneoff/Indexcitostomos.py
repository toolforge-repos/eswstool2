import pywikibot
from pywikibot import pagegenerators
from pywikibot import Page, Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot

import utils.template_utils
import re

class MyBot(CurrentPageBot):

    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: ajustes varios.',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text
        pagenum = re.search(r'page=(\d+)', text)
        if pagenum:
            text = utils.template_utils.set_index_data(text, 'Imagen', pagenum[1], overwrite=True)
            text = utils.template_utils.set_index_data(text, 'Notas', '{{Tomos CL}}')
        self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    # args += ('-commandline:argument',)
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
