import pywikibot, re
from pywikibot.proofreadpage import IndexPage, ProofreadPage
from pywikibot import pagegenerators, Site
from pywikibot.bot import CurrentPageBot

from VARIABLES import DEFAULT_TUPLES

class OCRBot(CurrentPageBot):
    
    update_options = {
        'always': False,
        'summary': 'Bot: OCR + arreglos varios',
        'tipo': 0,
        'head': ''
    }
    def __init__(self, index = '', **kwargs) -> None:
        prefix = 'Índice:'
        if index == '':
            index = pywikibot.input('Ingresa el Índice, con o sin el prefijo.')
        index = index if index.startswith(prefix) else prefix + index

        self.index = IndexPage(Site('wikisource:es'), index)
        while not self.index.exists():
            index = pywikibot.input(f'<<lightred>>{self.index.title()} no existe, ingresa un Índice creado<<gray>>:')
            index = index if index.startswith(prefix) else prefix + index
            self.index = IndexPage(Site('wikisource:es'), index)

        index_gen = self.index.page_gen(filter_ql=[1], only_existing=False)

        super().__init__(generator=index_gen, **kwargs)

        if self.opt.tipo == 0:
            modelos = { '1': '— num — centrado',
                    '2': 'par e impar con el mismo texto',
                    '3': 'par e impar con textos distintos (pero iguales a través del libro)'}
        
            pywikibot.info('Ingresa el tipo de encabezado:\n' + '\n'.join(f'[{n}]: {desc}' for n, desc in modelos.items()))
            
            self.tipo = int(pywikibot.input_choice('tipo', (('1', '1'), ('2', '2'), ('3', '3'))))
        else:
            self.tipo = self.opt.tipo

        if self.opt.head == '':
            if self.tipo == 2:
                self.head = pywikibot.input('Encabezado (pares e impares)')
            elif self.tipo == 3:
                self.head = [pywikibot.input('Encabezado (pares)'), pywikibot.input('Encabezado (impares)')]
        else:
            self.head = self.opt.head


    def get_header(self, N):
        if N == False:
            return ""
        if self.tipo == 1:
            return f"{{{{cp||— {N} —|}}}}"
        elif self.tipo == 2:
            if N % 2 == 0:
                return f"{{{{cp|{N}|{head}|}}}}"
            else:
                return f"{{{{cp||{head}|{N}}}}}"
        elif self.tipo == 3:
            if N % 2 == 0:
                return f"{{{{cp|{N}|{head[0]}|}}}}"
            else:
                return f"{{{{cp||{head[1]}|{N}}}}}"

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        label = self.index.get_label_from_page(page)
        HEADER = self.get_header()
        
        ppage.header = HEADER

        ppage.body = ppage.ocr(ocr_tool='wmfOCR') # ‘phetools’, ‘wmfOCR’ or ‘googleOCR’
        if len(ppage.body) < 4:
            ppage.header = ''   
            ppage.without_text()

        ppage.body = transform_body(ppage.body)
        
        print(ppage.body)
        ppage.save(summary=self.opt.summary)

    def transform_body(body):    
        REPLACE_TUPLES = [(r"^[—-][ ]*\d+[ *][—-]\s*", ""),] + DEFAULT_TUPLES 

        for x in REPLACE_TUPLES:
            body = re.sub(x[0], x[1], body)
        return body

def main(*args: str):
    '''Plantilla para bots que trabajan en ProofreadPage'''
    args = pywikibot.handle_args(args)
    options = { }
    
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-tipo', '-force', '-head'):
            options[opt] = value
        else:
            options[opt] = True

    OCRBot(**options).run()

if __name__ == '__main__':
    main()

    