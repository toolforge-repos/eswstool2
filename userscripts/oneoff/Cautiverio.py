import pywikibot, re
from pywikibot import proofreadpage
from pywikibot import pagegenerators
from pywikibot.bot import CurrentPageBot

from VARIABLES import DEFAULT_TUPLES

site=pywikibot.Site('es', fam="wikisource")
site.login()

FileName= "CAUTIVERIO"
file=open(FileName,'a+', encoding="utf8")

class MyBot(CurrentPageBot):

    
    update_options = {
        'always': False,
        'summary': 'Bot: OCR + arreglos varios',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        ppage = self.current_page
        
        label = INDEX.get_label_from_page(ppage)
        try:
            N = int(label)
        except ValueError:
            N = False

        HEADER = get_header(N)
        if HEADER:
            ppage.header = HEADER

        ppage.body = ppage.ocr(ocr_tool='googleOCR') # ‘phetools’, ‘wmfOCR’ or ‘googleOCR’
        

        ppage.body = transform_body(ppage.body)
        
        #file.write('$'+label+'$\n'+ppage.body+'\n\n')

        #print(ppage.header)
        #print(ppage.body)
        ppage.save(summary=self.opt.summary)

def get_header(N, S = False):
    if not N:
        return ""

    if N % 2 == 0:
        return "{{{{cp|{}|HISTORIADORES DE CHILE.|}}}}".format(str(N))
    else:
        return "{{{{cp||NUÑEZ DE PINEDA Y BASCUÑAN.|{}}}}}".format(str(N))

def transform_body(body):    
    REPLACE_TUPLES = [  (r"(?s)^.{,10}NU[A-ZÑÁ ]+\.*\n\d+\n",""), #encabes
                        (r"(?s)^.{,10}\d+\nHIS[A-Z ]+\.*\n", ""), #encabes
                        (r"\n+CAP[IÍ]TULO ([IVXLC]+)\.*\n", r"\n\n{{t3|CAPITULO \1.}}\n\n")
    ] + DEFAULT_TUPLES

    for x in REPLACE_TUPLES:
        body = re.sub(x[0], x[1], body)
    return body

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    index = "Cautiverio feliz, y razón de las guerras dilatadas de Chile.pdf"
    
    global INDEX
    INDEX = proofreadpage.IndexPage(site, title="Index:"+index)

    gen_factory = INDEX.page_gen(filter_ql=[1], only_existing=False, start=551)

    MyBot(generator=gen_factory).run()

if __name__ == '__main__':
    main()