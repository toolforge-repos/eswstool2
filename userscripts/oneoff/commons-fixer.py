import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot
import UTILS

site = pywikibot.Site('commons', "commons")


def get_template_data(page, template, parameter, from_link=False):
    temps = page.raw_extracted_templates
    params = [t[1] for t in temps if t[0].lower() == template.lower()]
    if len(params) == 0:
        return False  # False = no hay plantilla, None = no hay parámetro

    params = {k.lower(): v for k, v in params[0].items()}
    value = params.get(parameter.lower())
    return value


def set_template_data(page, param, setto, overwrite=False, create=False, delete=False):
    text = page.text
    value = get_template_data(page, 'Book', param)
    PATTERN = rf'\|([{param[0].lower() + param[0].upper()}]{param[1:]})\s*=\s*({re.escape(str(value))})\s*'
    Param = rf'{param[0].upper()}{param[1:]}'

    if value == False:
        return text
    elif value == None and create:
        text = re.sub(r'{{Book\s*\|',
                      rf'{{{{Book\n|{Param} = {setto}\n|', text)
    elif value == '':
        text = re.sub(PATTERN, rf'|{Param} = {setto}\n', text)
    elif not setto and delete:
        text = re.sub(PATTERN, rf'|{Param} =\n', text)
    elif value and overwrite:
        text = re.sub(PATTERN, rf'|{Param} = {setto}\n', text)
    return text


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Semi-automatic edit: cleaning bibliographical data',
    }

    def treat_page(self):
        page = self.current_page
        old_text = page.text
        page.text = UTILS.clean_codecs(page.text)
        language = get_template_data(page, 'Book', 'Language')
        if str(language).strip().lower() in ['spa', 'es', '{{language|1=es}}', 'spanish', 'español']:
            page.text = set_template_data(
                page, 'Language', '{{language|1=es}}', overwrite=True)

            autor = get_template_data(page, 'Book', 'Author')
            autores = autor.split('\n')
            creators = []
            for x in autores:
                process = UTILS.process_human(x)
                creator = UTILS.creator(process, only_creator=True)

                if creator[1]:
                    creators.append(creator[0])
                else:
                    creators.append(x)

            publisher = get_template_data(page, 'Book', 'Publisher')
            print(publisher)
            publishers = UTILS.process_publisher(publisher)

            if len(publishers) == 2:
                page.text = set_template_data(
                    page, 'City', publishers[0], create=True)
                page.text = set_template_data(
                    page, 'Publisher', publishers[1], overwrite=True)

            page.text = set_template_data(
                page, 'Author', '\n'.join(creators), overwrite=True)
            title = get_template_data(page, 'Book', 'Title')

            wikititulo = UTILS.process_title(title)
            if len(wikititulo) == 2:
                page.text = set_template_data(
                    page, 'Title', f'{{{{es|1={wikititulo[0]}}}}}', overwrite=True)
                page.text = set_template_data(
                    page, 'Subtitle', f'{{{{es|1={wikititulo[1]}}}}}', overwrite=True, create=True)
            elif len(wikititulo) == 1:
                page.text = set_template_data(
                    page, 'Title', f'{{{{es|1={wikititulo[0]}}}}}', overwrite=True)

            new_text = page.text
            page.text = old_text
            self.put_current(new_text, summary=self.opt.summary)


def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value

    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()


if __name__ == '__main__':
    main()
