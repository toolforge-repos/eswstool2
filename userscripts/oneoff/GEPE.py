from OCRBot import OCRBot
import re


def fun(m):
    m1 = m[1].lower()
    m2 = m[2]
    return ';' + m[1] + ':' + re.sub(rf'({m1}(?:s|es|as)?)\b(?![\'}}])', rf"''\1''", m2, flags=re.I)


options = {
    # Todo esto es opcional y tiene valores por defecto
    'always': False,
    # resumen de edición
    'summary': 'Bot: arreglos varios',
    # un número del 1 al 3. 0 para ver para tipos de encabezados disponibles.
    'tipo': 2,
    # el texto del medio en un encabezado típico, o una lista de dos textos
    'head': '',
    # ‘phetools’, ‘wmfOCR’ o ‘googleOCR’. False = no OCR
    'method': False,
    # una lista de tuplas (r"regex", r"sub") para substituciones para
    'tuples': [   (r" ([Dd]e)([\d«»@#$%^&+;=?¿!—¡A-Zwxyzñ><*,.\(\)]\S)", r" \1 \2"),
        (r"(dado|[Aa]rábig[ao]|voz|voces|verbos?|del v\.|[áÁaA]r(?:abe|\.)|[Hh]ebr(?:eo|\.)|[Pp]ers(?:a|\.)|\. De) ((?!{{)(?:[a-z]+?[\d«»@#$%^&+;=?¿!¡—<>*ª\(\).,wxyzñ]\S*|\S*[\d«»@#$%^&+;=?¿!¡—<>*ª\(\).,wxyzñ]\S*[ ]?\S+|(?:[\d«»@#$%^&+;=?¿!¡—<>*ª\(\)., wxyzñA-Z]+ ?)+|[\d«»@#$%^&+;=?¿!¡—<>*ª\(\)., wxyzñ]+)) ([A-Za-z\-áéíóú]+)(?=[,.)]| [oOóÓ] | y )(dado|[Aa]rábig[ao]|voz|voces|verbos?|del v\.|[áÁaA]r(?:abe|\.)|[Hh]ebr(?:eo|\.)|[Pp]ers(?:a|\.)|\. De) ((?!{{)(?:[a-z]+?[\d«»@#$%^&+;=?¿!¡—<>*ª\(\).,wxzñ]\S*|\S*[\d«»@#$%^&+;=?¿!¡—<>*ª\(\).,wxzñ]\S*[ ]?\S+|(?:[\d«»@#$%^&+;=?¿!¡—<>*ª\(\)., wxyzñA-Z]+ ?)+|[\d«»@#$%^&+;=?¿!¡—<>*ª\(\)., wxyzñ]+)) ([A-Za-z\-áéíóú]+)(?=[,.)]| [oOóÓ] | y )(dado|[Aa]rábig[ao]|voz|voces|verbos?|del v\.|[áÁaA]r(?:abe|\.)|[Hh]ebr(?:eo|\.)|[Pp]ers(?:a|\.)|\. De) ((?!{{)(?:[a-z]+?[\d«»@#$%^&+;=?¿!¡—<>*ª\(\).,wxzñ]\S*|\S*[\d«»@#$%^&+;=?¿!¡—<>*ª\(\).,wxzñ]\S*[ ]?\S+|(?:[\d«»@#$%^&+;=?¿!¡—<>*ª\(\)., wxyzñA-Z]+ ?)+|[\d«»@#$%^&+;=?¿!¡—<>*ª\(\)., wxyzñA-Z]+)) ([A-Za-z\-áéíóú]+)(?=[,.)]| [oOóÓ] | y )",
         r"\1 {{GEPE|\2|\3}}"),
        #(r"([áa]r[áa]b[e.]|ár.) ((?!{{)(?=[a-z]+?[\d«»@#$%^&+;=?¿!—¡A-Zwxy><*\(\).,]\S*|\S*[\d«»@#$%^&+;=?¿!¡—*ª\(\).,]\S*[ ]?)\S+|[\d«»@#$%^&+;=?¿!¡—*ª\(\)., ]+)", r"\1 {{árabe|\2}}"),
               (r"^([A-Z]{4,})\b[\.,]?", lambda m: ';'+m[1][0]+m[1][1:].lower()+':'),
               (r";(\w+):([^\n]+)", lambda m: fun(m)),
               #(r"Colec\. de fueros mun....?p\.", r'Colec. de fueros municip.'),
               #(r"Ord\. de Se.\.", r'Ord. de Sev.'),
               #(r"Anot\. . Dios.\.", r'Anot. á Diosc.'),
               (r"La Gran Con.. de Ultr", r'La Gran Conq. de Ultr'),
               #(r"gr\. ([a-záéíóú\d]+)\b", r"gr. {{griego|\1}}"),
               #(r" lat\. ([a-z]+)\b", r" lat. {{latín|\1}}"),
        (r"(?<!')((?:Ord|Arch)\.[\w ]+|La Gran Conq. de Ultr|Bibl?. Ven)\b(\.?)(?![' ])", r"''\1''\2"),
        #(r", [1L],(?= \d+)", r", I,"),
        (r"D\.[^\w\s] ", r"D.ª "),
        (r"(?<=\d)\.(?:[^\w\s]|\d)", r".ª"),
        (r"\.(?=\d)", r". "),
        (r"(?<=\S)\.(?=[^,\s\-\)ªº—\|'»\.:;])", r" "),
        (r" \.", r" "),
        (r" v(?=[A-Z])", r" «"),
        (r"\s*;([ivxlcIVXLC]+):\s*", lambda m: ' ' + m[1].upper() + ' '),


    ],
    # ser aplicadas *al final* de todas las substituciones por defecto
}
start = 473
end = 575

Índice = 'Glosario etimológico de las palabras españolas (1886).djvu'
OCRBot(Índice, start, end, **options).run()
