import pywikibot, re
from pywikibot import proofreadpage
from pywikibot import pagegenerators
from pywikibot.bot import CurrentPageBot

site=pywikibot.Site('es', fam="wikisource")
site.login()

class MyBot(CurrentPageBot):

    
    update_options = {
        'always': False,
        'summary': 'Bot: OCR + arreglos varios',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        
        ppage = self.current_page
        
        only_non = True
        if only_non and ppage.exists():
            return ''
        
        N = ppage._parse_title()[2]
        ppage.header = get_header(N)

        #ppage.body = ppage.ocr(ocr_tool='wmfOCR') # ‘phetools’, ‘wmfOCR’ or ‘googleOCR’
        
        ppage.body = transform_body(ppage.body)

        print(ppage.header)
        print(ppage.body)
        ppage.save(summary=self.opt.summary)

def get_header(N):
    if N % 2 == 0:
        return "{{{{cp|{}|ELIODORO FLORES|[{}}}}}".format(str(N+130), str(N-6))
    else:
        return "{{{{cp|{}]|ADIVINANZAS|{}}}}}".format(str(N-6), str(N+130))

def transform_body(body):
    REPLACE_TUPLES = [
    (r'(?:(?<=^)|(?<=\n))(\d+) *\n+([\w \n,.;:<>/?¿¡!-]*?)\s*((?:[Cc]p\. *)*(?:D|F. C)\. *\d+[^\n]+)?(?=\n\n|$)',r'{{bc|{{c|\1|id=\1}}\n<poem>:\2</poem>{{d|\3}}}}\n'),
    (r'{{d\|}}', ''),
    (r'\n\n+', '\n\n'),
    (r'^\d+]\s+ADIVINANZAS\s+\d+\s*', ''),
    (r'^\d+\s+ELIODO[RK]O FLO[RK]ES\s+\[\d+\s*', '')]

    for x in REPLACE_TUPLES:
        body = re.sub(x[0], x[1], body)
    return body

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    index = "Adivinanzas corrientes en Chile.djvu"

    INDEX = proofreadpage.IndexPage(site, title="Index:"+index)
    
    gen_factory = INDEX.page_gen(end=179)

    MyBot(generator=gen_factory).run()

if __name__ == '__main__':
    main()