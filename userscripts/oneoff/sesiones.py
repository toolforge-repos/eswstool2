import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'text': '''{{encabezado
 |titulo         = [[../|{{ROOTPAGENAME}}]]
 |sub-titulo     = 
 |año            = 
 |autor          =
 |más info       =
 |notas          =
 |anterior       = 
 |sección        = {{SUBPAGENAME}}
 |próximo        =
}}
''',
        'summary': 'Bot: añadiendo encabezados a páginas de las Sesiones de los Cuerpos Legislativos, etc.',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        if self.current_page.depth != 2:
            pywikibot.output('DEPTH')
            return

        text = self.current_page.text
        
        if text[0:5] == '{{enc':
            pywikibot.output('ENC')
            return
        
        text = self.opt.text + text
        
        self.put_current(text, summary=self.opt.summary)

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value
    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()

if __name__ == '__main__':
    main()