import pywikibot, string, time



## extraer texto, quitar puntuación, separar por palabras,  histograma

f = open('CORPUS', 'r', encoding='utf8')

txt = f.read()
words = [x.translate(str.maketrans('ſ', 's', '"#$%\'()*+,-./:;<=>?@[\\]^_`{|}~')) for x in txt.split()] ## traducir ſ por s, mantener &, eliminar el resto de puntuación.
histogram = {}

for word in words:
	if len(word)>0: histogram[word] = histogram.get(word,0)+1

sorthist = sorted(histogram, key=histogram.get, reverse=True)

pywikibot.output('Histograma completo: ' + str(len(histogram)) + ' palabras')

## wikicosas
site = pywikibot.Site('es', 'wiktionary')

start_time = time.time()
count = 0

for word in sorthist:
	count += 1
	if count % 20 == 0: pywikibot.output('Palabras analizadas: ' + str(count) + '. Tiempo transcurrido: ' + str(time.time() - start_time))

	if histogram[word]==1: break ## si solo tiene una palabra en todo el corpus, parar acá. 
	pageexists = pywikibot.Page(site, word).exists()
	if not pageexists:
		pywikibot.output(word)
		with open('LIST', 'a', encoding='utf8') as t:
			t.write('*[['+word+']]: '+str(histogram[word])+'\n')

