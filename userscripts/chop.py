import os
import copy
from PyPDF2 import PdfWriter, PdfReader


def chop(filename):
    filepath = os.path.join(os.getcwd() + '\\PDF', filename)

    with open(filepath, "rb") as pdf1:
        pdf = PdfReader(pdf1)
        output = PdfWriter()
        numpages = len(pdf.pages)
        #page2skip = int(input('Insert how many pages do you want to skip: '))
        page2skip = 0
        for i in range(page2skip, numpages):
            page = pdf.pages[i]
            pagebis = copy.copy(page)
            page.mediabox.upper_left = (
                page.mediabox.right / 2, page.mediabox.top,)
            output.add_page(page)
            pagebis.mediabox.upper_right = (
                pagebis.mediabox.right / 2, pagebis.mediabox.top,)
            output.add_page(pagebis)

        filepath = os.path.join(os.getcwd() + '\\PDF', f'_chop_{filename}')
        with open(filepath, "wb") as newpdf:
            output.write(newpdf)


def main(*args: str) -> None:
    chop("Escritores en prosa anteriores al siglo XV - bdh0000246527.pdf")


if __name__ == '__main__':
    main()
