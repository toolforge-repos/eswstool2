import pywikibot, re
from pywikibot import pagegenerators, FilePage
from pywikibot.bot import CurrentPageBot #ExistingPageBot #CurrentPageBot
import time

def treat(page):
    REXP = r'{{[ ]*Information(?=\s*\|)'
    if re.search(REXP, page.text):
        page.text = re.sub(REXP, r'{{Book' , page.text, flags=re.I)
        page.save('Changing {{information}} to {{book}} in book files')
        time.sleep(40)
    else:
        return
def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    options = {}
    args += ('-cat:Índices no conectados a Wikidata',)

    args = pywikibot.handle_args(args)
    site=pywikibot.Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator()
    Commons = pywikibot.Site('commons', 'commons')
    for page in gen:
        filepage = FilePage(Commons, 'File:'+page.title(with_ns=False))
        treat(filepage)


if __name__ == '__main__':
    main()