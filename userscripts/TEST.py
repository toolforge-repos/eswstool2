import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot import Page, Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot


class MyBot(CurrentPageBot):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: eliminando {{ma}} del espacio Página:',
        'custom': 'This is the value of a custom -custom:parameter'
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = re.sub(
            r"\s*{{(?:modernización automática|ma)}}\s*", "",
            page.text, flags=re.I)

        if not re.search('</ref>', text):
            text = re.sub('<references/>', '', text)
        self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    gen_factory.handle_arg('-transcludes:modernización automática')
    gen_factory.handle_arg('-ns:102')

    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
