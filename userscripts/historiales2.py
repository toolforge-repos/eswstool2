import pywikibot
import sys
from datetime import datetime
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

pywikibot.Site().login()
TALK_MESSAGE = '\n\n== Historial ampliado ==\n<!-- Generado en ' + \
    str(datetime.now()) + '-->\n{{historial ampliado|{{/historial}}}}'
HISTORIALES = ''


class MyBot(ExistingPageBot):

    def treat_page(self):
        global HISTORIALES

        P = self.current_page
        result = "'''[[" + P.title() + "]]'''\n<table>\n<tr><th>oldid</th><th>timestamp</th><th>Usuario</th><th>Resumen de edicion</th></tr>"

        for entry in P.revisions():
            result += '<tr>'
            result += ('<td> {r.revid} </td><td> {r.timestamp} </td><td> {r.user} </td><td> '
                       '<nowiki>{r.comment}</nowiki></td></tr>'.format(r=entry))
        result += '</table>\n\n'
        HISTORIALES = HISTORIALES + result

        P.delete(reason="Página fusionada", deletetalk=True, prompt=True)


def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options

    for arg in local_args:
        opt, sep, value = arg.partition(':')

        if opt in ('-summary', '-text'):
            options[opt[1:]] = value
    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()

    global HISTORIALES
    FileName = "output.txt"
    file = open(FileName, 'w')
    file.write(HISTORIALES)

    for arg in sys.argv[1:]:
        if arg[:7] == '-target':
            target = arg[8:].strip('"')

    if target:
        target_talk = pywikibot.Page(pywikibot.Site(), title=target, ns="Talk")
        target_talk.text += TALK_MESSAGE
        target_talk.save('Agregando historial ampliado')

        target_talk_history = pywikibot.Page(
            pywikibot.Site(), title=target + '/historial', ns="Talk")
        target_talk_history.text = HISTORIALES
        target_talk_history.save('Agregando historial ampliado')


if __name__ == '__main__':
    main()
