import pywikibot, re, roman
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

class MyBot(ExistingPageBot):


    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: limpiando [[:Categoría:Páginas con errores en las referencias]]',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        text = self.current_page.text
        
        EMPTY_REFS_REGEXP = r'<\s*ref\s*name\s*=\s*(\"[^\"]+\"|\'[^\"]+\'|\S+)\s*>\s*<\s*\/\s*ref\s*>'
        LOWER_ALPHA_REGEXP = r'(<ref\s*group\s*=\s*[\"\']*\s*lower-alpha\s*[\"\']*\s*>.*?<noinclude></noinclude>)'

        text = re.sub(EMPTY_REFS_REGEXP, r'<ref name=\1>{{zw}}</ref>', text)
        text = re.sub(LOWER_ALPHA_REGEXP, r'\1{{listaref|group=lower-alpha}}</noinclude>', text)
        

        self.put_current(text, summary=self.opt.summary)

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value
    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()

if __name__ == '__main__':
    main()