#!/usr/bin/env python
'''
Created on 7 jul 2024

@author: WS
'''
from pypdf import PdfWriter, PdfReader
from os.path import exists
from tqdm import tqdm
import winsound


def bad_chars(s):
    bad = '#<>[]|{}:¿¡?!"/'
    bad_map = {c: '' for c in bad}
    return s.translate(str.maketrans(bad_map))


class PDF(object):
    '''
    classdocs
    '''
    pdfmetadata = {}  # Metadata dictionary
    total_pages = 1

    def __init__(self, filename):
        self.filename = filename

    def beep(self):
        duration = 1000  # Duración en milisegundos
        freq = 440  # Frecuencia en Hertz
        winsound.Beep(freq, duration)

    def crop(self, left=0, top=0, right=0, bottom=0, overwrite=True):
        input_pdf = self.filename
        output_pdf = self.filename[:-4] + "_crop.pdf"

        # Normalizar unidades, de milímetros a puntos

        left = left * 2.83465
        top = top * 2.83465
        right = right * 2.83465
        bottom = bottom * 2.83465

        if exists(output_pdf) and not overwrite:
            return

        reader = PdfReader(input_pdf)
        writer = PdfWriter()
        # Crear una barra de progreso
        # Recorrer todas las páginas del PDF
        for page_num in tqdm(range(len(reader.pages)), desc="Cropping..."):
            page = reader.pages[page_num]

            # Cropping
            page.cropbox.left = float(page.cropbox.left) + left
            page.cropbox.top = float(page.cropbox.top) - top
            page.cropbox.bottom = float(page.cropbox.bottom) + bottom
            page.cropbox.right = float(page.cropbox.right) - right

            # Add page to output file
            writer.add_page(page)

        # Write to output file
        # writer.add_metadata(self.pdfmetadata)
        writer.write(output_pdf)
        writer.close()
        print("File " + output_pdf + " is done.")
        self.beep()


def main(*args: str) -> None:
    pdf = PDF('PDF/A través del Atlántico en globo - iai1859671837.pdf')
    pdf.crop_pdf(top=20, bottom=20, right=20, left=20)


if __name__ == '__main__':
    main()
