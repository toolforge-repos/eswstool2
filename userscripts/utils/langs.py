def lang_cat_name(code):

    return {
        'en': "English",
        'la': "Latin",
        'de': "German",
        'es': "Spanish",
        'pi': "Pali"
    }[code]


def lang_QID(code):
    return {
        'es': 'Q1321',
        'en': 'Q1860'
    }[code]
