import os
import zipfile
import requests
from tqdm import tqdm
from random import randint
import pywikibot
from pywikibot import Site
from pywikibot.specialbots import UploadRobot


def download_pdf_file(url: str, filename: str, chunk_size=1024 * 1024, verify=True, retry=False):

    response = requests.get(url,
                            stream=True,
                            headers={'User-agent': 'SpanishWikisourceBot 0.1'},
                            verify=verify)

    if response.status_code == 200:
        # Save in current working directory
        total = int(response.headers.get('content-length', 0))
        # if total < 1024:
        #    return

        # Crear directorio
        os.makedirs('PDF', exist_ok=True)

        filepath = os.path.join(os.getcwd() + '\\PDF', filename)
        try:
            if not retry and os.path.isfile(filepath):
                print(f'{filename} ya se descargó antes.')
                return filepath
            with open(filepath, 'wb') as file, \
                tqdm(desc=filename, total=total, unit='iB',
                     unit_scale=True, unit_divisor=1024,) as bar:
                for data in response.iter_content(chunk_size=chunk_size):
                    size = file.write(data)
                    bar.update(size)
                print('\a')
                return filepath
        except requests.exceptions.ChunkedEncodingError:
            # retry con un número aleatorio para chunk_size
            filepath = download_pdf_file(
                url=url, filename=filename,
                chunk_size=randint(1, 1024) * 1024, verify=verify, retry=True)
            return filepath
    else:
        print(f'Uh oh! Could not download {url},')
        print(f'HTTP response status code: {response.status_code}')
        return


def upload_to_commons(path, filename, commonsText, query=None, verify=True):
    # cada scraper debe tener exactamente la misma estructura para que funcione
    Commons = Site('commons', 'commons')
    if query:
        search = Commons.search(
            query, namespaces='File', total=1)
        for result in search:
            title = result.title()
            pywikibot.output(
                f'"Al parecer ya está subido en Commons: <<lightred>>{title}')
            choice = pywikibot.input_choice(
                " Saltar?", (('Sí', 'S'), ('No', 'N'), ('Usar este', 'U')))
            if choice == 's':
                return
            elif choice == 'u':
                return result.title()

    bot = UploadRobot(path, description=commonsText, use_filename=filename,
                      chunk_size=1024 * 1024, verify_description=verify,
                      target_site=Commons)

    # bot.last_filename QEPD
    bot.filename = ''

    def _(old, new):
        bot.filename = new

    bot.post_processor = _
    try:
        bot.run()
    except pywikibot.exceptions.APIError:
        return filename
    except Exception as exc:
        print(exc)
        bot.chunk_size = 1024 * 1024
        bot.run()

    print('\a')
    return bot.filename


def extract_zip_to(zip_fo, dir_name, excluder=None):

    with zipfile.ZipFile(zip_fo) as zip_ref:
        if not excluder:
            zip_ref.extractall(dir_name)
        else:
            for member in zip_ref.infolist():

                if not excluder(member):
                    zip_ref.extract(member, dir_name)
