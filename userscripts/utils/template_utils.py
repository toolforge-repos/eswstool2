import re


def get_index_data(text, param):
    return get_template_data(text,
                             ':MediaWiki:Proofreadpage_index_template',
                             param)


def set_index_data(text, param, setto, **kwargs):
    return set_template_data(text,
                             ':MediaWiki:Proofreadpage_index_template',
                             param, setto, compact=True, **kwargs)


def get_template_data(text, template, param):
    """Summary

    Args:
        page (TYPE): Description
        template (TYPE): Description
        parameter (TYPE): Description

    Returns:
        TYPE: Description
    """
    # Regex que captura el parametro de una plantilla... muy esotérico
    # 1: captura la existencia de la plantilla
    REXP_0 = r'{{ *###'.replace('###', template)
    # 2. captura el parámetro
    REXP = REXP_0 + \
        r'(?>[^{}]|{+(?>[^{}]+|{+[^}]+}+)+}+|{+[^}]+}+|\[+[^\]]+\]+)*\|\s*&&&\s*=\s*((?>[^|{}\[\]]|{+[^}]+}+|\[+[^\]]+\]+)*)\s*(?=\||}})'.replace('&&&', param)

    if not re.search(REXP_0, text, flags=re.I):
        return False    # False = No hay plantilla

    if m := re.search(REXP, text, flags=re.I):
        return m[1].strip()
    else:
        return None     # None  = Hay plantilla, no hay parámetro


def set_template_data(text, template, param, setto,
                      overwrite=False, create=False, delete=False,
                      compact=False):
    """Summary

    Args:
        page (TYPE): Description
        param (TYPE): Description
        setto (TYPE): Description
        overwrite (bool, optional): Description
        create (bool, optional): Description
        delete (bool, optional): Description

    Returns:
        TYPE: Description
    """
    value = get_template_data(text, template, param)
    if value is False:   # no hay Book
        return text

    if value:
        escaped = re.escape(value)
    else:
        escaped = ''
    REXP = r'({{ *###(?>[^{}]|{+[^}]+}+|\[+[^\]]+\]+)*)((?:\|\s*&&&\s*=\s*))'\
           r'~~~\s*(?=\||}})'
    REXP = REXP.replace('###', template)
    REXP = REXP.replace('&&&', param)
    REXP = REXP.replace('~~~', escaped)

    Param = rf'{param[0].upper()}{param[1:]}'

    if compact:
        n = 0
    else:
        n = 12
    if setto and value is None and create:
        # con cadenas vacías el REXP falla
        REXP1 = REXP.replace('))', ')?)')
        text = re.sub(
            REXP1, rf'\1|{"":{n/12}}{Param:{n}}={setto}\n', text, 1, flags=re.I)
    elif value == '':
        text = re.sub(
            REXP, rf'\1|{"":{n/12}}{Param:{n}}= {setto}\n', text, 1, flags=re.I)
    elif not setto and delete:
        text = re.sub(REXP, rf'\1|{"":{n/12}}{Param:{n}}=\n', text, 1, flags=re.I)
    elif value and overwrite:
        text = re.sub(REXP,
                      rf'\1|{"":{n/12}}{Param:{n}}= {setto}\n', text, 1, flags=re.I)
    return text
