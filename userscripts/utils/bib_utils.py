import re
import pywikibot
import utils.string_utils as su
import utils.langs as langs
import Data


TRANSLATED = {'Titulo': 'title', 'Subtitulo': 'subtitle',
              'Volumen': 'volume', 'Autor': 'author', 'Editor': 'editor',
              'Prologuista': 'introducer', 'Traductor': 'translator',
              'Imprenta': 'printer', 'Editorial': 'publisher',
              'Ilustrador': 'illustrator', 'Ano': 'date', 'Lugar': 'city',
              'Fuente': 'source', 'Imagen': 'image_page',
              'Wikidata': 'wikidata',
              'Paginas': '_pagelist', 'Progreso': '_progreso'}

ROLES = ['author', 'translator', 'editor', 'illustrator', 'introducer']

PUBLISHERS = [
    (r'La Tribuna Nacional', 'Q19448759'),
    (r'Espasa[\-—– ]*Calpe', 'Q16912403'),
    (r'Calpe\b', 'Q5779674'),
    (r'J(?:acobo|\.?) Peuser', 'Q45835307'),
    (r'\bConi\b', 'Q19469522'),
    (r'Espasa', 'Q17629741'),
    (r'del Ferrocarril\b', 'Q19446883'),
    (r'Montaner y Sim[oó]n', 'Q50895561'),
    (r'Rivadeneyra', 'Q19458204'),
    (r'Mundo Latino', 'Q19465976'),
    (r'Maucci', 'Q19458589'),
    (r'\bSempere\b', 'Q107565786'),
    (r'D\.? *Appleton', 'Q3011053'),
    (r'billetes de banco', 'Q19422451'),
    (r'Fortanet', 'Q19491061'),
    (r'Victoriano Su[áa]rez', 'Q19493133'),
    (r'S\.* Manero', 'Q123494665'),
    (r'Garnier Hermanos', 'Q19444101'),
    (r'Tipograf[ií]a Americana', 'Q19490872'),
    (r'Imprenta Cervantes', 'Q19446727'),
    (r'[ÁA]ngel Estrada', 'Q19494414'),
    (r'Luis Tasso', 'Q19457472'),
    (r'Revista de Occidente', 'Q124984164'),
    (r'Imprenta Bol[íi]var', 'Q19446715'),
    (r'\bCalleja\b', 'Q19274715'),
    (r'Sociedad Cient[íi]fica Argentina', 'Q9078455'),
    (r'Sopena', 'Q30000169'),
    (r'España Moderna', 'Q17339743'),
    (r'Biblioteca Nueva', 'Q5818602'),
    (r'Cooperativa Editorial "?Buenos Aires"?', 'Q110320225'),
    (r'(?:Suc.+|Viuda) de Hernando', 'Q19493130'),
    (r'Perlado.? P[aá]ez', 'Q19493130'),
    (r'Miguel Ginesta', 'Q125208041'),
    (r'Cervantes', 'Q19446727'),
    (r'Imprenta Universitaria', 'Q19446779'),
    (r'Juan (?:A.? )?Alsina', 'Q130209915'),
    (r'Prometeo', 'Q77956577'),
    (r'Cosm[óo]polis', 'Q111628900'),
    (r'Villalpando', 'Q15126130'),
    (r'Garnier', 'Q733801'),
    (r'Imprenta,* Litograf[ií]a [yi] Encuadernaci[óo]n [«"]*Barcelona["»]¨*', 'Q19417362'),
    (r'Bailly.?Bailli.re', 'Q45029787'),
    (r'Lajouane', 'Q19443991'),
    (r'Elzeviriana', 'Q19446736'),
    (r'Gaspar y Roig', 'Q19444127'),
    (r'Nascimento', 'Q19788728'),
    (r'Igon H', 'Q64917876'),
    (r'^UN$', 'Q1065'),
    (r'Editorial Am[ée]rica', 'Q19434158'),
    (r'Renacimiento', 'Q19446777'),
    (r'Dubrull', 'Q65128427'),
    (r'Claridad', 'Q19434164'),
]

CITIES = [
    # Argentina
    (r'Buenos[ \-]*Aires', 'Buenos Aires'),
    # Chile
    (r'La Serena', 'La Serena'),
    (r'Santiago(?: de Chile|, Chile)?', 'Santiago de Chile'),
    (r'Valpara[íi]so', 'Valparaíso'),
    (r'Concepci.n', 'Concepción'),
    # Colombia
    (r'Bogot[áa]', 'Bogotá'),
    # Costa Rica
    (r'San Jos[eé]', 'San José'),
    # Cuba
    (r'(?:La )?Habana', 'La Habana'),
    # México
    (r'M[ée][xjg]ico(?: City|D[\. ]*F[\. ]*)?', 'México'),
    (r'Vera[ ]*Cruz', 'Veracruz'), (r'Puebla', 'Puebla'),
    (r'Guadalajara', 'Guadalajara'), (r'Monter+ey', 'Monterrey'),
    (r'Zacatecas', 'Zacatecas'),
    # Panamá
    (r'Panam[áa]', 'Panamá'),
    # Perú
    (r'Lima', 'Lima'),
    # Uruguay
    (r'Montevideo', 'Montevideo'),
    # Venezuela
    (r'Caracas', 'Caracas'),
    # España
    (r'Madrid', 'Madrid'), (r'Barcelona', 'Barcelona'), (r'Murcia', 'Murcia'),
    (r'Se[uv]illa', 'Sevilla'), (r'Valencia', 'Valencia'),
    (r'Tarragona', 'Tarragona'), (r'Zaragoza', 'Zaragoza'),
    (r'C[áa]diz', 'Cádiz'), (r'M[áa]laga', 'Málaga'),
    # Otros
    (r'(?:Nueva|New) York', 'New York'), (r'Lond(?:on|res)', 'London'),
    (r'Par[ií]s', 'Paris'), (r'Rom[ae]', 'Rome'),
]

QIDCITIES = {
    # Argentina
    'Buenos Aires': 'Q1486',
    # Chile
    'Santiago de Chile': 'Q2887',
    'Valparaíso': 'Q33986', 'La Serena': 'Q14467',
    'Concepción': 'Q1880',
    # Colombia
    'Bogotá': 'Q2841',
    # Cuba
    'La Habana': 'Q1563',
    # Costa Rica
    'San José': 'Q3070',
    # México
    'México': 'Q1489', 'Veracruz': 'Q173270',
    'Puebla': 'Q125293',
    'Guadalajara': 'Q9022', 'Monterrey': 'Q81033', 'Zacatecas': 'Q139242',
    # Panamá
    'Panamá': 'Q3306',
    # Perú
    'Lima': 'Q2868',
    # Uruguay
    'Montevideo': 'Q1335',
    # Venezuela
    'Caracas': 'Q1533',
    # España
    'Madrid': 'Q2807', 'Barcelona': 'Q1492', 'Murcia': 'Q12225',
    'Sevilla': 'Q8717', 'Valencia': 'Q8818', 'Tarragona': 'Q15088',
    'Zaragoza': 'Q10305', 'Cádiz': 'Q15682', 'Málaga': 'Q8851',
    # Otros
    'New York': 'Q60', 'London': 'Q84', 'Paris': 'Q90',
    'Rome': 'Q220',
}


def get_city(data):
    if not data:
        return
    name = data.name
    for C in CITIES:
        if re.fullmatch(C[0], name, flags=re.I):
            city = C[1]
            data.QID = QIDCITIES.get(city)
            return data


def get_publisher(data, bot=False):
    if not data:
        return

    value = data.name

    if not value:
        return

    #{{institution:}}
    if m := re.search(r'(Q\d+)', value):
        data.QID = m[1]
        return data
    value = re.sub(r"{{\w+:(.*?)}}", r"\1", value)
    data.name = value
    for REXP in PUBLISHERS:
        if re.search(REXP[0], value, flags=re.I):
            data.QID = REXP[1]
            return data

    if bot:
        return data
    Wikidata = pywikibot.Site('wikidata', 'wikidata')
    search = Wikidata.search_entities(value, 'es', total=3, uselang='es')
    lista = []

    for i, result in enumerate(search):
        pywikibot.output(f"<<lightyellow>>[{i + 1}] {result['id']:10} "
                         f"<<lightgray>> {result['label']} : "
                         f" {result.get('description', '')}")
        lista.append(result['id'])

    if len(lista) > 0:

        i = pywikibot.input(f'{value}: cualquier letra = ninguno')
        if i.isdigit():
            data.QID = lista[int(i) - 1]
            return data

    return data


def get_ID(site, title):
    page = pywikibot.Page(site, title)
    ID = None
    if page.exists():
        if page.isRedirectPage():
            page = page.getRedirectTarget()
        if page.isDisambig():
            return ID
        try:
            ID = page.data_item().getID()
        except Exception:
            pass
    return ID


def search_QID(value):
    if not value:
        return
    if m := re.search(r"(Q\d+)", value):
        return m[1]
    if re.search(r'{{[Aa]nonymous}}|[Aa]n[óo]nimo', value):
        return

    QID = None

    value = value.strip(" :")
    # Remueve creator
    value = re.sub(
        r'\s*{{[ ]*[Cc]reator[ ]*[:\|][ ]*(.*?)[ ]*}}\s*', r'\1', value)
    # Remueve enlaces wiki, deja el primer componente
    value = re.sub(r'\[\[([^|\]]+)(?:\|[^|\]]*)?\]\]', r'\1', value)
    # Remueve NS:
    value = re.sub(r'^[A-Z][a-z]+:', r'', value)

    value = value.strip(" :")

    page = pywikibot.Page(pywikibot.Site(
        'commons', 'commons'), 'Creator:' + value)
    if page.exists():
        if page.isRedirectPage():
            page = page.getRedirectTarget()
        if m := re.search(r'(Q\d+)', page.text):
            QID = m[1]
            return QID

    if wspage := get_ID(pywikibot.Site('es', 'wikisource'), 'Autor:' + value):
        QID = wspage

    elif wikipage := get_ID(pywikibot.Site('es', 'wikipedia'), value):
        QID = wikipage

    elif enwspage := get_ID(pywikibot.Site('en', 'wikisource'), 'Author:' + value):
        QID = enwspage

    return QID


def process_dict(D):
    """Summary

    Args:
        D (TYPE): Description

    Returns:
        TYPE: Description
    """
    P = {}
    D['_sources'] = D.get('_sources', {}) | process_source(D.get('source'))

    for param, value in D.items():
        if isinstance(value, Data.Newtype):
            P[param] = value
        else:
            P = process_param(D, P, param, value)
    for param in D:
        if param not in P:
            P[param] = D[param]
    return P


def process_param(D, P, param, value):

    pairings = {'title': process_title,
                'volume': process_volume,
                'publisher': process_publisher,
                'printer': process_publisher,
                'edition': process_edition,
                'language': process_language,
                }

    if param in pairings:
        P[param] = pairings[param](D.get(param))

    if param in ROLES:
        P[param] = preprocess_humans(D.get(param))
    if param == 'printer':
        P['printer'] = P['printer'][0]
    if param == 'publisher':
        P['city'] = D.get('city') or P['publisher'][1]
        P['publisher'] = P['publisher'][0]

    if param == 'title':
        P['subtitle'] = D.get('subtitle') or P['title'][1]
        P['title'] = P['title'][0]

    return P


def process_language(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return ''

    REXP = r"\s*{{[ ]*[Ll]anguage\|[ ]*(?:1[ ]*=[ ]*)?(.+?)}}\s*"

    value = re.sub(REXP, r"\1", value)

    return langs.lang_QID(value)


def process_title(value):
    '''Título: subtítulo -> ['Título', 'subtítulo']

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    '''

    subtitle = ''
    value = re.sub(r"\[\[(?:[^\]\|]+\|)?([^\]\|]+)\]\]", r"\1", value)
    value = re.sub(r"{{es[ ]*\|(?:[ ]*1[ ]*=[ ]*)?([^}]*)}}", r"\1", value)
    value = re.sub(r"\[.*?\]\.*$", r"", value)          # [microform].
    value = re.sub(r"\[*microform\]*\.*$", r"", value)          # [microform].

    # Metadata Faeiana y sus gringadas
    value = re.sub(r"!(.{3,40}!)", r"¡\1", value)
    value = re.sub(r"\?(.{3,40}\?)", r"¿\1", value)

    if re.search(r"[;:]", value):                       # Título: subtítulo
        value, subtitle = re.split(r'[:;]', value, maxsplit=1)
        value = value.strip()
        subtitle = subtitle.strip()

    elif match := re.search(r"(\w[\w ]+)\(([\w ]+)\)", value):
        value = match[1].strip()
        subtitle = match[2].strip()

    return (value.strip(" .,[]'"), subtitle.strip(" .,[]'"))


def process_publisher(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return ('', '')
    value = re.sub(
        r'\s*\[\[(?::?\w+:)?(.+?)(?:\|.+?)?\]\]\s*', r"\1", value)
    value = re.sub(r"^(?:Impres+o )?En ", "", value, flags=re.I)
    if value and re.search(r'{{ *\w\w+ *\| *(.*) *}}', value):
        return (value, '')
    pub = value
    for C in CITIES:
        if re.fullmatch(C[0], pub, flags=re.I):
            city = C[1]
            return ('', city)

    city = ''
    separator = r"([:;,])"
    if re.search(separator, value):  # Si hay coma, o dos puntos: separar
        match = re.split(separator, value, maxsplit=1)
        match = [x.strip("[ ].") for x in match]
        city, sep, pub = match
        if re.fullmatch(r'[Ss]\.? ?[Nn]\.? ?', pub):
            pub = ''
        # Si está en la lista, devuelve versión canónica
        for C in CITIES:
            if re.match(C[0], city, flags=re.I):
                city = C[1]
                return (pub, city)

    if re.fullmatch(r'[Ss]\.? ?[Nn]\.? ?', pub):
        pub = ''

    return (pub, '')


def process_volume(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return ''

    # old_value = value
    # {{es|1=}}
    value = re.sub(r"{{es[ ]*\|(?:[ ]*1[ ]*=[ ]*)?([^}]*)}}", r"\1", value)
    value = re.sub(r" \(*\d\d\d\d\)*", "", value)
    # saca volume(n)
    value = re.sub(r"^(?:volumen*|tomo|vol\.) *", "", value, flags=re.I)
    value = re.sub(r"^[tv]\.* *(?=\d)", "", value, flags=re.I)

    value = re.sub(r"^([\w]+).*", r"\1", value)
    value = value.lstrip('0')

    '''No.
    # Number queda en entredicho... sólo para ediciones periódicas?

    # separator = r"((?:, )*[Nn]o.* *)"
    # if re.search(separator, value):
    #     vol, sep, num = [x.strip("[ ].")
                           for x in re.split(separator, value, maxsplit=1)]

    #     if vol.isdigit() and num.isdigit():
    #         return (vol, num)
    '''

    # Romanos a arábicos
    import roman

    def is_roman(s):

        try:
            roman.fromRoman(s)
            return True
        except Exception:
            return False

    if is_roman(value):
        value = str(roman.fromRoman(value))

    # lenguaje natural
    t = [r'primer.*|uno', r'segund.*|dos', r'tercer.*|tres',
         r'[qc]uart.*|[qc]uatro', r'quint.*|cinco', r'se[sx]t.*|seis',
         r's.p?tim.*|siete', r'oc?ta[uv].*?|ocho',
         r'no[uv]en.*?|nueve', r'd[ée]cim.*?|diez']

    for i, v in enumerate(t):
        if re.match(v, value):
            return str(i + 1)

    return value


def process_edition(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return ''
    value = re.sub(r"{{es[ ]*\|(?:[ ]*1[ ]*=[ ]*)?([^}]*)}}",
                   r"\1", value)  # {{es|1=}}

    value = re.sub(r"^(\d+)[a-zª]+(?: ed\.)?$", r"\1", value)  # 2nd, 3rd, 1ra

    # if value.isdigit(): return value

    return value


def process_source(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return {}
    value = ' '.join(value.split())  # remueve molestos saltos de línea

    idrexps = [
        ('ia_id', r"https://archive\.org/details/([\S]*)"),
        ('ia_id',
         r"\{\{ *(?:IA|Internet Archive link) *\|"
         r"(?: *1 *= *)?(.*?)(?:\s*\|.*?)*\}\}"),
        ('bdh_id',
         r"\{\{ *BDH *\| *1* *=* *http:\/\/bdh.bne.es\/"
         r"bnesearch\/detalle\/(.*?) *\}\}",),
        ('gb_id',
         r"https?://books\.google\.com/books\?id=([0-9A-Za-z_-]{12})"),
        ('gb_id', r"\{\{ *(?:gbs*|google book search link|google books link)"
            r" *\|(?: *1 *= *)?(.*?)(?:\s*\|.*?)*\}\}"),
        ('galiciana_id',
         r"\{\{ *(?:galiciana) *\|(?: *1 *= *)?http://biblioteca\.galiciana"
         r"\.gal/(?:es|gl)/consulta/registro\.do\?id=(.*?)(?:\s*\|.*?)*\}\}"),
        ('hathi', r"https?://catalog.hathitrust.org/Record/(\d+)")
    ]

    d = {}

    for t in idrexps:
        if match := re.search(t[1], value, flags=re.I):
            d[t[0]] = match[1]

    # if len(d) == 0:
    #     print(value)
    d['oldstring'] = value
    return d


def preprocess_humans(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return ''
    # eliminar comentarios HTML
    value = re.sub(r'<!--.*?-->', '', value)
    # eliminar enlaces internos
    value = re.sub(
        r'\s*\[\[(?::?\w+:)?(.+?)(?:\|.+?)?\]\]\s*', r"\1\n", value)
    # separar {{creator}}s
    REXP = r'({{ *[Cc]reator *[:\|] *(?:[Ww]ikidata *= *)?.*? *}})\s*'
    value = re.sub(REXP, lambda m: m[1].strip(" \n") + '\n', value)
    return value


def process_human(value, role=None, QID=None):
    # Returns: value, role, QID
    if not value:
        return value, role, QID

    value = value.strip(" :*.")

    if m := re.search(r"(Q\d+)", value):
        return value, role, m[1]

    if re.match(r'{{ *(?:[Uu]nknown|[Aa]nonymous|[Aa]uthor)', value):
        return "UNKNOWN", role, QID

    if m := re.match(r"{{ *[Cc]reator[:|] *(?![wW]ikidata)([^}]+)}}", value):
        QID = search_QID(m[1])
        return m[1], role, QID

    TUPLES = [
        # {{unknown|author}} <cómo dato desconocido en wikidata?>

        (r"\.*\s*\[.*?\]\s*", ""),  # [from old catalog]
        (r"\.*(?: n[or]? *\d+)+", ""),  # (. nr|no 43284234798237)

        (r"(?:, )?\(?(?:n|f|m|fl|d|b|d|ca)\. \d+\?*\)?\.*", ""),  # (n. 1899)
        (r"(?:, )?\(?(?:\d+\?*-\d*\?*|\d*\?*-\d+\?*)\)?\.*", ""),  # (1892-1999)

        (r", (?:n|f|m|fl|d|b|d|ca)\. \d+\?*\.*", ""),  # , n. 1899|f. 1899
        (r", (?:\d+\?*-\d*\?*|\d*\?*-\d+\?*)\.*", ""),  # , 1882-1999 etc..

        (r", (?:\d+th cent(?:ury)*)\.*", ""),  # (19th cent)
        # R. de (Rafael)
        (r"([A-Z])\.( \w*)? \((\1\w+)\)", r"\3\2"),
        # M. Rigoberto (Manuel Rigoberto)
        (r"([A-Z])\.( \w*)? \((\1\w+\2)\)", r"\3"),
        # José C. de (José Carmen)
        (r"(\w+ [A-A])\.( \w*)? \((\1\w+)\)", r"\3\2"),
        # Ramón A. (Ramón Arminio)
        (r"([A-Z]\w+) ([A-Z]). \((\1 \2\w+)\)", r"\3\2"),
        (r"([A-Z])\. *([A-Z]). \((\1\w+ \2\w+)\)", r"\3"),  # W. H. (Wo Ho)
        (r"([A-Z]\w+ [A-Z]\b)(?!\.)", r"\1."),  # José T -> José T.
        (r"([A-Z]\. *[A-Z]\b)(?!\.)", r"\1."),  # E. L -> E. L.

        (r' +- +', ', ')
    ]

    for t in TUPLES:
        value = re.sub(*t, value)
    # ROLES
    if m := re.search(r'(?: -|,)* (?:auth*or)\b\.*', value):
        value = re.sub(m.re, '', value)
        role = 'author'
    elif m := re.search(r'(?: -|,)* (?:tr(?:ad(?:uctor)?|anslator)?)\b\.*', value):
        value = re.sub(m.re, '', value)
        role = 'translator'
    elif m := re.search(r'(?: -|,)* (?:il+ustra[dt]or)\b\.*', value):
        value = re.sub(m.re, '', value)
        role = 'illustrator'
    elif m := re.search(r'(?: -|,)* (?:ed(?:itor)?|compilador)\b\.*', value):

        value = re.sub(m.re, '', value)
        role = 'editor'
    elif m := re.search(r'(?: -|,)* (?:prologuista)\b\.*', value):
        value = re.sub(m.re, '', value)
        role = 'introducer'

    if ',' in value:
        value = [x.strip() for x in value.split(',')]
        value = value[::-1]   # invertir
        value = " ".join(value).strip()

    return value, role, QID


def commons_to_dict(text):
    temps = pywikibot.textlib.extract_templates_and_params(text, strip=True)

    params = [t[1] for t in temps if t[0].lower() == 'book']
    if len(params) == 0:
        return False

    D = {}
    for k, v in params[0].items():
        if v:
            D[k.lower().replace(" ", "_")] = su.clean(v).strip()

    return D


def ws_to_dict(text):
    temps = pywikibot.textlib.extract_templates_and_params(text, strip=True)
    params = [t[1] for t in temps if t[0].lower(
    ) == ':mediawiki:proofreadpage_index_template']
    if len(params) == 0:
        return False

    D = {}
    for k, v in params[0].items():
        if v:
            k = TRANSLATED.get(k)
            if k:
                D[k.lower().replace(" ", "_")] = su.clean(v).strip()

    return D


def main(*args: str):
    print(process_human('Nuñez de Arce, Pedro, 1956-1999 - editor'))


if __name__ == '__main__':
    main()
