import re
from dateparser import DateDataParser
from pywikibot import WbTime


def parse(text, settings=None):
    return DateDataParser(settings=settings).get_date_data(text)


def text_to_WbTime(text):
    settings = {'PREFER_DAY_OF_MONTH': 'first',
                'PREFER_MONTH_OF_YEAR': 'first'}
    datedata = parse(text, settings=settings)
    obj = datedata.date_obj
    if not obj:
        return
    precisions = {'day': 11, 'month': 10, 'year': 9}
    wbtime = WbTime(year=obj.year,
                    month=obj.month,
                    day=obj.day,
                    precision=precisions[datedata.period])
    return wbtime