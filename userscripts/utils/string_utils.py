import re


def combine_list(items, final_joiner="and"):

    if len(items) > 1:
        text = ", ".join(items[:-1]) + " " + final_joiner + " " + items[-1]
    elif len(items):
        text = items[0]
    else:
        text = ""

    return text


def bad_chars(s):
    s = str(s)
    bad = '#<>[]|{}:.¿¡?!"/\\'
    bad_map = {c: '' for c in bad}
    return s.translate(str.maketrans(bad_map))


def clean_codecs(text):
    d = {"Ã©": "é",
         "Ã": "Á",
         "Ã¡": "á",
         "Ã‰": "É",
         "Ã©": "é",
         "Ã": "Í",
         "Ã­": "í",
         "Ã“": "Ó",
         "Ã³": "ó",
         "Ãš": "Ú",
         "Ãº": "ú",
         "Ã‘": "Ñ",
         "Ã±": "ñ",
         "Â¿": "¿",
         "©Ł": 'á',
         '©♭': 'é',
         "©Ư": 'í',
         "n̋": "ñ",
         "©ł": 'ñ',
         'á': 'á',  # acento extra raro
         'é': 'é',
         'í': 'í',
         'ó': 'ó',
         'ú': 'ú',
         'ñ': 'ñ',
         '[\u0300-\u036F]': ''  # diacríticos combinados
         # r'[\x00-\x1f\x7f-\x9f]': '', #caracteres de control
         }
    pattern = '|'.join(sorted(k for k in d))
    text = re.sub(pattern, lambda m: d.get(m[0]), text)
    return text

def clean(text):
    """Summary

    Args:
        text (TYPE): Description

    Returns:
        TYPE: Description
    """
    text = re.sub(r"\s*<[ ]*br[ ]*/*[ ]*>\s*", "\n", text)
    text = re.sub(r'\s*\n+\s*', '\n', text)
    return clean_codecs(text)

def normalized(s):
    s = s.lower()
    dia_map = {'a': 'áäà',
               'e': 'éëè',
               'i': 'íïì',
               'o': 'óöò',
               'u': 'úüù',
               'n': 'ñ'}

    for k, v in dia_map.items():
        s = re.sub(rf'[{v}]', k, s)
    return s