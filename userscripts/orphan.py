import pywikibot
from pywikibot.bot import ExistingPageBot
from pywikibot import pagegenerators



class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot - minor edit',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""


def main(*args: str) -> None:
    P = pywikibot.Category(pywikibot.Site('commons'), title="Files from Academia Argentina de Letras")
    for page in P.articles():
        orphan = True
        title = page.title()

        if title.split('.')[-1] not in ["pdf", "djvu"]:
            continue
        for link in page.globalusage():
            if str(link.site) == 'wikisource:es':
                print(title)
                orphan = False
                break
        if orphan:
            with open('output.txt', 'a') as f:
                f.write(f'[[{title}]]\n')
        


if __name__ == '__main__':
    main()
