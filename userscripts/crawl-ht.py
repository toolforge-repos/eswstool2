import pywikibot
import re
import UTILS
from pywikibot.proofreadpage import IndexPage
from pywikibot.bot import ExistingPageBot
from pywikibot import pagegenerators, Page
import crawler
from os.path import exists
from hathi import HathiTrust as HT

BLACKLIST = {}
IDBLACKLIST = {'uiug.30112066963890'}
Commons = pywikibot.Site('commons', 'commons')
Wikisource = pywikibot.Site('es', fam='wikisource')


REXP = r"{{ *(?:[Hh]athiTrust) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"


def bad_chars(s):
    bad = '#<>[]|{}:¿¡?!"/'
    bad_map = {c: '' for c in bad}
    return s.translate(str.maketrans(bad_map))


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot - Enlazando a índices.',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        try:
            page = self.current_page
            if page.title() in BLACKLIST:
                return
            text = page.text

            results = re.findall(REXP, text, flags=re.I)
            for r in results:
                bdhid = r[0]
                alttext = r[1]
                if bdhid in IDBLACKLIST:
                    continue

                libros = HT.getViewability(bdhid)
                for identifier, institution in libros.items():
                    print(f'File {identifier}.pdf, from {institution}')
                for identifier, institution in libros.items():

                    if exists(bad_chars(identifier) + ".pdf") \
                            or identifier in IDBLACKLIST:
                        print(bad_chars(identifier) + ".pdf already exists.")
                        continue
                    print(f'Downlading {identifier}.pdf, from {institution}')
                    libro = HT(identifier)
                    libro.save_and_merge()
                    libro.crop_pdf()
                continue

                for page in pages:
                    P = crawler.crawl('File:' + page)
                    if len(P) > 3:
                        index = IndexPage(Wikisource, 'Índice:' + page)
                        index.text = UTILS.index_expression(P)
                        print(index.text)
                        index.save(
                            'Bot - Creando índices a partir de HathiTrust')

                if len(pages) == 0:
                    return
                elif len(pages) == 1:
                    at_text = r'{{at|' + pages[0] + alttext + r'}}'
                else:
                    lista = [rf'{{{{at|{page}|Volumen {i+1}}}}}' for i,
                             page in enumerate(pages)]
                    at_text = ', '.join(lista)

                text = re.sub(r"{{ *(?:[Mm]emoria [Cc]hilena) *\|(?: *id *= *)? *" +
                              bdhid + r" *(\|.+?|) *}}",
                              at_text, text, flags=re.I)
            self.put_current(text, summary=self.opt.summary)
        except KeyboardInterrupt:
            return


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    # args += ('-transcludes:IA',)  # 106 Autor
    options = {}
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse your own command line arguments
    for arg in local_args:
        arg, _, value = arg.partition(':')
        option = arg[1:]
        if option in ('summary', 'text'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        # take the remaining options as booleans.
        # You will get a hint if they aren't pre-defined in your bot class
        else:
            options[option] = True

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)

    # check if further help is needed
    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        # pass generator and private options to the bot
        bot = MyBot(generator=gen, **options)
        bot.run()  # guess what it does


if __name__ == '__main__':
    main()
