import pywikibot
import re
from pywikibot.proofreadpage import IndexPage
from pywikibot.bot import CurrentPageBot


class IndexBot(CurrentPageBot):

    update_options = {
        #    'always': False,
        #    'summary': 'Bot: trabajando en secciones',
    }

    def __init__(self, index=None, start=1, end=None, **kwargs) -> None:
        site = pywikibot.Site('es', 'wikisource')
        prefix = 'Índice:'
        if index is None:
            index = pywikibot.input('Ingresa el Índice, con o sin el prefijo')
        index = index if index.startswith(prefix) else prefix + index

        self.index = IndexPage(site, index)
        while not self.index.exists():
            index = pywikibot.input(
                f'<<lightred>>{self.index.title()} no existe, '
                f'ingresa un Índice creado<<gray>>:')
            index = index if index.startswith(prefix) else prefix + index
            self.index = IndexPage(site, index)

        index_gen = self.index.page_gen(start=start, end=end)
        super().__init__(generator=index_gen, **kwargs)

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        ppage = self.current_page

        # label = self.index.get_label_from_page(ppage)

        # ppage.body = ppage.ocr()
        # if len(ppage.body) < 4:
        #    ppage.without_text()

        # print(ppage.header)
        # print(ppage.body)
        # print(ppage.footer)
        # ppage.save(summary=self.opt.summary)


def main(*args: str):
    args = pywikibot.handle_args(args)
    options = {}
    for arg in args[:]:
        if arg.startswith('-'):
            opt, sep, value = arg.partition(':')
            if opt in ('-summary'):
                options[opt[1:]] = value
            elif opt in ('-start', '-end'):
                options[opt[1:]] = int(value)
            else:
                options[opt[1:]] = True
            args.remove(arg)

    if len(args) == 0:
        index = pywikibot.input('Ingresa el Índice, con o sin el prefijo')
    else:
        index = args[0]

    options['start'] = options.get('start', 1)
    options['end'] = options.get('end', None)

    IndexBot(index=index, **options).run()


if __name__ == '__main__':
    main()
