import pywikibot
import re
from pywikibot.bot import ExistingPageBot
from pywikibot import pagegenerators

Commons = pywikibot.Site('commons', 'commons')
Wikisource = pywikibot.Site('es', fam='wikisource')


REXP = r"{{(?:ia|internet archive)\|(.+?)(\|.+?|)}}"


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Enlazando a índices ya subidos previamente',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text

        results = re.findall(REXP, text, flags=re.I)

        for r in results:
            query = r[0]
            search = Commons.search(query, namespaces='File', total=2)
            for result in search:
                title = result.title(with_ns=False)
                if title.split('.')[-1] not in ["pdf", "djvu"]:
                    continue
                print(query, title)
                _REXP = REXP.replace("(.+?)", f"({query})")
                text = re.sub(
                    _REXP, fr'{{{{at|{title}\2}}}}', text, flags=re.I)
                break

        self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    args += ('-transcludes:IA', '-namespace:106')  # 106 Autor

    options = {}
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse your own command line arguments
    for arg in local_args:
        arg, _, value = arg.partition(':')
        option = arg[1:]
        if option in ('summary', 'text'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        # take the remaining options as booleans.
        # You will get a hint if they aren't pre-defined in your bot class
        else:
            options[option] = True

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)

    # check if further help is needed
    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        # pass generator and private options to the bot
        bot = MyBot(generator=gen, **options)
        bot.run()  # guess what it does


if __name__ == '__main__':
    main()
