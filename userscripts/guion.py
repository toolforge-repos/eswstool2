import pywikibot, re
from pywikibot.proofreadpage import ProofreadPage
from pywikibot import pagegenerators
import time

site=pywikibot.Site('wikisource:es')


def pp(page):
    page = ProofreadPage(site, f'Página:{page._base}/{page._num-1}')
    return page.exists() and page

def np(page):
    page = ProofreadPage(site, f'Página:{page._base}/{page._num+1}')
    return page.exists() and page

def inverse_page(page):
    REXP_1 = r"(\w+-)\s*(?:{{np}})?\s*(?=$|<section end)"
    REXP_2 = r"(?<=^|(?<=/>)){{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*-?([\w]*[ ]*[.,;:?!]*)[ ]*\|*[ ]*-?([\w]*?[ ]*[.,;:?!]*)[ ]*}}"
    
    g2 = re.search(REXP_2, page.body)
    if not g2: 
        print('Caso1')
        return

    prev_page = pp(page)
    if not prev_page: 
        print('Caso2')
        return
    g1 = re.search(REXP_1, prev_page.body)
    if not g1: 
        print('Caso3')
        return

    prev_page.body = re.sub(REXP_1, g1[1], prev_page.body)
    page.body = re.sub(REXP_2, g2[1], page.body)

    prev_page.save(summary=r'quitando instancias de {{guion}}')
    page.save(summary=r'quitando instancias de {{guion}}')


def treat_page(page):        
    REXP_1 =  r"{{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*([\w]+?)-?[ ]*\|*[ ]*-?([\w]*?)[ ]*[.,;:?!]*[ ]*}}\s*(?:{{np}})?\s*(?=$|<section end)"
    REXP_2 =  r"(?<=^|(?<=/>)){{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*-?([\w]*)([ ]*[.,;:?!]*)-?[ ]*\|*[ ]*-?([\w]*?)([ ]*[.,;:?!]*)[ ]*}}"

    next_page = np(page)
    if not next_page: 
        #print('Caso 2', page.title())
        return
    
    #corrección cursivas 1
    
    g1 = re.search(REXP_1, page.body)
    if not g1: 
        REXP_3 = r"{{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*([\w]+?)-[ ]*\|*[ ]*-?.*?}}\s*(?:{{np}})?\s*('+)(?=$|<section end)"
        if re.search(REXP_3, page.body):
            REXP_4 = r"(?<=^|(?<=/>))('+){{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*-?([\w]*)([ ]*[.,;:?!]*)-?[ ]*\|*.*?}}"
            if re.search(REXP_4, next_page.body):
                page.body = re.sub(REXP_3, r"\1\2-", page.body)    
                next_page.body = re.sub(REXP_4, r"\1\2", next_page.body)
                print('Caso 8', page.title())

                page.save(summary=r'quitando instancias de {{guion}}')
                next_page.save(summary=r'quitando instancias de {{guion}}')
            
        REXP_3 = r"{{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*('+)(.*?)-?\1-?\|.*?}}(?=$|<section end)"
        if re.search(REXP_3, page.body):
            REXP_4 = r"(?<=^|(?<=/>)){{[ ]*[Gg]ui[óo]n[ ]*\|[ ]*('+)(.+?)\1\|.*?}}"
            if re.search(REXP_4, next_page.body):
                page.body = re.sub(REXP_3, r"\1\2\1-", page.body)    
                next_page.body = re.sub(REXP_4, r"\1\2\1", next_page.body)
                print('Caso 8', page.title())

                page.save(summary=r'quitando instancias de {{guion}}')
                next_page.save(summary=r'quitando instancias de {{guion}}')
            

        return


    g2 = re.search(REXP_2, next_page.body)
    if not g2:
        REXP_2 = r"(?<=^|(?<=/>))([\w]+)([ ]*[.,;:?!]*)()()"
        g2 = re.search(REXP_2, next_page.body)
        if not g2:
            #print('Caso 3', page.title())
            return


    if (g1[1] + g2[1]  == g1[2] + g2[3]) or (  #Al- + varado == Alvarado + ''
        g1[1] + g2[1] == g1[2] == g2[3]) or (  #Al- + varado == Alvarado1 == Alvarado2                                                                    
        g1[1] + g2[1] == g1[2])          or (  #AL- + varado == Alvarado
        (g1[1] not in g1[2]) and (g1[2] == g2[1]) ) or (
        g1[1] + g2[1] == g2[3]):
        print('Caso 4', page.title())
        page.body = re.sub(REXP_1, g1[1]+'-', page.body)
        next_page.body = re.sub(REXP_2, g2[1]+g2[2], next_page.body)

    elif (g1[1] == g2[1][:len(g1[1])]) and g1[2] == g2[1]:  #Al- ==(Al)varado AND  Alvarado1 == Alvarado
        print('Caso 5', page.title())
        page.body = re.sub(REXP_1, g1[1]+'-', page.body)
        next_page.body = re.sub(REXP_2, g2[1][len(g1[1]):]+g2[2], next_page.body)

    elif (g1[1] + g2[3] == g1[2]): #Al- + |varado == Alvarado1
        print('Caso 6', page.title())
        page.body = re.sub(REXP_1, g1[1]+'-', page.body)
        next_page.body = re.sub(REXP_2, g2[3] + g2[4], next_page.body)

    else:
        print('Caso 7', page.title())
        return
        print( g1[1], g1[2])
        print( g2[1], g2[2], g2[3])

        parte1 = pywikibot.input(f'<<lightyellow>>{page.body[-56:]}<<red>>{g1[1]}')
        parte2 = pywikibot.input(f'<<lightgreen>>{next_page.body[:56]}<<red>>{g2[1]}{g2[2]}')

        if not (parte1 or parte2):
            page.body = re.sub(REXP_1, g1[1]+'-', page.body)
            next_page.body = re.sub(REXP_2, g2[1]+g2[2], next_page.body)
        else:
            page.body = re.sub(REXP_1, parte1, page.body)
            next_page.body = re.sub(REXP_2, parte2, next_page.body)

    page.save(summary=r'quitando instancias de {{guion}}')
    next_page.save(summary=r'quitando instancias de {{guion}}')

def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    args += ('-page:Página:El Tratado de la Pintura.djvu/15', '-transcludes:guion', '-ns:102', ) 
    options = {}
    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators.GeneratorFactory(site=site)
    local_args = gen_factory.handle_args(local_args)
    for arg in local_args:
        arg, _, value = arg.partition(':')
        option = arg[1:]
        if option in ('summary', 'text'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        else:
            options[option] = True
    
    gen = gen_factory.getCombinedGenerator(preload=True)

    for page in gen:
        page = ProofreadPage(page)
        treat_page(page)
    
if __name__ == '__main__':
    main()