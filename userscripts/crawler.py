''' TODO: convertir en clase, cambiar nombre'''

import re
import webbrowser
import pywikibot
from pywikibot import pagegenerators
from pywikibot.proofreadpage import IndexPage
from utils.string_utils import clean_codecs, bad_chars
from utils.bib_utils import ROLES, get_publisher, get_city
from utils.langs import lang_QID
import datetime
import traceback
import Data

from VARIABLES import MAP

P_MAP = {v: k for k, v in MAP.items()}


QUERY = '''
SELECT DISTINCT ?item WHERE {{
  ?item p:{} ?statement0.
  ?statement0 (ps:{}) "{}".
}}
LIMIT 5'''

# any site will work, this is just an example
site = pywikibot.Site('wikidata', 'wikidata')
repo = site.data_repository()

Commons = pywikibot.Site('commons', 'commons')


def setup_claim(item: pywikibot.ItemPage, propertyid: str,
                target: str, oldstring='', imgpage='') -> None:
    """Correctly setup the Wikidata Claim, and uploads it

    Args:
        item (pywikibot.ItemPage): the Wikidata ItemPage.
        propertyid (str): Wikidata Property, as a string. 'P230'.
        target (Any): a properly chosen target for the claim.
        oldstring (str, optional): the string used as reference for this claim.
        imgpage (str, optional): page number for P4714

    Returns:
        None
    """
    if not propertyid:
        return
    if target is False or target == '':
        return

    claim = pywikibot.Claim(repo, propertyid)
    # somevalue = valor desconocido
    if target is None or target == "UNKNOWN":
        claim.setSnakType('somevalue')
    else:
        claim.setTarget(target)
    # no duplicados
    if propertyid in item.claims:
        for c in item.claims[propertyid]:
            if claim.same_as(c, ignore_quals=True):
                return
            elif propertyid == 'P31':  # corregir instanceof q no corresponden
                if c.getTarget().title() == 'Q732577':   # publicación
                    item.removeClaims(
                        c, summary='Removing misleading P31 from edition item')
                    pywikibot.output(
                        f'<<red>>DELETE: <<gray>>{propertyid}'
                        f'{P_MAP.get(propertyid)} {target}')

    # Importado de:
    imp = pywikibot.Claim(repo, getP('imported'))
    imp_target = pywikibot.ItemPage(repo, 'Q565')  # Q565 Wikimedia Commons
    imp.setTarget(imp_target)

    # Fecha de acceso:
    today = datetime.date.today()
    wbtime = pywikibot.WbTime(
        precision=11, year=today.year, month=today.month, day=today.day)
    acc = pywikibot.Claim(repo, getP('accessdate'))
    acc.setTarget(wbtime)

    # Imported URL:
    url = pywikibot.Claim(repo, getP('importurl'))
    url.setTarget(cpURL)

    sources = [imp, acc, url]

    # Oldstring
    if oldstring:
        oldstring = bad_chars(oldstring)
        old = pywikibot.Claim(repo, getP('named'))
        old.setTarget(oldstring)
        sources.append(old)
    # Número de página del título
    if imgpage and imgpage.isdigit():
        page_qual = pywikibot.Claim(repo, getP('titlepage'))
        wbquant = pywikibot.WbQuantity(int(imgpage), site=repo)
        page_qual.setTarget(wbquant)
        claim.addQualifier(page_qual)

    if propertyid not in ['P31', 'P996']:
        claim.addSources(sources)

    # PRINT
    if isinstance(target, pywikibot.ItemPage):
        pywikibot.output(f'<<gray>>{propertyid:5} {P_MAP.get(propertyid):10}'
                         f' {target.labels.get("es","")} {oldstring}')

    elif isinstance(target, pywikibot.WbTime):
        pywikibot.output(
            f'<<gray>>{propertyid:5} {P_MAP.get(propertyid):10}'
            f' {target.toTimestr()}')

    elif isinstance(target, pywikibot.WbMonolingualText):
        pywikibot.output(
            f'<<gray>>{propertyid:5} {P_MAP.get(propertyid):10} {target.text}')

    else:
        pywikibot.output(
            f'<<gray>>{propertyid:5} {P_MAP.get(propertyid):10} '
            f'{target} {oldstring}')

    item.addClaim(claim, summary='Adding claims from Commons')


def edit_data(item: pywikibot.ItemPage, propertyid: str,
              value: Data.Data) -> None:

    if not isinstance(value, Data.Data):
        return
    elif not value.QID:
        target = None
    else:
        target = pywikibot.ItemPage(repo, value.QID)
    setup_claim(item, propertyid, target, oldstring=value.oldstring)


def edit_wikidata(item: pywikibot.ItemPage, propertyid: str,
                  value: str, oldstring='') -> None:
    """Summary

    Args:
        item (pywikibot.ItemPage): Description
        propertyid (str): Description
        value (str): Description
        oldstring (str, optional): Description

    Returns:
        None: Description
    """
    if value is None:
        target = None
    elif not value:
        return
    else:
        target = pywikibot.ItemPage(repo, value)
    setup_claim(item, propertyid, target, oldstring=oldstring)


def edit_monolingual(item: pywikibot.ItemPage,
                     propertyid: str,
                     text: Data.Text = None):

    if not text:
        return

    if isinstance(text, Data.Multilingual):
        for mono in text:
            target = pywikibot.WbMonolingualText(mono.text, mono.lang)
            setup_claim(item, propertyid, target)

    elif isinstance(text, Data.Monolingual):
        target = pywikibot.WbMonolingualText(text.text, text.lang)
        setup_claim(item, propertyid, target)


def edit_date(item, propertyid, date):
    """Summary

    Args:
        item (TYPE): Description
        propertyid (TYPE): Description
        text (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not date:
        return

        return

    target = date.WbTime

    setup_claim(item, propertyid, target)


def edit_filepage(item, propertyid, filename, imgpage=''):
    """Summary

    Args:
        item (TYPE): Description
        propertyid (TYPE): Description
        filename (TYPE): Description
        imgpage (str, optional): Description

    Returns:
        TYPE: Description
    """
    if not filename:
        return 0
    target = pywikibot.FilePage(Commons, filename)
    setup_claim(item, propertyid, target, imgpage=imgpage)


def createwikidata(data):
    """Summary

    Args:
        data (TYPE): Description

    Returns:
        TYPE: Description
    """
    # data = {'labels': {'es': title},
    #         'descriptions': {'es': 'COSHCOSHCOSH'},
    #         'aliases': {'es': ['CosCos', 'Coscorrin'], 'de': ['kishkish']},
    #         'sitelinks': [{'site': 'enwiki', 'title': 'Earth'}]
    #         }
    item = pywikibot.ItemPage(repo)
    item.editEntity(data, summary='Create item')
    return item


def getP(param):
    """Summary

    Args:
        param (TYPE): Description

    Returns:
        TYPE: Description
    """
    if repo.code == 'test':
        return    # m = TESTMAP
    elif repo.code == 'wikidata':
        m = MAP
    else:
        return ''
    return m.get(param, '')


def mainloop(E, bot=False, **kwargs):

    filename = E.filename
    QID = E.QID
    P = E
    global cpURL
    cpURL = E['_url']
    # Identificadores adicionales
    E['_sources']['oclc'] = E.get('oclc', '')
    E['_sources']['lccn'] = E.get('lccn', '')

    # Colapsar Date y Publication date
    E['date'] = E.data.pop('publication_date', '') or E.get('date')

    choice = ''
    try:
        while choice != 's':
            choice = pywikibot.input_choice(
                f"¿Procesar <<lightyellow>>{filename}?<<lightgray>>",
                (('Sí', 'S'), ('No', 'N'), ('Open in Browser', 'B')),
                default='s', force=bot)
            if choice == 'n':
                return {}
            elif choice == 'b':
                webbrowser.open(cpURL)
    except pywikibot.bot_choice.QuitKeyboardInterrupt:
        exit()

    # IDENTIFIER CHECK
    for pname, ID in E['_sources'].items():
        if not ID:
            continue
        propertyid = getP(pname)
        generator = pagegenerators.WikidataSPARQLPageGenerator(
            QUERY.format(propertyid, propertyid, ID), site=repo)
        for result in generator:
            if bot:
                return E
            while choice != 'c':
                choice = pywikibot.input_choice(
                    f"<<lightred>>Parece que hay otro elemento"
                    f"<<gray>>({result.title()}, <<lightgray>>"
                    f"{result.labels.get('es','')}, "
                    f"{result.descriptions.get('es','')}) "
                    f"<<lightred>> con <<lightyellow>>{propertyid} ({pname})"
                    f"<<lightred>> {ID}"
                    f"<<lightgray>> Saltar?",
                    (('Saltar', 'S'),
                     ('Crear un nuevo elemento', 'C'),
                     ('Open in Browser', 'B'),
                     ('Usar este elemento', 'U')), default='s')
                if choice == 's':
                    return {}
                elif choice == 'c':
                    QID = ''
                elif choice == 'b':
                    webbrowser.open(result.full_url())
                elif choice == 'u':
                    QID = result.title()
                    choice = 'c'

    # FECHA Y VOLUMEN PARA DESCRIPCION
    date = E['date']
    desc = ''
    if str(date):
        desc = f'edición de {date}'
    else:
        desc = 'edición'

    if E.get('volume') and E['volume'].isdigit():
        desc = 'volumen {}, {}'.format(E['volume'], desc)
    elif E.get('volume'):
        volume_clean = re.sub(
            r"{{\w\w+[ ]*\|(?:[ ]*1[ ]*=[ ]*)?[ ]*(.+?)}}", r"\1", E['volume'])
        desc = '{}, {}'.format(volume_clean, desc)

    title = str(E['title'])

    # CREAR ITEM EN WIKIDATA
    if QID:
        pywikibot.info(f"<<lightyellow>>{filename} ya tiene entrada "
                       f'en Wikidata: {QID}')
        item = pywikibot.ItemPage(repo, QID)
        if item.exists():
            if item.isRedirectPage():
                item = item.getRedirectTarget()

    else:
        try:
            pywikibot.info('Se va a crear una nueva entrada en Wikidata:')
            title = pywikibot.input(
                'Etiqueta:<<lightyellow>> ', default=title, force=bot)
            desc = pywikibot.input(
                'Descripción:<<lightyellow>> ', default=desc, force=bot)

            item = createwikidata(
                {'labels': {'es': title}, 'descriptions': {'es': desc}})
            pywikibot.info(f'Se creó una nueva entrada en Wikidata: '
                           f'<<lightyellow>>{item.title()}')
            QID = item.title()
        except pywikibot.exceptions.OtherPageSaveError as e:
            pywikibot.info(f'No se pudo crear la entrada en Wikidata, '
                           f'ya existe otro elemento con etiqueta "{title}" y'
                           f' descripción "{desc}"')
            if bot and False:
                if m := re.search(r'Q\d+', str(e)):
                    E.QID = m[0]
                return E
            title = pywikibot.input('Etiqueta:<<lightyellow>> ', default=title)
            desc = pywikibot.input(
                'Por favor, ingresa una descripción única: o ingresa un QID')
            if re.match(r'Q\d+$', desc):
                QID = desc
                item = pywikibot.ItemPage(repo, QID)
            else:
                item = createwikidata(
                    {'labels': {'es': title}, 'descriptions': {'es': desc}})
                QID = item.title()
        except KeyboardInterrupt:
            return {}
    E.QID = QID
    try:  # Todo lo de Wikidata en un try
        # P31 -> edición, etc.
        edit_wikidata(item, getP('instanceof'), 'Q3331189')

        edit_monolingual(item, getP('title'), E['title'])
        edit_monolingual(item, getP('subtitle'), E.get('subtitle'))

        for role in ROLES:
            for person in E.get(role, []):
                edit_data(item, getP(role), person)

        # PUBLISHER
        city = get_city(E.get('city'))
        edit_data(item, getP('city'), city)

        publisher = get_publisher(E.get('publisher'))
        edit_data(item, getP('publisher'), publisher)

        # FECHA
        edit_date(item, getP('date'), date)

        # IDIOMA
        langQID = lang_QID(E.get('language'))
        if langQID:
            edit_wikidata(item, getP('language'), langQID)

        # FI1CHERO EN COMMONS
        edit_filepage(item, getP('document'),
                      filename, imgpage=E.get('image_page'))

        # WIKISOURCE INDEX URL
        setup_claim(item, getP('indexurl'), E.index_page.full_url())

        # VOLUMEN
        setup_claim(item, getP('volume'), E.get('volume', ''))

        # NUMERO
        setup_claim(item, getP('num'), E.get('num', ''))

        # EDICIÓN
        setup_claim(item, getP('edition'), E.get('edition', ''))

        # SOURCE
        for pname, ID in E.sources.items():
            try:
                setup_claim(item, getP(pname), ID)
                # oldstring=E['source']['oldstring'] - innecesario?
            except Exception as exc:
                print(exc)
                continue

    # HASTA ACÁ TRY PRINCIPAL
    except KeyboardInterrupt:
        return E
    except Exception as exc:
        # Salvar QID en Commons aunque el programa falle

        pywikibot.output('<<red>>{}'.format(exc))
        print(traceback.format_exc())
        return E
    return E


def _crawl(E, createindex=False, savecommons=True, **kwargs):
    filename = E.filename
    page = E.commons_page or pywikibot.Page(Commons, 'File:' + filename)
    index = IndexPage(
        pywikibot.Site('es', 'wikisource'), 'Índice:' + filename)

    E.commons_page = page
    E.index_page = index

    if not E.QID:
        commons_dict = E.get_commons_dict()
        E.data |= commons_dict
        if commons_dict.get('wikidata'):
            E.QID = commons_dict.get('wikidata')

    if len(E.data) < 4:
        if m := re.search(r'{{ *[Ii]nformation(?:[^{}]|{{[^}]+}})+}}', page.text):
            if n := re.search(r'https?://archive.org/details/(\S+)', page.text):
                print('testing................................................')
                try:
                    import utils.ia_source as IA
                    ia = IA.IaSource(n[1])
                    iadict = ia.get_dict()
                    E.data = iadict
                    E.process_data()
                    E.commons_page.text = E.commons_page.text.replace(
                        m[0], E.to_commons())
                except Exception as e:
                    print(e)
                    return {}
        else:
            pywikibot.info(
                f'<<gray>>Sin {{{{Book}}}} en {filename}. Saltando.')
            return {}
    if not E['title']:
        pywikibot.info(
            f'<<red>>{filename}<<white>> no tiene título.')
        # Inferencia título
        return {}

    E['_url'] = page.full_url()

    P = mainloop(E, **kwargs)
    if savecommons and P and len(P.data) > 3:
        # Magical expressión
        oldctext = page.text
        page.text = re.sub(r'{{ *[Bb]ook(?:[^{}]|{{[^}]+}})+}}',
                           P.to_commons(), page.text)

        # Otros favorcillos temporales
        page.text = clean_codecs(page.text)
        page.text = re.sub(r" s:spa?:", ' s:es:', page.text)
        page.text = re.sub(r" = None *\n", ' = \n', page.text)
        print(page.text)
        # choice = pywikibot.input_choice(
        #     's/n', (('s', 's'), ('n', 'n')), default='s')
        # if choice == 's':
        #     page.save('Cleaning bibliographical info, adding wikidata')
        if page.text != oldctext:
            page.save('Cleaning bibliographical info, adding wikidata')

    if createindex:
        olditext = index.text
        filename = filename.split(':')[-1]
        index.text = P.to_wikisource()
        print(index.text)
        if index.text != olditext:
            index.save('Bot - Creando índices a partir de archivo')

    return P


def crawl(value, **kwargs):
    # str = nombre de archivo en Commons
    if isinstance(value, str):
        page = pywikibot.Page(Commons, f'File:{value}')
        if page.exists():
            if page.isRedirectPage():
                page = page.getRedirectTarget()
        E = Data.Edition(filename=page.title(), commons_page=page)
        E.data = E.get_commons_dict()

        return _crawl(E, **kwargs)

    # Data.Edition = formato listo
    elif isinstance(value, Data.Edition):
        return _crawl(value, **kwargs)

    # Page = página de commons
    elif isinstance(value, pywikibot.Page):
        E = Data.Edition(filename=value.title(), commons_page=value)
        E.data = E.get_commons_dict()
        return _crawl(E, **kwargs)

    # dict = un diccionario de otro script
    elif isinstance(value, dict):
        E = Data.Edition(data=value)
        E.process_data()
        return _crawl(E, **kwargs)

    else:
        raise Exception('Formato desconocido para crawl()')


def main(*args: str) -> None:
    local_args = pywikibot.handle_args(args)

    gen_factory = pagegenerators.GeneratorFactory(
        site=pywikibot.Site('commons', 'commons'))
    if local_args and ':' not in local_args[0]:
        local_args[0] = f'-page:File:{local_args[0]}'

    local_args = gen_factory.handle_args(local_args)
    gen = gen_factory.getCombinedGenerator(preload=True)

    # test
    # gen = pagegenerators.PetScanPageGenerator(
    #         [],
    #         site=Commons,
    #         namespaces=[6],
    #         extra_options={
    #             'project': 'wikimedia',
    #             'language': 'commons',
    #             'psid': '29897086'
    #         })

    for page in gen:
        crawl(page, createindex=True)


if __name__ == '__main__':
    main()
