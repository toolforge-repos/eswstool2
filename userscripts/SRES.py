import requests
import re
from bs4 import BeautifulSoup
import pywikibot
from pywikibot import Page, Site
import Data
import UTILS
import crawler

URL = "https://digitallibrary.un.org/record/"
PDFURL = "https://digitallibrary.un.org"

Commons = Site('commons', 'commons')


def get_metadata_rows(soup):
    m = {}
    t = soup.find_all("div", class_="metadata-row")
    for row in t:
        title = row.find('span', 'title')
        value = row.find('span', 'value')
        if title and value:
            m[title.string.strip()] = value
    return m


def get_pdf_url(soup):
    for child in soup.find_all(href=re.compile("-ES.pdf")):
        return PDFURL + child['href']


def get_book_dict(ID):
    d = {}

    page = requests.get(f'{URL}{ID}')
    soup = BeautifulSoup(page.content, "html.parser")

    m = get_metadata_rows(soup)

    d["title"] = Data.Multilingual({'en': m['Title'].string})
    d['_resyear'] = re.search(r'(\d\d\d\d)', d['title']['en'].text)[1]

    # para estas:
    d['title']['es'] = f'Resoluciones y decisiones del Consejo de Seguridad, {d["_resyear"]}'

    d["accession_number"] = m['Symbol'].string

    d["author"] = 'Consejo de Seguridad de las Naciones Unidas'
    d["description"] = ''.join(m['Description'].strings).strip()

    if n := re.search(r'([\w ]+?) *: *([\w ]+?) *, *(\d+)', m['Date'].string):
        d['city'] = n[1]
        d['publisher'] = n[2]
        d["date"] = n[3]

    d["source"] = f'[{URL}{ID} United Nations Digital Library]'
    d['_undl'] = ID

    d['wikisource'] = r's:es:Índice:{{PAGENAME}}'

    d["_pdfurl"] = get_pdf_url(m['Access'])

    return d


def get_book_desc(E):

    return f'''== {{{{int:filedesc}}}} ==

{E.to_commons()}

== {{{{int:license-header}}}} ==
{{{{PD-UN-doc}}}}

[[Category:United Nations Security Council resolutions in Spanish]]
[[Category:{E['_resyear']} United Nations Security Council resolutions]]
'''


def do_upload(E, pdfurl, filename):
    filepage = Page(Commons, 'File:' + filename)

    if filepage.exists():
        pywikibot.info(f'{filename} ya existe en Commons. Saltando.')
        return filename

    filepath = UTILS.download_pdf_file(pdfurl, filename)

    if not filepath:
        return None

    commonsText = get_book_desc(E)

    filename = UTILS.upload_to_commons(filepath, filename,
                                       commonsText)
    return filename


def do_SRES(mcid):
    print(mcid)
    D = get_book_dict(mcid)

    E = Data.Edition(data=D)
    E.process_data()
    E.sources['undl'] = D['_undl']

    filename = f'ONU - Resoluciones y decisiones del Consejo de Seguridad - {E["_resyear"]}.pdf'

    pdfurl = E['_pdfurl']

    filename = do_upload(E, pdfurl, filename)
    if filename:
        E.filename = filename

        crawler.crawl(filename, createindex=True, bot=True)
        return E


def main(*args: str):
    for i in [196228, 229719]:
        do_SRES(str(i))


if __name__ == '__main__':
    main()
