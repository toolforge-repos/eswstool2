import requests
import re
from bs4 import BeautifulSoup
import pywikibot
from pywikibot import Page, Site
import Data
import UTILS
import crawler

URL = "https://digitallibrary.un.org/record/"
PDFURL = "https://digitallibrary.un.org"

Commons = Site('commons', 'commons')


def get_metadata_rows(soup):
    m = {}
    t = soup.find_all("div", class_="metadata-row")
    for row in t:
        title = row.find('span', 'title')
        value = row.find('span', 'value')
        if title and value:
            m[title.string.strip()] = value
    return m


def get_pdf_url(soup):
    for child in soup.find_all(href=re.compile("-ES.pdf")):
        return PDFURL + child['href']


def get_book_dict(ID):
    d = {}

    page = requests.get(f'{URL}{ID}')
    soup = BeautifulSoup(page.content, "html.parser")

    m = get_metadata_rows(soup)
    signatura = ' '.join(m['Symbol'].strings)

    d["accession_number"] = signatura

    titleparts = m['Title'].string.split('=')
    if len(titleparts) > 1:
        d["title"] = Data.Multilingual(
            {'en': titleparts[0], 'fr': titleparts[1]})
    else:
        d["title"] = Data.Multilingual({'en': titleparts[0]})

    entitle = d["title"]["en"].text

    if n := re.findall(r'(\d\d\d\d)', entitle):
        d['_resyear'] = n[-1]
    elif n := re.findall(r'(\d\d\d\d)', m['Authors'].string):
        d['_resyear'] = n[-1]

    if n := re.search(r'(\d+)\w\w part|part (\d)', entitle):
        d['_parte'] = n[1]
    if n := re.search(r'(\d+)\w\w (?:emergency|special)', entitle):
        d['_sesion'] = f'{n[1]} sesión extraordinaria'
    elif n := re.search(r'(\d+)\w\w session', entitle):
        d['_sesion'] = f'{n[1]} sesión'

    if n := re.search(r'(corrigendum|addendum)', entitle):
        d['_endum'] = n[1]
    elif n := re.search(r'(Add.2)', signatura):
        d['_endum'] = 'addendum 2'
    elif n := re.search(r'(Add)', signatura):
        d['_endum'] = 'addendum'
        # para estas:
    #d['title']['es'] = f'Resoluciones adoptadas por la Asamblea General, {d["_resyear"]}'

    if n := re.search(r'(?:Vol\.|Volume|v\.) (\w+)', entitle):
        d['volume'] = n[1]

    if d.get('volume') or d.get('_parte'):
        if d.get('_endum') == 'addendum':
            d['_endum'] = ''

    d["author"] = 'Asamblea General de las Naciones Unidas'
    d["description"] = ''.join(m['Description'].strings).strip()

    if n := re.search(r'([\w, ]+?) *: *([\w ]+?) *, *.*?(\d\d\d\d)', m['Date'].string):
        d['city'] = n[1]
        d['publisher'] = n[2]
        d["date"] = n[3]
    elif n := re.search(r'([\w ]+?) *: *\[*(\d\d\d\d)\]*', m['Date'].string):
        d['city'] = n[1]
        d['date'] = n[2]

    d["source"] = f'[{URL}{ID} United Nations Digital Library]'
    d['_undl'] = ID

    d['wikisource'] = r's:es:Índice:{{PAGENAME}}'

    d["_pdfurl"] = get_pdf_url(m['Access'])

    return d


def get_book_desc(E):

    return f'''== {{{{int:filedesc}}}} ==

{E.to_commons()}

== {{{{int:license-header}}}} ==
{{{{PD-UN-doc}}}}

[[Category:United Nations General Assembly resolutions in Spanish]]
[[Category:{E['_resyear']} United Nations General Assembly resolutions]]
'''


def do_upload(E, pdfurl, filename):
    filepage = Page(Commons, 'File:' + filename)

    if filepage.exists():
        pywikibot.info(f'{filename} ya existe en Commons. Saltando.')
        return filename

    filepath = UTILS.download_pdf_file(pdfurl, filename)

    if not filepath:
        return None

    commonsText = get_book_desc(E)

    filename = UTILS.upload_to_commons(filepath, filename,
                                       commonsText)
    return filename


def do_SRES(mcid):
    print(mcid)
    D = get_book_dict(mcid)

    E = Data.Edition(data=D)
    E.process_data()
    E.sources['undl'] = D['_undl']

    filename = f'ONU - Resoluciones de la Asamblea General'
    if ses := E.get('_sesion'):
        filename += f' - {ses}'
    if parte := E.get('_parte'):
        filename += f' - Parte {parte}'
    if vol := E.get('volume'):
        filename += f' - Volumen {vol}'
    if endum := E.get('_endum'):
        filename += f' ({endum})'
    filename += f' - {E["_resyear"]}.pdf'

    pdfurl = E['_pdfurl']

    filename = do_upload(E, pdfurl, filename)
    if filename:
        E.filename = filename

        crawler.crawl(E, createindex=True, bot=True)
        return E


def main(*args: str):
    for i in [ 727406, 792293]:
        do_SRES(str(i))


if __name__ == '__main__':
    main()
