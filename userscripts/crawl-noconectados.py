import pywikibot
from pywikibot.bot import ExistingPageBot
from pywikibot import pagegenerators
import crawler
from utils.template_utils import set_template_data

Commons = pywikibot.Site('commons', 'commons')
Wikisource = pywikibot.Site('es', fam='wikisource')


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot - Enlaza',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        index = self.current_page
        title = index.title(with_ns=False)

        if title.split('.')[-1] not in ["pdf", "djvu"]:
            return

        P = crawler.crawl('File:' + title)

        if len(P) > 3:
            text = set_template_data(index.text,
                                     ':Mediawiki:Proofreadpage_index_template',
                                     'wikidata', P['_QID'])

            self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    gen_factory = pagegenerators.GeneratorFactory()
    gen_factory.handle_arg('-cat:Índices no conectados a Wikidata')

    gen = gen_factory.getCombinedGenerator(preload=True)

    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        bot = MyBot(generator=gen)
        bot.run()


if __name__ == '__main__':
    main()
