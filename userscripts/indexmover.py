import pywikibot
import re
from pywikibot import proofreadpage
from pywikibot import pagegenerators
from pywikibot.bot import CurrentPageBot
from tqdm import tqdm

site = pywikibot.Site('es', fam="wikisource")
site.login()
commons = pywikibot.Site('commons', 'commons')


class MyBot(CurrentPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot: OCR + arreglos varios',
    }

    def treat_page(self):
        ppage = self.current_page
        title = ppage.title()
        pagenum = ppage._parse_title()[2]
        ppage.move('Página:' + commons_page + '/' + str(pagenum),
                   reason="Moviendo páginas a nuevo índice: " + commons_page, noredirect=True)


def main(*args: str):
    """Parse command line arguments and invoke bot."""
    local_args = pywikibot.handle_args(args)
    # global options
    global commons_page
    global INDEX
    for arg in local_args:
        if arg == 'manual':
            title = pywikibot.input('Índice (sin namespace):')
            commons_page = pywikibot.input('Nuevo índice (sin namespace):')
            INDEX = proofreadpage.IndexPage(site, title="Index:" + title)
            gen_factory = INDEX.page_gen(
                filter_ql=[0, 1, 2, 3, 4, 5], only_existing=True)
            MyBot(generator=gen_factory).run()
            INDEX.move('Índice:' + commons_page,
                       reason="Moviendo índice:", noredirect=True)
            return True

    maingen = pagegenerators.AllpagesPageGenerator(
        start='!', namespace=104, includeredirects=False, site=site)
    for ind in (pbar := tqdm(maingen)):
        title = ind.title(with_ns=False)
        pbar.set_description(f'Analizando {title}')
        commons_page = pywikibot.Page(commons, 'File:' + title)
        if commons_page.isRedirectPage():
            commons_page = commons_page.getRedirectTarget().title(with_ns=False)
            print(title, commons_page)
            INDEX = proofreadpage.IndexPage(site, title="Index:" + title)
            gen_factory = INDEX.page_gen(
                filter_ql=[0, 1, 2, 3, 4, 5], only_existing=True)
            MyBot(generator=gen_factory).run()
            INDEX.move('Índice:' + commons_page,
                       reason="Moviendo índice:", noredirect=True)


if __name__ == '__main__':
    main()
