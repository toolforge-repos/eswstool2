'''
Se puede ejecutar sin parametros:

    python pwb.py OCRBot

o con cualquiera de los siguientes parámetros:

:primer parámetro sin nombre: el nombre del Índice, con o sin prefijo
:-tipo: un número del 1 al 4
:-head: el texto del encabezado
:-method: uno de los siguientes: 'wmfOCR' o 'googleOCR'
'''
import pywikibot
import re
import roman
from pywikibot.proofreadpage import IndexPage, ProofreadPage
from pywikibot import pagegenerators, Site
from pywikibot.bot import CurrentPageBot

from VARIABLES import DEFAULT_TUPLES


def is_roman(s):
    try:
        roman.fromRoman(s)
        return True
    except Exception:
        return False


class OCRBot(CurrentPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot: OCR + arreglos varios',
        'tipo': 0,
        'head': None,
        'method': None,
        'tuples': [],
        'columna': False,
    }

    def __init__(self, index=None, start=1, end=None, **kwargs) -> None:
        prefix = 'Índice:'
        if index is None:
            index = pywikibot.input('Ingresa el Índice, con o sin el prefijo')
        index = index if index.startswith(prefix) else prefix + index

        self.index = IndexPage(Site('wikisource:es'), index)
        while not self.index.exists():
            index = pywikibot.input(
                f'<<lightred>>{self.index.title()} no existe, '
                f'ingresa un Índice creado<<gray>>:')
            index = index if index.startswith(prefix) else prefix + index
            self.index = IndexPage(Site('wikisource:es'), index)

        index_gen = self.index.page_gen(
            filter_ql=[1], only_existing=False, start=start, end=end)

        super().__init__(generator=index_gen, **kwargs)

        if self.opt.tipo == 0:
            pywikibot.info('Ingresa el tipo de encabezado:')
            pywikibot.info('''
1: — num — centrado
2: par e impar con el mismo texto
3: par e impar con textos distintos (pero iguales a través del libro)
4: no agregar encabezado (casos más complejos, agregarlo en una segunda etapa)''')

            self.tipo = int(pywikibot.input_choice(
                'tipo', (('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'))))
        else:
            self.tipo = self.opt.tipo

        if self.opt.head is None:
            if self.tipo == 2:
                self.head = pywikibot.input('Encabezado (pares e impares)')
            elif self.tipo == 3:
                self.head = [pywikibot.input('Encabezado (pares)'),
                             pywikibot.input('Encabezado (impares)')]
        else:
            self.head = self.opt.head

        if self.opt.method is None:
            pywikibot.info('Ingresa el método de OCR')
            pywikibot.info('''1: wmfOCR
2: googleOCR
3: no (sin OCR, usar la capa de texto del archivo)''')

            _m = int(pywikibot.input_choice(
                'Método', (('1', '1'), ('2', '2'), ('3', '3'))))
            self.method = ['wmfOCR', 'googleOCR', 'no'][_m]

        else:
            self.method = self.opt.method
        self.tuples = self.opt.tuples

    def get_header(self, n, label):
        if n is None:
            return " "

        if self.tipo == 1:
            return f"{{{{cp||— {label} —|}}}}"

        elif self.tipo == 2:
            if n % 2 == 0:
                return f"{{{{cp|{label}|{self.head}|}}}}"
            else:
                return f"{{{{cp||{self.head}|{label}}}}}"

        elif self.tipo == 3:
            if n % 2 == 0:
                return f"{{{{cp|{label}|{self.head[0]}|}}}}"
            else:
                return f"{{{{cp||{self.head[1]}|{label}}}}}"

        return False

    def do_ocr(self, page):
        if (method := self.method) == 'no' or not method:
            return page.body  # no OCR
        elif method not in page._OCR_METHODS:
            pywikibot.info(
                f'<<lightyellow>>{method} '
                f'<<gray>>no es un motor de OCR válido. '
                'Usando <<lightyellow>> wmfOCR')
            # ‘phetools’, ‘wmfOCR’ or ‘googleOCR’
            method = 'wmfOCR'

        if self.opt.columna:

            page.width = int(re.search(r'page\d+\-(\d+)px', page.url_image)[1])

            # variables columnas
            lang = page.site.lang
            percent = 0.52
            page.width = int(re.search(r'page\d+\-(\d+)px', page.url_image)[1])
            ancho1 = int(page.width * percent)
            x2 = page.width - ancho1

            # columnas
            uri = (f'https://ocr.wmcloud.org/api.php?engine=tesseract&'
                   f'langs[]={lang}&image={page.url_image}&uselang={lang}'
                   f'&crop[x]=0&crop[y]=0&crop[width]='
                   f'{ancho1}&crop[height]=9999')

            uri2 = (f'https://ocr.wmcloud.org/api.php?engine=tesseract&'
                    f'langs[]={lang}&image={page.url_image}&uselang={lang}'
                    f'&crop[x]={x2}&crop[y]=0&crop[width]='
                    f'9999&crop[height]=9999')

            columna1 = page._ocr_callback(uri, ocr_tool=method)[1]
            columna2 = page._ocr_callback(uri2, ocr_tool=method)[1]

            return ' '.join([columna1, columna2])
        else:
            return page.ocr(ocr_tool=method)

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        label = self.index.get_label_from_page(page)
        oldtext = page.text

        # roman support
        if is_roman(label):
            n = roman.fromRoman(label)
        else:
            n = int(label) if label.isdigit() else None

        header = self.get_header(n, label)
        if header:
            page.header = header

        # OCR
        page.body = self.do_ocr(page)

        if len(page.body) < 4:
            page.header = ''
            page.without_text()
            page.save(summary=self.opt.summary)
            return

        page.body = self.transform_body(page.body)

        newtext = page.text

        if oldtext.rstrip() == newtext.rstrip():
            pywikibot.info(f'No hubo cambios en {page}')
            return False

        pywikibot.showDiff(oldtext, newtext)
        if not self.user_confirm('¿Aceptas los siguientes cambios?'):
            return False

        sumario = self.opt.summary
        if self.opt.columna:
            sumario += ' (modo columnas)'
        page.save(summary=self.opt.summary)

    def transform_body(self, body):

        TIPO_TUPLES = []

        if self.tipo == 1:
            TIPO_TUPLES = [(r"^[-—][ ]*\d+[ ]*[-—]\s*", "")]
        elif self.tipo == 2:
            TIPO_TUPLES = [(rf"^[\d\s]*{self.head}[\d\s]*", "")]
        elif self.tipo == 3:
            TIPO_TUPLES = [(rf"^[\d\s]*{self.head[0]}[\d\s]*", ""),
                           (rf"^[\d\s]*{self.head[1]}[\d\s]*", "")]

        REPLACE_TUPLES = TIPO_TUPLES + DEFAULT_TUPLES + self.tuples

        for x in REPLACE_TUPLES:
            body = re.sub(x[0], x[1], body)
        return body


def main(*args: str):
    args = pywikibot.handle_args(args)
    options = {}
    for arg in args[:]:
        if arg.startswith('-'):
            opt, sep, value = arg.partition(':')
            if opt in ('-summary', '-head', '-method'):
                options[opt[1:]] = value
            elif opt in ('-tipo', '-start', '-end'):
                options[opt[1:]] = int(value)
            else:
                options[opt[1:]] = True
            args.remove(arg)

    if len(args) == 0:
        index = pywikibot.input('Ingresa el Índice, con o sin el prefijo')
    else:
        index = args[0]

    options['start'] = options.get('start', 1)
    options['end'] = options.get('end', None)

    OCRBot(index, **options).run()


if __name__ == '__main__':
    main()
