import pywikibot
from pywikibot.pagegenerators import AllpagesPageGenerator

site = pywikibot.Site('wikisource:es')


def escape(text):
    d = dict(zip('ÁÉÍÓÚÜáéíóúü', 'AEIOUUaeiouu'))
    return text.translate(str.maketrans(d))


def treat_page(page):
    print(page.title())
    
    title = page.title(with_ns=False)
    redirect = pywikibot.Page(site, title)
    if not redirect.exists():
        redirect.set_redirect_target(page, create=True, force=False, save=True,
                                     summary="Bot - Agregando redirecciones")

    escaped = pywikibot.Page(site, escape(title))
    if not escaped.exists():
        escaped.set_redirect_target(page, create=True, force=False, save=True,
                                    summary="Bot - Agregando redirecciones")

    au_esc = pywikibot.Page(site, 'Autor:' + escape(title))
    if not au_esc.exists():
        au_esc.set_redirect_target(page, create=True, force=False, save=True,
                                      summary="Bot - Agregando redirecciones")

    


def main(*args: str) -> None:
    # Autores
    gen = AllpagesPageGenerator(
        start='!', namespace=106, includeredirects=False,
        site=site, content=False)
    for page in gen:
        treat_page(page)

    # Portales
    gen = AllpagesPageGenerator(
        start='!', namespace=100, includeredirects=False,
        site=site, content=False)
    for page in gen:
        treat_page(page)


if __name__ == '__main__':
    main()
