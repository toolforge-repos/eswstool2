from collections import Counter
import re

az = 'abcdefghijklmnñopqrstuvwxyz'
AZ = az.upper()

with open('CORPUS', 'r') as file:
    txt = file.read()

words = [x.translate(str.maketrans(
    '', '', '"#$%\'()*+,-./:;<=>?@[\\]^_`{|}~')) for x in txt.split()]
hist = Counter(words)
S = Counter()
F = Counter()

for word in hist:
    if m := re.search(r'^ſ\w\w', word):
        S.update({'_' + m[0]: hist[word]})
    if m := re.search(r'^f\w\w', word):
        F.update({'_' + m[0]: hist[word]})
    if m := re.search(r'\wſ\w', word):
        S.update({m[0]: hist[word]})
    if m := re.search(r'\wf\w', word):
        F.update({m[0]: hist[word]})
    if m := re.search(r'ſ\w\w', word):
        S.update({m[0]: hist[word]})
    if m := re.search(r'f\w\w', word):
        F.update({m[0]: hist[word]})
# for word in hist:
#     for x in az + AZ:
#         for y in az:
#             f = x + 'f' + y
#             s = x + 'ſ' + y
#             if f in word:
#                 F.update({f: hist[word]})
#             if s in word:
#                 S.update({s: hist[word]})
R = Counter()

for key in S:
    kfy = key.replace('ſ', 'f')
    R[kfy] = S[key] / (F[kfy] or 1)

LIMITE = 10

for r in R.most_common():
    print(r)
    if r[1] < LIMITE:
        break
        
    pollo = r'(?<=\P{L})'
    rr = r[0].replace('_', pollo)
    t = r.replace('f', 'ſ')
    s = f'.replace(/{rr}/gu, "{t}")\n'

    with open('LIST', 'a') as file:
        print(s)
        file.write(s)
