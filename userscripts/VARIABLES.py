
MAP = {'instanceof': 'P31',  # Q3331189 edición, traducción o versión
       'title': 'P1476',
       'subtitle': 'P1680',
       'author': 'P50',
       'translator': 'P655',
       'editor': 'P98',
       'illustrator': 'P110',
       'introducer': 'P2679',
       'date': 'P577',
       'publisher': 'P123',
       'printer': 'P872',
       'city': 'P291',
       'language': 'P407',  # Q1321 español
       'document': 'P996',
       'titlepage': 'P4714',
       'imported': 'P143',  # Q565 Wikimedia Commons
       'accessdate': 'P813',
       'importurl': 'P4656',
       'named': 'P5997',  # P1810?? P1932???? NO
       'edition': 'P393',
       'volume': 'P478',
       'num': 'P433',
       'ia_id': 'P724',
       'gb_id': 'P675',
       'galiciana_id': 'P3004',
       'bdh_id': 'P4956',
       'hathi': 'P1844',
       'oclc': 'P243',
       'lccn': 'P1144',
       'ol_id': 'P648',
       'indexurl': 'P1957',
       'undl': 'P12922',
       }

TESTMAP = {}

DEFAULT_TUPLES = [
    (r" +\n", r'\n'),

    # remove trailing whitespace preceding a hard line break
    (r" +<br *\/?>", r'<br />'),

    # remove trailing whitespace and numerals at the end of page text
    # (numerals are nearly always page numbers in the footer)
    (r"[\s\d]+$", r''),
    (r"^[\s\d]+", r''),

    # quitar restos de OCR de Google
    (r"Digitized.{8,15}$", r''),

    # quitar Biblioteca Nacional de España
    (r"(© )?Biblioteca Nacional de España", r''),

    # remove trailing spaces at the end of refs
    (r" +<\/ref>", r'</ref>'),

    # remove trailing spaces at the end of template calls
    (r" +}}", r'}}'),

    # convert double-hyphen to mdash (avoiding breaking HTML comment syntax)
    (r"([^\!])--([^>])", r'\1—\2'),

    # remove spacing around mdash, but only if it has spaces on both sides
    # (we don't want to remove the trailing space from "...as follows:— ",
    # bearing in mind that the space will already be gone if at end of line).
    (r" +— +", r'—'),

    # aparecer "guiones suaves"
    # (r"\u00AD", r'-'),

    # join words that are hyphenated across a line break
    # (but leave "|-" table syntax alone)
    (r"([^\|])[\-¬\u00AD]\n", r'\1'),

    # lines that start with " should probably be new lines,
    # if the previous line ends in punctuation,
    # other than a comma or semicolon
    # and let's get rid of trailing space while we're at it
    (r"([^\n\w,;])\n\" *", r'\1\n\n"'),

    # nueva línea y abre comillas (o cierra en algunos casos)
    # probablemente sea continuación de las comillas, excepto
    # si precede un punto (creo que de eso se encarga la expresión)
    # anterior, pero mejor estar seguros.
    (r"([^\n\.:])\n[\"«»]", r'\1 '),

    # lines that end with " should probably precede a new line,
    # unless preceded by a comma,
    # or unless the new line starts with a lower-case letter;
    # and let's get rid of preceding space while we're at it
    (r"([^,])\ *\"\n([^a-z\n])", r'\1"\n\n\2'),

    # nueva línea y emdash probablemente es un diálogo y debería ser un nuevo párrafo
    (r"([^\n])\n—", r'\1\n\n—'),

    # remove single line breaks; preserve multiple.
    # but not if there's a tag, template or table syntax either side of the line break
    # mantener saltos de línea por el momento
    # (r"([^>}\|\n])\n([^:#\*<{\|\n])", r'\1 \2'),


    # collapse sequences of spaces into a single space
    (r"  +", r' '),

    # dump spurious hard breaks at the end of paragraphs
    (r"<br *\/?>\n\n", r'\n\n'),

    # caracteres fantasma
    (r"[�]", r''),

    # remove unwanted spaces around punctuation marks
    (r" ([;:\?!,\.])", r'\1'),

    # unicodify
    (r"&mdash;", r'—'),
    (r"&ndash;", r'–'),
    (r"&quot;", r'"'),

    # straighten quotes and apostrophes.
    (r"[“”]", r'"'),
    (r"[‘’`]", r'\''),

    # OCR fixes
    # números
    (r"([a-zA-ZáéíoúüÁÉÍÓU])1([a-zA-ZáéíoúüÁÉÍÓU])", r'\1l\2'),
    # (r"([a-zA-ZáéíoúüÁÉÍÓU])1", r'\1l'),
    # (r"([a-zA-ZáéíoúüÁÉÍÓU])0", r'\1o'),
    (r"([a-zA-ZáéíoúüÁÉÍÓU])0([a-zA-ZáéíoúüÁÉÍÓU])", r'\1o\2'),
    (r"([a-zA-ZáéíoúüÁÉÍÓU])[58~]([a-zA-ZáéíoúüÁÉÍÓU])", r'\1s\2'),
    (r"([a-zA-ZáéíoúüÁÉÍÓU])6", r'\1ó'),
    (r"([a-zA-ZáéíoúüÁÉÍÓU])0", r'\1o'),
    (r"([a-zA-ZáéíoúüÁÉÍÓU])6([a-zA-ZáéíoúüÁÉÍÓU])", r'\1é\2'),

    (r"!([a-záéíóú])", r'l\1'),
    (r"([a-záéíóú])l·([a-záéíóú])", r'\1r\2'),

    (r"_", r'—'),
    #(r" \(l", r' d'),
    (r"([^IVXLCDM])I\)", r'\1P'),  # defensa contra números romanos
    (r"([a-záéíóú])U", r'\1u'),

    # partículas y consonantongos que no existen, o casi no existen en español
    #(r"(!)c([.,;:!?}{\]\[\|\s]|$)", r'e\1'), # c final --> e
    (r"aiia", r'ana'),
    (r"abn([.,;:!?}{\]\[\|\s]|$)", r'aba\1'),
    (r"aiit", r'ant'),
    (r" hl([aeiouáéíóú])", r'bl\1'),
    #(r"hr", r'br'),
    (r"bn([^eiou ,.\n])", r'bu\1'),
    (r"cb", r'ch'),
    (r"Cb", r'Ch'),
    (r"chn([.,;:!?}{\]\[\|\s]|$)", r'cha\1'),
    (r"([ae])iit([eo])", r'\1nt\2'),
    (r"([ae])ute(s?[.,;:!?}{\]\[\|\s]|$)", r'\1nte\2'),
    (r"([scgp])nn", r'\1un'),
    #(r"clas([.,;:!?}{\]\[\|\s]|$)", r'das\1'),
    (r"dn([ds])([.,;:!?}{\]\[\|\s]|$)", r'da\1\2'),
    (r" rlic", r' dic'),
    (r" [rc]lil", r' dil'),
    (r"clel", r'del'),
    (r" cles", r' des'),
    (r" clis", r' dis'),
    (r"clici", r'dici'),
    (r"clig", r'dig'),
    (r"clv", r'dv'),
    (r"cou", r'con'),
    (r" lr", r' h'),
    (r" hn", r' hu'),
    (r"nii", r'mi'),
    (r" iia", r' na'),
    (r"iinp", r'imp'),
    (r"inip", r'imp'),
    (r"inb", r'mb'),
    (r"cne", r'cue'),
    (r"cleb", r'deb'),
    (r"snf", r'suf'),
    (r"ln([dn])", r'lu\1'),
    (r"rn([pb])", r'm\1'),
    (r" iuc", r' inc'),
    (r" llc", r' lle'),
    (r" inu(?!bnst)", r' mu'),  # inubicable
    (r" rn", r' m'),
    (r"rrn", r'rm'),
    (r"mh", r'mb'),
    (r"nibr", r'mbr'),
    (r"mcn", r'men'),
    (r"iiip", r'mp'),
    (r"inp", r'mp'),
    (r"enip(?!o)", r'emp'),
    (r"onip", r'omp'),
    (r" nne", r' nue'),
    (r"clia", r'cha'),
    (r" lia", r' ha'),
    (r"([^um])irn([^o])", r'\1im\2'),
    (r"([ae])iid", r'\1nd'),
    (r"((?!eu|Eu).{2})cli([jrd])", r'\1di\2'),
    (r"clist", r'dist'),
    (r"cnto([.,;:!?}{\]\[\|\s]|$)", r'ento\1'),
    (r"nndo([.,;:!?}{\]\[\|\s]|$)", r'ando\1'),
    (r"ii([.,;:!?}{\]\[\|\s]|$)", r'n\1'),
    (r"cl([.,;:!?}{\]\[\|\s]|$)", r'd\1'),
    (r"fc", r'fe'),
    (r"gnl", r'gul'),
    (r"jni", r'jui'),
    (r"lln([.,;:!?}{\]\[\|\s]|$)", r'lla\1'),
    (r"nlt", r'nst'),
    (r"iic", r'nc'),
    (r"oncli", r'ondi'),
    (r"pcr", r'per'),
    (r"pne", r'pue'),
    (r"oue", r'one'),
    (r"([rl])cs([.,;:!?}{\]\[\|\s]|$)", r'\1es\2'),
    (r"rcg", r'reg'),
    (r"([^eé])tn", r'\1tu'),
    (r" sne", r' sue'),  # etnia, étnico
    (r"tc([lm])", r'te\1'),
    (r"tiir", r'tur'),
    #			(r"urne", r'ume'),
    (r" Jni", r' Uni'),
    (r" vne", r' vue'),
    (r"xol", r'xcl'),
    (r" gn", r' gu'),
    (r"dn([oa])", r'du\1'),
    (r" oo", r' co'),
    (r"([n])eoe", r'\1ece'),
    (r"ncs", r'nes'),

    (r" i[)>]", r' p'),
    (r"(?<=[a-z] )Ja (?![Jj]a)", r'la '),  # evitar modificar Ja ja ja

    # fi --> ñ
    (r" afio", r' año'),
    (r" afiad", r' añad'),
    (r"ifia", r'iña'),
    (r"compafi", r'compañ'),
    (r"([Ee])spa[fi]ia", r'\1spaña'),
    (r"([Ee])spa[fi]iol", r'\1spañol'),
    (r"afier", r'añer'),
    (r"tafia", r'taña'),
    (r"([Ss])efia([lrb])", r'\1eña\2'),
    (r"([Ss])efior", r'\1eñor'),
    (r"efio([ .,\n])", r'eño\1'),
    (r"mafio([ .,\n])", r'maño\1'),
    (r"trafio", r'traño'),
    (r"empefi(?=o|a)", r'empeñ'),

    # tl —————> d (exepto en casos como atlas, y palabras aztecas)
    (r"otl", r'od'),
    (r"utl", r'ud'),
    (r"tlo([.,;:!?}{\]\[\|\s]|$)", r'do\1'),
    (r"itla", r'ida'),

    # OCR a español (gracias Aleator)


    (r"([Qq])(?:ll|n|li)", r'\1u'),
    (r"([Qq])(?:o|a|tl|u)([eié])", r'\1u\2'),
    #(r"([Qq])(?:o|a|tl|u)á", r'\1ué'),
    (r"([Qq])[aou][cers]", r'\1ue'),
    (r"([Qq])lll", r'\1ue'),
    (r"d([A-ZÁÉÍÓÚ])", r'¿\1'),
    (r" d([qcdhp])", r' ¿\1'),
    (r"([a-zó])[P]([ ,\n])", r'\1?\2'),

    (r" DO ", r' no '),
    (r" UTI ", r' un '),
    (r" UD ", r' un '),
    (r" UDA ", r' una '),
    (r" u n ", r' un '),
    (r" n ", r' a '),
    (r" d ", r' el '),
    (r" im ", r' un '),
    (r" ima ", r' una '),
    (r" tari ", r' tan '),
    (r" ([Aa])sl ", r' $sí '),

    (r" \'[lI]\'", r' T'),

    (r"]([a-záéíóú])", r'l\1'),

    (r"ﬁ", r'fi'),
    (r"ﬂ", r'fl'),
    (r" £[1l] ", r' El '),

    (r"((?!para).{4})noi([ao])", r'\1nci\2'),

    (r"au([.,;:!?}{\]\[\|\s]|$)", r'an\1'),
    (r"aha([.,;:!?}{\]\[\|\s]|$)", r'aba\1'),
    (r"ahan([.,;:!?}{\]\[\|\s]|$)", r'aban\1'),
    (r" (a[iïíìl!1I]g[anou][nu])", r' algun'),
    (r"(ag[un]a)", r'agua'),
    (r" afio ", r' año '),
    (r" ao", r' au'),
    (r"hle([.,;:!?}{\]\[\|\s]|$)", r'ble\1'),
    (r" oen", r' cen'),
    (r"c[iïíìl!1I][56d][un]([.,;:!?}{\]\[\|\s]|$)", r'ción\1'),
    (r"oio[un]", r'cion '),
    (r"oió[un]", r'ción '),
    (r"(c[iïíìl!1I][56d][ns][ce][8s] )", r'ciones '),
    (r"( co[ir]no )", r' como '),
    (r"( co[un][iïíìl!1I]o )", r' como '),
    (r"( eomo )", r' como '),
    (r"( [oce][op]n )", r' con '),
    (r"cb", r'ch'),
    (r" Kn", r'En'),
    (r"c[ïl!1I]o([.,;:!?}{\]\[\|\s]|$)", r'cio\1'),
    (r"dn([.,;:!?}{\]\[\|\s]|$)", r'da\1'),
    (r"tlar([.,;:!?}{\]\[\|\s]|$)", r'dar\1'),
    (r"tla([.,;:!?}{\]\[\|\s]|$)", r'da\1'),
    (r"( [tc][li]e )", r' de '),
    (r"( d[ocs] )", r' de '),
    (r"( ¿c )", r' de '),
    (r"( c[li][ce]l )", r' del '),
    (r"( d[ce][iïíìl!1I] )", r' del '),
    (r"(d[ce][s8])", r'des'),
    (r"(d[ïì1I])", r'di'),
    (r"([BE][iïíìl!1I] )", r'El '),
    (r"(EUa )", r'Ella '),
    (r" eua ", r' ella'),
    (r"( [ce][iïíìl!1I] )", r' el '),
    (r"( [ce][iïíìl!1I][iïíìl!1I]a)", r' ella'),
    (r"( [ce][iïíìl!1I][iïíìl!1I]o)", r' ello'),
    (r"(eU)", r'ell'),
    (r"cm(?!\.)", r'em'),
    (r"[BE][nqu] ", r'En '),
    (r" cnal", r' cual'),
    (r" cn", r' en'),
    (r"(?!cudo)[ce][un]do([.,;:!?}{\]\[\|\s]|$)",
     r'endo\1'),  # escudo, zancudo, picudo...`
    (r" [ce][un] ", r' en '),
    (r"[ce]n[lt] ", r'ent '),
    (r"[ce][un][lt]o[un][ce][ce]s", r'entonces'),
    (r"E[un][lt]o[un][ce][ce]s", r'Entonces'),
    (r" [ce]n[it]r[ce]", r' entre'),
    (r" [ce][as] ", r' es '),
    (r"[ce][8s][ce]ri", r'escri'),
    (r" [ce][s8][lt]([aáà])([ t])", r' est\1\2'),
    (r" [ce]t[ce]([.,;:!?}{\]\[\|\s]|$)", r' etc\1'),
    (r" é[8s][tl]([aoe])", r' ést\1'),
    (r"exlr", r'extr'),
    (r"(g[iïíìl!1I][6é]s)", r'glés'),
    (r"(?!ma)(..)g[nu]é", r'\1gué'),  # con excepción para magnético
    (r"gn[ce]", r'gne'),
    (r"gu[ce]", r'gue'),
    (r"ha[nu] ", r'han '),
    (r" ba ", r' ha '),
    (r" h[ns]b", r' hab'),
    (r" H[ns]b", r' Hab'),
    (r" hah", r' hab'),
    (r" Hah", r' Hab'),
    (r"[bh][oe]mbr[ce]", r'hombre'),
    (r"horn([^eoi])", r'hom\1'),

    (r"(?<!\.) (?!in|Ia)[iïíìl!1I][na] ", r' la '),
    (r" [iïíìl!1I][na][a8s] ", r' las '),
    (r" [iïíìl!1I]o ", r' lo '),
    (r" [iïíìl!1I][oO0][a8s] ", r' los '),

    (r"lloa([.,;:!?}{\]\[\|\s]|$)", r'llos\1'),
    (r"llaa([.,;:!?}{\]\[\|\s]|$)", r'llas\1'),
    (r"loient[ec]([.,;:!?}{\]\[\|\s]|$)", r'lmente\1'),
    (r"m[ce][un]t[ce]([.,;:!?}{\]\[\|\s]|$)", r'mente\1'),
    (r"m[ce]ot[ce]([.,;:!?}{\]\[\|\s]|$)", r'mente\1'),
    # excepción para teniente, poniente, conveniente, interviniente, concerniente y similares
    (r"(?!te|ve|po|.r|vi|Te|Po)(..)ni[ce]nt[ce]([.,;:!?}{\]\[\|\s]|$)", r'\1mente\2'),
    (r" mne", r' mue'),
    (r" misino([.,;:!?}{\]\[\|\s]|$)", r' mismo\1'),
    (r"(Q[anou][ce])", r'Que'),
    (r"([çq][anou][ce])", r'que'),
    (r"([çq]ii[ce])", r'que'),
    (r"q[a-záéíóú]e", r'que'),
    (r"q[a-záéíóú]é", r'qué'),
    (r"([çq][anou][óé])", r'qué'),
    (r" [sB][ec] ", r' se '),
    (r" sn ", r' su '),
    (r" tau ", r' tan '),
    (r"trn([.,;:!?}{\]\[\|\s]|$)", r'tra\1'),
    (r" TJ([a-z])", r' U\1'),
    (r" [un][un] ", r' un '),
    (r" [un](?:[un]|ii)a ", r' una '),
    (r" o[un]a ", r' una '),
    (r"(•)", r'.'),
    #(r"[lt!]'(?!'),", r'r'), -- demasiados falsos positivos
    (r"i([A-ZÁÉÍÓÚ])", r'¡\1'),

    # paréntesis en palabras
    (r"\(\)", r'o'),
    (r"\(\'\)", r'ó'),
    (r"l\)([a-záéíóú])", r'b\1'),
    (r"\(\(", r'«'),
    (r"\)\)", r'»'),
    (r"\(pi", r'qu'),
    (r"\(]", r'q'),
    (r"([^\]])]\)", r'\1p'),

    # espacio después de punto o coma
    (r"(\.)([A-ZÁÉÍÓÚ])", r'\1 \2'),
    (r"([,;:!\?])([a-záéíóú])", r'\1 \2'),
    #(r"([a-záéíóú][a-záéíóú][a-záéíóú])\. ([a-záéíóú][a-záéíóú])", r'\1, \2'),  demasiados falsos positivos con abreviaturas
    #(r"([a-záéíóú])\'([a-záéíóú])", r'\1 \2'), demasiados falsos positivos con palabras francesas e inglesas
    (r"\.-", r'.—'),
    (r"\^ ", r', '),
    #(r" P ", r'? '),
    (r"[.,][.,][.,]", r'...'),
    (r"([¿¡]) ", r'\1'),
    (r"\( ", r'('),
    (r" \)", ')')]
