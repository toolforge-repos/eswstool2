import pywikibot, re
from pywikibot import proofreadpage
from pywikibot import pagegenerators
from pywikibot.bot import CurrentPageBot

FileName= "output.txt"
file=open(FileName,'a')

site=pywikibot.Site('es', fam="wikisource")
site.login()

class MyBot(CurrentPageBot):

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        ppage = self.current_page

        label = INDEX.get_label_from_page(ppage)
        pagenum = ppage.title().split('/')[-1]

        index_item = re.search(r"{{[Tt][23]\|(.*?)}}", ppage.body)

        if index_item:
            file.write(' '.join([label, pagenum, index_item[1]])+'\n')
        

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    index = "Cautiverio feliz, y razón de las guerras dilatadas de Chile.pdf"
    
    global INDEX
    INDEX = proofreadpage.IndexPage(site, title="Index:"+index)

    gen_factory = INDEX.page_gen(only_existing=False, start=1, end=10, content=True)

    MyBot(generator=gen_factory).run()

if __name__ == '__main__':
    main()