import pywikibot
import re
import sys
import webbrowser
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot


site = pywikibot.Site('es', fam="wikisource")
site.login()


def main(*args: str):

    for arg in sys.argv[1:]:

        if arg[:5] == '-file':
            filename = arg[6:].strip('"')
    try:
        in_file = open(filename, 'r', encoding='utf8')
    except:
        raise Exception('Se necesita una lista formateada (-file:)')

    FILE = in_file.read()
    REGGIE = r"({\|\s*(?>{{(?>{{[^{}]+}}|[^{}])+}}\s*|{{[^{}]+}}\s*|<!--[ \w:]+-->\s*|\|[^|\n]+\s*)*)^(['\w\[( ]{3,}+)"

    for line in FILE.splitlines():
        if line:
            A = pywikibot.Page(site, title=line, ns=0)
            A.text = re.sub(r"(^<!--[ \w:]+-->) *", r"\1", A.text, flags=re.M)
            basurita = r"\|}\n</[Pp]oem>((?:<!--.*?-->)?)<[Pp]oem>\n{\|"
            A.text = re.sub(basurita, r'\1', A.text)

            RESULT = re.search(REGGIE, A.text, flags=re.S | re.M)
            pywikibot.output(A.title())
            switch = False
            choice = 'n'
            while RESULT:
                choice = 'y'
                if RESULT[2][0] not in "['(":
                    print(RESULT[2])
                    switch = True
                A.text = re.sub(REGGIE, r"\1|-\n|colspan=2|\2",
                                A.text, flags=re.S | re.M)
                RESULT = re.search(REGGIE, A.text, flags=re.S | re.M)

            if switch:
                choice = pywikibot.input_choice(
                    "ERGO: ", (('Yes', 'Y'), ('No', 'N'), ('Open in Browser', 'B')))
                if choice == 'b':
                    webbrowser.open(
                        "https://es.wikisource.org/w/index.php?title=" + A.title(as_url=True) + "&action=edit")
                    choice = pywikibot.input_choice(
                        "ERGO: ", (('Yes', 'Y'), ('No', 'N')))

            if choice == 'y':
                A.save('Arreglando [[Especial:Errores_de_sintaxis/fostered]]')


if __name__ == '__main__':
    main()
