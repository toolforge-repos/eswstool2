import pywikibot
import os
from pywikibot import pagegenerators
from pywikibot import FilePage, Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot
import copy
from PyPDF2 import PdfWriter, PdfReader
from pywikibot.specialbots import UploadRobot


def chop(filename):
    with open(filename, "rb") as pdf1:
        pdf = PdfReader(pdf1)
        output = PdfWriter()
        numpages = len(pdf.pages)
        #page2skip = int(input('Insert how many pages do you want to skip: '))
        page2skip = 0
        for i in range(page2skip, numpages):
            page = pdf.pages[i]
            pagebis = copy.copy(page)
            page.mediabox.upper_left = (
                page.mediabox.right / 2, page.mediabox.top,)
            output.add_page(page)
            pagebis.mediabox.upper_right = (
                pagebis.mediabox.right / 2, pagebis.mediabox.top,)
            output.add_page(pagebis)

        with open('_chop_' + filename, "wb") as newpdf:
            output.write(newpdf)


class MyBot(CurrentPageBot):

    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: a bot test edit with Pywikbot.',
        'custom': 'This is the value of a custom -custom:parameter'
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        filepage = FilePage(page)
        filename = filepage.title(with_ns=False).replace(' ', '_')
        if not os.path.exists(filename):
            filepage.download()
        chop(filename)
        bot = UploadRobot('_chop_' + filename, use_filename=filename,
                          chunk_size=1024 * 1024 * 2, verify_description=False,
                          target_site=Site('commons', fam='commons'),
                          summary='Chopping in half double-paged books',
                          description=filepage.text, ignore_warning=True)
        bot.run()

        # self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    # args += ('-commandline:argument',)
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('commons', fam="commons")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
