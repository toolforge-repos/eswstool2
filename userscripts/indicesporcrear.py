import pywikibot
import re
#import UTILS
from pywikibot.bot import ExistingPageBot
from pywikibot import pagegenerators, Page
import crawler

import utils.bib_utils as bu
import utils.ia_source as IA
import utils.template_utils as t_u
Commons = pywikibot.Site('commons', 'commons')
Wikisource = pywikibot.Site('es', fam='wikisource')


REXP = r"{{ *(?:at|a transcribir) *\| *(.+?) *(\|.+?|) *}}"
REXPIA = r"{{ *(?:ia|internet archive link) *\|(?: *id *= *)? *(.+?) *(\|.+?|) *}}"


class MyBot(ExistingPageBot):
    def __init__(self, **kwargs):
        self.always = False
        super().__init__(**kwargs)

    update_options = {
        'always': False,
        'summary': 'Bot - Enlaza',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text

        results = re.findall(REXP, text, flags=re.I)
        for r in results:
            filename = r[0].split(':')[-1]
            if filename.split('.')[-1] not in ["pdf", "djvu"]:
                continue

            index = Page(Wikisource, 'Índice:' + filename)
            if index.exists():
                continue

            P = crawler.crawl(filename, bot=True)
            #P = UTILS.page_dict(Page(Commons, 'File:' + filename))
            if not P:
                continue
            if len(P['title']) > 3:
                index.text = P.to_wikisource()
                if m := re.search(REXPIA, index.text, flags=re.I):
                    ia = IA.IaSource(m[1])
                    pl = ia.get_pagelist()
                    tag = pl.to_pagelist_tag(0)
                    index.text = t_u.set_index_data(
                        index.text, 'Paginas', tag, overwrite=True)
                    if len(t_u.get_index_data(index.text, 'Paginas')) > 16:
                        index.text = t_u.set_index_data(
                            index.text, 'Progreso', 'C', overwrite=True)
                print(index.text)

                if self.always:
                    choice = 'a'
                else:
                    choice = pywikibot.input_choice(
                    's/n/always', (('s', 's'), ('n', 'n'), ('a', 'a')), default='s')
                
                if choice == 's' or choice == 'a':
                    index.save('Bot - Creando índices a partir de Wikidata')

                if choice == 'a':
                    self.always = True
        # self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """
    args += ('-cat:Páginas que enlazan a índices por crear',)  # 106 Autor
    options = {}
    local_args = pywikibot.handle_args(args)

    # This factory is responsible for processing command line arguments
    # that are also used by other scripts and that determine on which pages
    # to work on.
    gen_factory = pagegenerators.GeneratorFactory()

    # Process pagegenerators arguments
    local_args = gen_factory.handle_args(local_args)

    # Parse your own command line arguments
    for arg in local_args:
        arg, _, value = arg.partition(':')
        option = arg[1:]
        if option in ('summary', 'text'):
            if not value:
                pywikibot.input('Please enter a value for ' + arg)
            options[option] = value
        # take the remaining options as booleans.
        # You will get a hint if they aren't pre-defined in your bot class
        else:
            options[option] = True

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)

    # check if further help is needed
    if not pywikibot.bot.suggest_help(missing_generator=not gen):
        # pass generator and private options to the bot
        bot = MyBot(generator=gen, **options)
        bot.run()  # guess what it does


if __name__ == '__main__':
    main()
