import os
import requests

USERNAME = os.getenv('BOTUSER')
PASSWORD = os.getenv('BOTPWD')

S = requests.Session()

URL = "https://es.wikisource.org/w/api.php"

# Retrieve login token first
PARAMS_0 = {
    'action': "query",
    'meta': "tokens",
    'type': "login",
    'format': "json"
}

R = S.get(url=URL, params=PARAMS_0)
DATA = R.json()

LOGIN_TOKEN = DATA['query']['tokens']['logintoken']

print(LOGIN_TOKEN)

# Send a post request to login. Using the main account for login is not
# supported. Obtain credentials via Special:BotPasswords
# (https://www.mediawiki.org/wiki/Special:BotPasswords) for lgname & lgpassword

PARAMS_1 = {
    'action': "login",
    'lgname': USERNAME,
    'lgpassword': PASSWORD,
    'lgtoken': LOGIN_TOKEN,
    'format': "json"
}

R = S.post(URL, data=PARAMS_1)
DATA = R.json()

print(DATA)
assert DATA['login']['result'] == 'Success'

# leer una lista y hacer lo mismo con cada artículo
for line in open("LIST", 'r', encoding='utf8').read().splitlines():
    # Step 3: GET request to fetch CSRF token
    PARAMS_2 = {
        "action": "query",
        "meta": "tokens",
        "format": "json"
    }

    R = S.get(url=URL, params=PARAMS_2)
    DATA = R.json()

    CSRF_TOKEN = DATA['query']['tokens']['csrftoken']

    # Step 4: POST request to edit a page
    PARAMS_3 = {
        "action": "changecontentmodel",
        "title": line,
        "summary": "Cambiando modelo de contenido a proofread-page",
        "model": "proofread-page",
        "token": CSRF_TOKEN,
        "bot": "true",
        "format": "json"
    }

    R = S.post(URL, data=PARAMS_3)
    DATA = R.json()

    print(DATA)
