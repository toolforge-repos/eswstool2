import pywikibot
import re
from pywikibot import pagegenerators

f = open("output.txt", "a", encoding="utf-8")

site = pywikibot.Site()
lista = set()

prefix = "Página:Diccionario etimolójico de las voces chilenas derivadas de lenguas indígenas americanas.djvu"
sear_pat = r";(.*)"

gen = pagegenerators.PrefixingPageGenerator(prefix)

for page in gen:
    #Do something with the page object, for example:
    print("Scrapping: "+page.title())
    text = page.text
    matches = re.findall(sear_pat,text)
    for match in matches:
    	lista.add(match)

for elem in lista:
	f.write(elem+"\n")