import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot
from utils import template_utils as tu


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Semi-automatic edit: fixing Memoria Chilena links',
    }

    def treat_page(self):
        page = self.current_page
        text = re.sub(r'\{\{Memoria Chilena *\| *1? *=? *?(\d+) *\}\}',
            r'{{Memoria Chilena|1=http://www.memoriachilena.cl/602/w3-article-\1.html}}', page.text)

        self.put_current(text, summary=self.opt.summary)


def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory(site=pywikibot.Site('commons', 'commons'))
    gen_factory.handle_arg('-transcludes:Memoria Chilena')
    gen_factory.handle_arg('-pt:15')
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value

    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()


if __name__ == '__main__':
    main()
