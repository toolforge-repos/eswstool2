#!/usr/bin/env python
'''
Created on 7 jul 2024

@author: WS
'''
import requests
from bs4 import BeautifulSoup
from PyPDF2 import PdfWriter, PdfReader
import os
import re
import json
from os.path import exists
from tqdm import tqdm
import winsound
import argparse

class HathiTrust(object):
    '''
    classdocs
    '''
    hathitrust_url = ""
    identifier = ""
    hathitrust_url_page = ""
    working_proxy = ""
    valid_proxies = []
    fullview = False
    pdfmetadata = {} #Metadata dictionary
    total_pages = 1
    working_proxy = None
    # Convertir las dimensiones del recorte a puntos (1 mm = 2.83465 puntos)
    crop_left = 8 * 2.83465
    crop_bottom = 12 * 2.83465

    def __init__(self, iden):
        '''
        Constructor
        '''
        self.hathitrust_url = "https://babel.hathitrust.org/cgi/pt?id=" + iden
        self.identifier = iden
        self.hathitrust_url_page = "https://babel.hathitrust.org/cgi/imgsrv/download/pdf?id=" + iden + "&attachment=1&tracker=D1&seq="
        self.fullview = self.extract_fullview()
        self.pdfmetadata = self.extract_metadata()
        self.total_pages = self.getTotalPages()
    
    def beep(self):
        duration = 1000  # Duración en milisegundos
        freq = 440  # Frecuencia en Hertz
        winsound.Beep(freq, duration)
    
    def extract_fullview(self):
        response = requests.get(self.hathitrust_url)
        
        if response.status_code != 200:
            raise Exception(f"Failed to load page {self.hathitrust_url}")
    
        soup = BeautifulSoup(response.content, 'html.parser')
        
        # Buscar el script que contiene los metadatos
        script_tag = soup.find('script', string=lambda text: text and 'HT.params.finalAccessAllowed' in text)
        
        if not script_tag:
            raise Exception("No se encontró el script con los datos de la página HathiTrust")
        
        # Extraer y parsear el contenido del script
        script_content = script_tag.string
        start = script_content.find('HT.params.finalAccessAllowed =') + len('HT.params.finalAccessAllowed =')
        end = script_content.find(';', start) 
        json_data = script_content[start:end].strip()
        
        # Convertir el contenido JSON variable booleana
        data = json.loads(json_data)
        if data:
            print('Full view')
        else:
            print('Downloading with proxy')
        return data
    
    def extract_metadata(self):
        response = requests.get(self.hathitrust_url)
        
        if response.status_code != 200:
            raise Exception(f"Failed to load page {self.hathitrust_url}")
    
        soup = BeautifulSoup(response.content, 'html.parser')
        
        # Buscar el script que contiene los metadatos
        script_tag = soup.find('script', string=lambda text: text and 'HT.params.metadata' in text)
        
        if not script_tag:
            raise Exception("No se encontró el script con los metadatos")
        
        # Extraer y parsear el contenido del script
        script_content = script_tag.string
        start = script_content.find('HT.params.metadata =') + len('HT.params.metadata =')
        end = script_content.find('};', start) + 1
        json_data = script_content[start:end].strip()
        
        # Convertir el contenido JSON a un diccionario
        data = json.loads(json_data)
        titulo = str(data.get('title', 'N/A'))
        print(f'Downlading {titulo} ')
        # Extraer los metadatos relevantes
        return {
            '/Title': data.get('title', 'N/A'),
            '/Author': data.get('author', 'N/A'),
            '/Subject': 'N/A',  
            '/Keywords': 'N/A',  
            '/Creator': 'Python Script',
            '/Producer': 'PyPDF2',
            '/CreationDate': 'D:20240101000000',  
            '/ModDate': 'D:20240101000000',  
            '/Publisher': data.get('publisher', 'N/A'),
            '/PublicationDate': data.get('publicationDate', 'N/A'),
            '/CatalogRecordNumber': data.get('catalogRecordNo', 'N/A'),
            '/Format': data.get('format', 'N/A'),
            '/EnumChron': data.get('enumChron', 'N/A'),
            '/Description': data.get('description', 'N/A')
        }

    def get_proxies(self):
        url_list = "https://www.us-proxy.org/"
        response = requests.get(url_list)
        soup = BeautifulSoup(response.text, 'html.parser')
        
        proxies = []
        for row in soup.find("table", attrs={"class": "table table-striped table-bordered"}).find_all("tr")[1:]:
            columns = row.find_all("td")
            if columns[4].text == "anonymous" and columns[6].text == "yes":
                proxy = f"{columns[0].text}:{columns[1].text}"
                proxies.append(proxy)        
        return proxies
    
    def test_proxy(self, proxy):
        try:
            url = self.hathitrust_url_page + "1"
            response = requests.get(url, proxies={"http": proxy, "https": proxy}, timeout=5)
            if response.status_code == 200:
                return True
        except:
            return False
    
    def get_working_proxy(self,proxies):
        for proxy in proxies:
            if self.test_proxy(proxy):
                return proxy
        return None
    
    def get_valid_proxies(self,proxies):
        for proxy in proxies:
            if self.test_proxy(proxy):
                self.valid_proxies.append(proxy)
                
    
    def getTotalPages(self): # count item's number of pages
        html = requests.get(self.hathitrust_url)
        soup = BeautifulSoup(html.text, "html.parser")
        #Buscar el script de la pagina de HathiTrut
        buscar= soup.find("script").getText()
        match = re.search(r'HT.params.totalSeq\s+=\s+(\d+)', buscar)
        #Buscar la cantidad de páginas
        npages = int(match.group(1))
        print(f'Total pages: {npages}')
        return npages

    def PDFDownload(self,output_line,filename):
        while True :
            if self.working_proxy: #If not fullview
                try :
                    PDF_download = requests.get(output_line, stream=True, timeout=300, proxies={"http": self.working_proxy, "https": self.working_proxy})
                except: #Try new proxy
                    print("Error de proxy, todavía no se descargó")
                    proxies = self.get_proxies( ) #get_proxies()
                    print(f"Found {len(proxies)} proxies.")
                    anterior = self.working_proxy
                    self.working_proxy = self.get_working_proxy(proxies)
                    if self.working_proxy:
                        print(f"Working proxy found: {self.working_proxy}")
                        continue
                    else:
                        print("No working proxy found.")
                        self.working_proxy = anterior #back it
                        continue
            else: #If fullview
                PDF_download = requests.get(output_line, stream=True, timeout=300)
            with open(filename, 'wb') as f:
                f.write(PDF_download.content)
            if os.stat(filename).st_size < 10000:
                os.remove(filename)
                continue
            break
        
    def save_and_merge(self):

        while not(self.fullview):
            proxies = self.get_proxies()
            print(f"Found {len(proxies)} proxies.")
            self.working_proxy = self.get_working_proxy(proxies)
            if self.working_proxy:
                print(f"Working proxy found: {self.working_proxy}")
                break;
            else:
                print("No working proxy found. Continue? Y/N")
                salida = input()
                if salida in { "N", "Not", "No","NO", "NOT" }:
                    exit()

        print(self.identifier)
        if len(self.identifier)>0 :
            # Create empty list of pages that will be merged
            pages = []
            # Save every page as pdf
            for i in tqdm(range(1, self.total_pages + 1),desc="Downloading..."):
                #time.sleep(1)
                url = self.hathitrust_url_page + str(i)
                filename = "page" + str(i) + ".pdf"
                if not(exists(filename)):
                    self.PDFDownload(url, filename)
                pages.append("page"+str(i)+".pdf")
                os.system("clear")
            # Merging
            merger = PdfWriter()
            for page in tqdm(pages,desc="Merging..."):
                merger.append(page)
            merger.add_metadata(self.pdfmetadata)
            merger.write(self.identifier + ".pdf")
            merger.close()
            
            print("File " + self.identifier +".pdf" + " is done.")
            
            for page in pages:
                os.remove(page)
                
            self.beep()
        else:
            print("Full View is not available.")
        
    def crop_pdf(self,left = 6, bottom = 12):
        input_pdf = self.identifier + ".pdf"
        output_pdf = self.identifier + "_crop.pdf"
        # Convertir las dimensiones del recorte a puntos (1 mm = 2.83465 puntos)
        crop_left = left * 2.83465
        crop_bottom = bottom * 2.83465
        
        reader = PdfReader(input_pdf)
        writer = PdfWriter()
        # Crear una barra de progreso
        # Recorrer todas las páginas del PDF
        for page_num in tqdm(range(len(reader.pages)),desc="Cropping..."):
            page = reader.pages[page_num]
                        
            # Cropping
            page.mediabox.lower_left = (page.mediabox.left + int(crop_left), page.mediabox.bottom + int(crop_bottom))
            page.mediabox.upper_right = (page.mediabox.right, page.mediabox.top)
            
            # Add page to output file
            writer.add_page(page)
        
        # Write to output file
        writer.add_metadata(self.pdfmetadata)
        writer.write(output_pdf)
        writer.close()
        print("File "+ output_pdf +" is done.")
        self.beep()
    
    @staticmethod 
    def getViewability(record):
        
        response = requests.get("https://catalog.hathitrust.org/Record/" + record)
        soup = BeautifulSoup(response.text, 'html.parser')
    
        listado = {}
        for row in soup.find("table", attrs={"class": "table-branded viewability-table"}).find_all("tr")[1:]:
            columns = row.find_all("td")
            
            listado |= {columns[0].find('a',href=True)['href'].removeprefix("//babel.hathitrust.org/cgi/pt?id=") : columns[1].get_text().strip()}        
        return listado
        
parser = argparse.ArgumentParser(description='HathiTrut downloader.',epilog='Ejemplos:')
parser.add_argument('id', type=str, help='HathiTrust identifier by default')
parser.add_argument('-r', '--record', action='store_true', help='Download files from identifier Record Catalog')
args = parser.parse_args()

if args.record:
    libros = HathiTrust.getViewability(str(args.id))
    for identifier, institution in libros.items():
        print(f'File {identifier}.pdf, from {institution}')
    for identifier, institution in libros.items():
        print(f'Downlading {identifier}.pdf, from {institution}')
        libro =  HathiTrust(identifier)
        libro.save_and_merge()
        libro.crop_pdf()
else:
    libro =  HathiTrust(args.id)
    libro.save_and_merge()
    libro.crop_pdf()