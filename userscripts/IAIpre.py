import pywikibot
import re
import requests
from pywikibot import pagegenerators
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot

URL = 'http://resolver.iai.spk-berlin.de/'
REXP = r'{{IAI\| *(?:id *= *)?(\w+)'


class MyBot(CurrentPageBot):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: cambiando ID para {{IAI}}',
        'custom': 'This is the value of a custom -custom:parameter'
    }
    def get_ID(self, ID):
        page = requests.get(f'{URL}{ID}')
        ID = re.search(r'\d+', page.url)[0]
        return ID

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        old_text = page.text
        text = page.text

        for m in re.findall(REXP, text):
            ID = self.get_ID(m)
            REXP2 = REXP.replace('(\\w+)', m)
            text = re.sub(REXP2, rf'{{{{IAI|{ID}', text)

        if old_text != text:
            self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    options = {}
    args = pywikibot.handle_args(args)
    site = pywikibot.Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    gen_factory.handle_arg('-transcludes:IAI')

    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
