import re
from bs4 import BeautifulSoup, Tag
from urllib.request import urlopen
import UTILS

URL = 'https://www.gutenberg.org/browse/languages/es'
html = urlopen(URL).read()
soup = BeautifulSoup(html, features="lxml")
file = open('GUTENBERG', 'a')

h2 = soup.find_all('h2')
for element in h2:
    autor = element.text
    proc = UTILS.process_human(autor)
    ul = element.find_next_sibling()
    if ul.name == 'ul':
        print(f'\n== {UTILS.get_author(proc)[:-3]}|{autor}]] ==', file=file)
        for li in ul:
            for a in li:
                if hasattr(a, 'name') and a.name == 'a':
                    print(f'*[[{a.text}]]<small>[https://www.gutenberg.org{a["href"]} PG]</small>', file=file)
