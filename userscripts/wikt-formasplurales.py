import pywikibot, re, roman, time
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot


## wikicosas
site = pywikibot.Site('es', 'wiktionary')

class MyBot(ExistingPageBot):

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        title = self.current_page.title()
        text = self.current_page.text

        ##TEMPLATE = r'\{\{(?:forma sustantivo plural|f.s.p.|plural)(?:\|leng=es)?\|(.*?)(?:\|leng=es)?}}' 
        ##TEMPLATE2 = r'\{\{(?:forma sustantivo)(?:\|leng=es)?\|(.*?)(?:\|leng=es)?\|plural(?:\|leng=es)?}}'
        
        TEMPLATE = r'\{\{(?:forma adjetivo)(?:\|\w+=\w+)*\|([^}\|=]*?)(?:\|\w+=\w+)*}}' 
        TEMPLATE2 = r'\{\{(?:f.adj2|forma adjetivo 2)\|([^}\|=]*).*?}}'



        PATTERN = r'(?:'+TEMPLATE+r'|'+TEMPLATE2+r')'
        
        mash = re.search(PATTERN, text)
        word = mash and mash.group(1)
        if word and not pywikibot.Page(site, mash.group(1)).exists():
            with open('LIST', 'a') as f:
                f.write('*[['+word+']] ([['+title+']])\n')

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value
    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()

if __name__ == '__main__':
    main()