import pywikibot, re
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

site=pywikibot.Site('es', fam="wikisource")

def get_template_data(page, template, parameter, from_link=False):
    temps = page.raw_extracted_templates
    params = [t[1] for t in temps if t[0].lower() == template.lower()]
    if len(params) == 0:
        return False # False = no hay plantilla, None = no hay parámetro
    
    params = {k.lower():v for k,v in params[0].items()}
    value = params.get(parameter.lower())
    return value
    
def get_index_data(page, param, from_link=False):  
    value = get_template_data(page, ':Mediawiki:Proofreadpage_index_template', param)    
    if value and from_link:
        PATTERN2 = r'\'*\[*([^\[\]\|\{\}]+)(?:\|[^\[\]\|\{\}]+)?\]*'
        research = re.search(PATTERN2, value)
        return research and research.group(1)
    else:
        return value
    
def set_index_data(page, param, setto, overwrite=False, create=False, delete=False):
    text = page.text
    value=get_index_data(page, param)
    PATTERN = rf'\|([{param[0].lower() + param[0].upper()}]{param[1:]})\s*=\s*({re.escape(str(value))})\s*'

    Param = rf'{param[0].upper()}{param[1:]}'

    if value == False:
        return text
    elif value == None and create:
        text = re.sub('}}$', rf'|{Param}={setto}\n}}}}', text)
    elif value == '':
        text = re.sub(PATTERN, rf'|{Param}={setto}\n', text)
    elif not setto and delete:
        text = re.sub(PATTERN, rf'|{Param}=\n', text)
    elif value and overwrite:
        text = re.sub(PATTERN, rf'|{Param}={setto}\n', text)
    return text

class MyBot(ExistingPageBot):

    update_options = {
        'always': True,
        'summary': 'Bot: agregando ID de Wikidata.',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text
        filename = self.current_page.title(with_ns=False)
        
        commons = pywikibot.Page(pywikibot.Site('commons', 'commons'), 'File:'+filename)
        wikidata =  get_template_data(commons, 'Book', 'wikidata')
        if wikidata:
            text = set_index_data(page, 'wikidata', wikidata, create=True)
            self.put_current(text, summary=self.opt.summary)
            return True
        else:
            import crawler
            P = crawler.crawl(commons.title(), bot=self.opt.always)
            if wikidata := P.get('_QID'):
                text = set_index_data(page, 'wikidata', wikidata, create=True)
                self.put_current(text, summary=self.opt.summary)
                return True

        titulo = get_index_data(page, 'titulo', from_link=True)
        if titulo and len(titulo)>250: titulo=''
        volumen = get_index_data(page, 'volumen', from_link=True)
        ppage = None
        
        if titulo:
            voltitulo = pywikibot.Page(site, titulo)
            if voltitulo.exists():
                ppage = voltitulo
        if volumen:
            volpage = pywikibot.Page(site, volumen)
            if volpage.exists():
                ppage = volpage
        try:
            item = ppage.data_item()
            wikidata = ppage.data_item().getID()
            text = set_index_data(page, 'wikidata', wikidata)
        except:
            pass
        self.put_current(text, summary=self.opt.summary)

def main(*args: str):
    """Parse command line arguments and invoke bot."""

    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    options={}
    args += ('-cat:Índices no conectados a Wikidata',)

    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value

    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()

if __name__ == '__main__':
    main()