import re
from pywikibot import Page, Site, SiteLink

PROGRESS = 'Resoluciones del Consejo de Seguridad de las Naciones Unidas/progreso'


def treat(page):
    resyear = re.search(r'(\d+)', page.title())
    entitle = f'United Nations Security Council Resolution {resyear[1]}'
    enwiki = Page(Site('en', 'wikipedia'), entitle)
    if enwiki.exists():
        item = enwiki.data_item()
        if 'eswikisource' in item.sitelinks:
            return
        newsl = SiteLink(
            title=page.title(),
            site=page.site)
        print('setting badge to sitelink')
        try:
            item.setSitelink(
            newsl, summary='Adding sitelink to UN Resolutions')
        except Exception as e:
            print(e)


def main(*args: str) -> None:
    page = Page(Site('es', 'wikisource'),
                'Resoluciones del Consejo de Seguridad de las Naciones Unidas')
    total = 0
    created = 0
    for link in page.linkedPages(namespaces=[0]):
        total += 1
        if link.exists():
            created += 1
            treat(link)

    page = Page(Site('es', 'wikisource'), PROGRESS)

    page.text = re.sub(r'(?<=<!--1-->)\d+', f'{created}', page.text)
    page.text = re.sub(r'(?<=<!--2-->)\d+', f'{total}', page.text)
    page.text = re.sub(r'(?<=<!--3-->)[\d.%]+',
                       f'{100*created/total:.2f}', page.text)

    page.save(
        summary='Bot - Actualizando progreso en [[Resoluciones del Consejo de Seguridad de las Naciones Unidas]]')


if __name__ == '__main__':
    main()
