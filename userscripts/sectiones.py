import pywikibot
import webbrowser
import re
from pywikibot import pagegenerators
from BASEINDEXBOT import IndexBot


class FormatError(Exception):
    pass


def decompose_sections(text):
    BEGIN = r'< *section *begin *= *"([^"]*?)"(?:[^/>]*) */>'
    END = r'< *section *end *= *"\1"(?:[^/>]*) */>'

    t = []
    first = 0
    last = 0

    for m in re.finditer(fr'{BEGIN}(.*?){END}', text, flags=re.DOTALL):
        start = m.start()
        last = m.end()
        if start > first:
            t.append([None, text[first:start], first])
        first = last
        t.append([m[1], m[2], start])

    if last < len(text):
        t.append([None, text[last:], last])

    if recompose_sections(t) == text:
        return t

    raise FormatError('Secciones del texto no tienen un formato regular')


def recompose_sections(t):
    result = ''
    for e in t:
        if e[0] and e[1]:
            result += f'<section begin="{e[0]}"/>{e[1]}<section end="{e[0]}"/>'
        else:
            result += e[1]
    return result


class MyBot(IndexBot):

    update_options = {
        'always': False,
        'summary': 'Bot: segmentando secciones para transclusión',
    }

    def __init__(self, index, *args, **kwargs):
        super().__init__(index, *args, **kwargs)

        self.TOC = {}
        self.current_chapter = None

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        old_text = page.body
        n = int(page.title().split('/')[1])
        decomp = decompose_sections(page.body)

        save = True
        if len(decomp) > 1:
            print('ya segmentada:', n)
            save = False
        for m in re.finditer(r'{{ *t[23] *\| *(?:cap[íi]tulo)? *([^|}]+?) *[}|]',
                             page.body, flags=re.I):
            cur = self.current_chapter
            chapter = m[1]
            start = m.start()

            if not self.TOC.get(chapter):
                # Segmentar Página
                cur_start = decomp[-1][2]
                decomp[-1][1] = decomp[-1][1][cur_start:start]

                self.TOC[chapter] = {'from': n}
                if cur:
                    decomp[-1][0] = cur
                    self.TOC[cur]['to'] = n

                decomp.append([chapter, page.body[start:], start])

                self.current_chapter = chapter
            else:
                print(self.TOC)
                webbrowser.open(
                    "https://es.wikisource.org/w/index.php?title=" + page.title(as_url=True) + "&action=edit")

        page.body = recompose_sections(decomp)
        # decomp = decompose_sections(page.body)

        if save and old_text != page.body:
            pywikibot.showDiff(old_text, page.body)
            if self.opt.always:
                choice = 'a'
            else:
                choice = pywikibot.input_choice(
                    'Do you want to accept these changes?',
                    [('Yes', 'y'), ('No', 'n'), ('all', 'a')],
                    default='N')

            if choice == 'y' or choice == 'a':
                page.save()

                if choice == 'a':
                    self.opt.always = True


def main(*args: str):
    '''Plantilla para bots que trabajan en ProofreadPage'''
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value

    index = 'Índice:Rafael. Páginas de los veinte años (1920).pdf'

    NS0 = MyBot(index=index, **options)
    NS0.run()

    print(NS0.TOC.keys())


if __name__ == '__main__':
    main()
