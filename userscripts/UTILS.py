import os
import requests
import re
from tqdm import tqdm
import pywikibot
from pywikibot.specialbots import UploadRobot
import roman
from utils.bib_utils import PUBLISHERS


BOOK_PARAMS = 'Author=|Translator=|Editor|Illustrator|Introducer|Title=|'\
    'Subtitle|Series_title|Volume=|Edition|Publisher=|Printer|Date=|'\
    'Publication_date|City=|Language=es|Description=|Source=|Permission=|'\
    'Image|Image_page=|Pageoverview|Wikisource=s:es:Índice:{{PAGENAME}}|'\
    'Homecat|Other_versions|'\
    'ISBN|LCCN|OCLC|References|Linkback|Wikidata=|noimage'

Commons = pywikibot.Site('commons', 'commons')
Wikisource = pywikibot.Site('es', fam='wikisource')
Wikipedia = pywikibot.Site('es', fam='wikipedia')
EnWikisource = pywikibot.Site('en', fam='wikisource')
Wikidata = pywikibot.Site('wikidata', 'wikidata')
repo = Wikidata.data_repository()

ROLES = ['author', 'translator', 'editor', 'illustrator', 'introducer']


def bad_chars(s):
    import utils.string_utils
    return utils.string_utils.bad_chars(s)


def is_roman(s):
    """Summary

    Args:
        s (TYPE): Description

    Returns:
        TYPE: Description
    """
    try:
        roman.fromRoman(s)
        return True
    except Exception:
        return False


def download_pdf_file(url: str, filename: str, chunk_size=10**5, verify=True):
    """Summary

    Args:
        url (str): Description
        filename (str): Description
        chunk_size (TYPE, optional): Description

    Returns:
        TYPE: Description
    """
    response = requests.get(url, stream=True,
                            headers={'User-agent': 'Mozilla/5.0'}, verify=verify)
    if response.status_code == 200:
        # Save in current working directory

        total = int(response.headers.get('content-length', 0))
        # if total < 1024:
        #    return
        # Crear directorio

        try:
            os.mkdir('PDF')
        except Exception:
            pass

        filepath = os.path.join(os.getcwd() + '\\PDF', filename)

        if os.path.isfile(filepath):
            print(f'{filename} ya se descargó antes.')
            return filepath
        with open(filepath, 'wb') as file, \
            tqdm(desc=filename, total=total, unit='iB',
                 unit_scale=True, unit_divisor=1024,) as bar:
            for data in response.iter_content(chunk_size=chunk_size):
                size = file.write(data)
                bar.update(size)
            print('\a')
            return filepath
    else:
        print(f'Uh oh! Could not download {filename},')
        print(f'HTTP response status code: {response.status_code}')
        return


def upload_to_commons(path, filename, commonsText, query=None, force=False):
    # cada scraper debe tener exactamente la misma estructura para que funcione

    if query:
        search = Commons.search(query, namespaces='File', total=1)
        for result in search:
            pywikibot.output(f'<<lightred>>{result.title()}')
            choice = pywikibot.input_choice("Al parecer ya está subido en Commons."
                                            " Saltar?", (('Sí', 'S'), ('No', 'N'), ('Usar este', 'U')))
            if choice == 's':
                return filename
            elif choice == 'u':
                return result.title()

    bot = UploadRobot(path, description=commonsText, use_filename=filename,
                      chunk_size=1024 * 1024, verify_description=force,
                      target_site=Commons)

    # bot.last_filename QEPD
    bot.filename = ''

    def _(old, new):
        bot.filename = new

    bot.post_processor = _
    try:
        bot.run()
    except pywikibot.exceptions.APIError:
        return filename
    except Exception as exc:
        print(exc)
        bot.chunk_size = 1024 * 1024
        bot.run()

    print('\a')
    return bot.filename


def index_dict(page):
    translate = {'Titulo': 'title', 'Subtitulo': 'subtitle',
                 'Volumen': 'volume', 'Autor': 'author', 'Editor': 'editor',
                 'Prologuista': 'introducer', 'Traductor': 'translator',
                 'Imprenta': 'printer', 'Editorial': 'publisher',
                 'Ilustrador': 'illustrator', 'Ano': 'date',
                 'Lugar': 'city', 'Fuente': 'source', 'Imagen': 'image_page',
                 'Wikidata': '_QID', 'Paginas': '_pagelist', 'Progreso': '_progreso'}

    temps = page.raw_extracted_templates
    params = [t[1] for t in temps if t[0].lower(
    ) == ':mediawiki:proofreadpage_index_template']

    if len(params) == 0:
        return False

    D = {}
    for k, v in params[0].items():
        if v:
            D[translate.get(k, '__')] = clean(v).strip()
    D['_filename'] = page.title(with_ns=False)
    return D


def get_author(value):
    QID = search_ID(value)
    if QID:
        link = get_link(QID)
        if link:
            return f'[[Autor:{link}|]]'
    return f'[[Autor:{value}|]]'


def get_publisher(value, bot=False):
    if not value:
        return None

    #{{institution:}}
    value = re.sub(r"{{\w+:(.*?)}}", r"\1", value)
    for REXP in PUBLISHERS:
        if re.search(REXP[0], value, flags=re.I):
            return REXP[1]

    if bot:
        return None

    search = Wikidata.search_entities(value, 'es', total=3, uselang='es')
    lista = []

    for i, result in enumerate(search):
        pywikibot.output(f"<<lightyellow>>[{i + 1}] {result['id']:10} "
                         f"<<lightgray>> {result['label']} : "
                         f" {result.get('description', '')}")
        lista.append(result['id'])

    if len(lista) > 0:

        i = pywikibot.input(f'{value}: cualquier letra = ninguno')
        if i.isdigit():
            return lista[int(i) - 1]

    return None


def get_dict_cats(P):
    return P.get_dict_cats()


def search_ID(value):
    """Summary

    Args:
        value (TYPE): Description

    Returns:
        TYPE: Description
    """
    if not value:
        return None
    value = value.strip(" :")
    if m := re.search(r"(Q\d+)", value):
        return m[1]
    if re.search(r'{{[Aa]nonymous}}', value):
        return None
    if m := re.search(r'\s*{{[ ]*[Cc]reator[ ]*[:\|][ ]*(.*?)[ ]*}}\s*', value):
        value = m[1]

    value = re.sub(r'\[\[([^|\]]+)(?:\|[^|\]]*)?\]\]', r'\1', value)
    value = re.sub(r'^[A-Z][a-z]+:', r'', value)

    value = value.strip(" :")

    page = pywikibot.Page(Commons, 'Creator:' + value)
    if page.exists():
        if page.isRedirectPage():
            page = page.getRedirectTarget()
        if m := re.search(r'(Q\d+)', page.text):
            return m[1]

    wspage = get_ID(Wikisource, 'Autor:' + value)
    if wspage:
        return wspage

    wikipage = get_ID(Wikipedia, value)
    if wikipage:
        return wikipage

    enwspage = get_ID(EnWikisource, 'Author:' + value)
    if enwspage:
        return enwspage

    return None
