import pywikibot
from pywikibot import pagegenerators, Site
from ubercrawler import CrawlBot
import sources


def main(*args: str) -> None:
    args = pywikibot.handle_args(args)

    source = args[0]

    if source not in sources.__all__:
        raise NotImplementedError(f'Módulo {source} no disponible')

    _module = __import__(f'sources.{source}', fromlist=[source])
    module = getattr(_module, source)

    CrawlBot.MODULE = module

    TEMPLATE = module.TEMPLATE

    gen_factory = pagegenerators.GeneratorFactory(
        site=Site('es', 'wikisource'))
    gen_factory.handle_arg(f'-transcludes:{TEMPLATE}')
    gen_factory.handle_arg('-namespace:0,1,100,106')  # 100

    gen = gen_factory.getCombinedGenerator()

    bot = CrawlBot(generator=gen)
    bot.run()


if __name__ == '__main__':
    main()
