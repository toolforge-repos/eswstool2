import pywikibot, re, roman
from pywikibot import pagegenerators
from pywikibot.bot import ExistingPageBot

class MyBot(ExistingPageBot):


    update_options = {
        'always': False,
        'text': 'This is a test text',
        'summary': 'Bot: completando parámetros encabezado',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        MIN_NUM = 1
        MAX_NUM = 29 

        title = self.current_page.title()
        subpage_num = int(re.search(r'(?:[\w ]+/)*([\w ]+)', title).group(1))

        text = self.current_page.text
        PREV_POS = re.search(r'anterior\s*=\s*',text).end()
        if subpage_num > MIN_NUM:
            text = text[:PREV_POS] + '[[../' + f"{subpage_num-1:0>2}" + '/|Capítulo '+roman.toRoman(subpage_num-2)+']]\n ' + text[PREV_POS:] 

        NEXT_POS = re.search(r'próximo\s*=\s*',text).end()
        if subpage_num < MAX_NUM:
            text = text[:NEXT_POS] + '[[../' + f"{subpage_num+1:0>2}" + '/|Capítulo '+roman.toRoman(subpage_num)+']]\n ' + text[NEXT_POS:] 

        #text += '\n' + self.opt.text
        self.put_current(text, summary=self.opt.summary)

def main(*args: str):
    """Parse command line arguments and invoke bot."""
    options = {}
    gen_factory = pagegenerators.GeneratorFactory()
    # Option parsing
    local_args = pywikibot.handle_args(args)  # global options
    local_args = gen_factory.handle_args(local_args)  # generators options
    for arg in local_args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text'):
            options[opt[1:]] = value
    MyBot(generator=gen_factory.getCombinedGenerator(), **options).run()

if __name__ == '__main__':
    main()