'''
Se puede ejecutar sin parametros:

    python pwb.py OCRBot

o con cualquiera de los siguientes parámetros:

:primer parámetro sin nombre: el nombre del Índice, con o sin prefijo
:-tipo: un número del 1 al 4
:-head: el texto del encabezado
:-method: uno de los siguientes: 'wmfOCR' o 'googleOCR'
'''
import pywikibot
import re
import roman
from pywikibot.proofreadpage import IndexPage
from pywikibot import Site
from pywikibot.bot import CurrentPageBot
import utils.pagelist


def is_roman(s):
    try:
        roman.fromRoman(s)
        return True
    except Exception:
        return False


class PagelistBot(CurrentPageBot):

    update_options = {
        'always': False,
        'summary': 'Bot: generando pagelist',
        'tipo': 0,
        'head': None,
        'method': None,
        'tuples': [],
        'columna': False,
    }

    def __init__(self, index=None, start=1, end=None, **kwargs) -> None:
        prefix = 'Índice:'
        if index is None:
            index = pywikibot.input('Ingresa el Índice, con o sin el prefijo')
        index = index if index.startswith(prefix) else prefix + index

        self.pl = utils.pagelist.PageList()
        self.index = IndexPage(Site('wikisource:es'), index)
        self.last_num = 1

        while not self.index.exists():
            index = pywikibot.input(
                f'<<lightred>>{self.index.title()} no existe, '
                f'ingresa un Índice creado<<gray>>:')
            index = index if index.startswith(prefix) else prefix + index
            self.index = IndexPage(Site('wikisource:es'), index)

        index_gen = self.index.page_gen(
            only_existing=False, start=start, end=end)

        super().__init__(generator=index_gen, **kwargs)

    def get_pagelist(self):
        return self.pl

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page

        header = page.body[:100]

        label = ''
        if len(page.body) < 4:
            label = '-'
            self.pl.append(label)
            return

        nums = re.findall(r"(?:^|[\s—-])(\d+)[\s—-]", header)
        for num in nums:
            n = int(num)
            if n < self.last_num or n > self.last_num + 20:
                continue
            self.last_num = n
            label = str(n)
        if not label.isdigit():
            label = '—'
        self.pl.append(label)


def main(*args: str):
    args = pywikibot.handle_args(args)
    options = {}
    for arg in args[:]:
        if arg.startswith('-'):
            opt, sep, value = arg.partition(':')
            if opt in ('-summary', '-head', '-method'):
                options[opt[1:]] = value
            elif opt in ('-tipo', '-start', '-end'):
                options[opt[1:]] = int(value)
            else:
                options[opt[1:]] = True
            args.remove(arg)

    if len(args) == 0:
        index = pywikibot.input('Ingresa el Índice, con o sin el prefijo')
    else:
        index = args[0]

    options['start'] = options.get('start', 1)
    options['end'] = options.get('end', None)

    PLBOT = PagelistBot(index, **options)
    PLBOT.run()

    pagelist = PLBOT.get_pagelist().to_pagelist_tag(0)

    print(pagelist)


if __name__ == '__main__':
    main()
