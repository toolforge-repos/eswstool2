import os
from pywikibot.specialbots import UploadRobot
from pywikibot import Site
Commons = Site('commons', 'commons')

filename = 'Test2.pdf'
filepath = os.path.join(os.getcwd() + '\\PDF', filename)

bot = UploadRobot(filepath, description=' ', use_filename=filename,
                  chunk_size=1000000,
                  target_site=Commons)


def _(old, new):
    bot.last_filename = new


bot.post_processor = _

bot.run()

print(bot.last_filename)
