import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot import Page, Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot


class MyBot(CurrentPageBot):

    update_options = {
        'always': False,
        'summary': 'Semi-automático: etimología',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        ETIM = r"{{etimología2\s*(\|(?:\s*leng\s*=\s*\w+\s*)?)\|"\
               r"((?>[^{}]|{{[^}]*?}})*?)}}"
        PALABRA = r"['\[]*([“”«»‘’\"a-zA-ZÀ-ž\*%-=]*?)['\]]*"
        PREFIJO = r"['\[]*(['“”«»‘’\"\{\}\|a-zA-ZÀ-ž\*%-=]+)\-['\]]*"
        SUFIJO = r"['\[]*\-(['“”«»‘’\"\{\}\|a-zA-ZÀ-ž\*%-=]+)['\]]*"

        SUF1 = fr"^ *[dD]e *{PALABRA} *,? *y *el *sufijo *{PALABRA} *$"
        SUF2 = fr"^ *[dD]e *{PALABRA} *\+? *{SUFIJO} *$"

        page = self.current_page
        text = page.text
        if m := re.search(ETIM, page.text):
            m1 = m[1]
            n = re.search(SUF1, m[2])
            if n:
                n1 = n[1]
                n2 = n[2][0].replace('-', '') + n[2][1:]
                print(n1, n2)
                text = re.sub(ETIM, r'{{etimología'+m1+r'|sufijo|'+n1+'|'+n2+r'}}', page.text)

        self.put_current(text, summary=self.opt.summary)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    # args += ('-commandline:argument',)
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('es', fam="wiktionary")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
