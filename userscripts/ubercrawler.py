import pywikibot
import re
from pywikibot.proofreadpage import IndexPage
from pywikibot.bot import ExistingPageBot
import crawler


Commons = pywikibot.Site('commons', 'commons')
Wikisource = pywikibot.Site('es', fam='wikisource')


class CrawlBot(ExistingPageBot):
    update_options = {
        'always': False,
        'summary': 'Bot - Enlazando índices subidos',
    }

    MODULE = NotImplemented
    BLACKLIST = {}
    IDBLACKLIST = {}
    REXP = NotImplemented
    FULLNAME = NotImplemented

    def __init__(self, **kwargs):
        self.REXP = self.MODULE.REXP
        self.BLACKLIST = self.MODULE.BLACKLIST
        self.IDBLACKLIST = self.MODULE.IDBLACKLIST
        self.FULLNAME = self.MODULE.FULLNAME
        super().__init__(**kwargs)

    def search(self, query):
        search = Commons.search(query, namespaces='File', total=2)
        for result in search:
            title = result.title(with_ns=False)
            if title.split('.')[-1] not in ["pdf", "djvu"]:
                continue

            pywikibot.output(
                f'Al parecer ya está subido en Commons: <<lightred>>{title}')
            choice = pywikibot.input_choice(
                " Saltar?", (('Sí', 'S'), ('No', 'N'), ('Usar este', 'U')))
            if choice == 's':
                return 1
            elif choice == 'u':
                return result.title(with_ns=False)
            return

    def treat_page(self):
        page = self.current_page
        try:
            if page.title() in self.BLACKLIST:
                return
            text = page.text

            results = re.findall(self.REXP, text, flags=re.I)
            for r in results:
                filename = None
                bid = r[0]
                NEWXP = self.REXP.replace('(.+?)', bid)

                pywikibot.output(f'<<gray>>{bid}')
                if bid in self.IDBLACKLIST:
                    continue

                filename = self.search(bid)
                if filename == 1:
                    continue
                elif filename:
                    text = re.sub(NEWXP, r'{{at|' + filename + r[1] + r'}}',
                                  text, flags=re.I)
                else:
                    try:
                        item = self.MODULE(bid)
                        pages = item.do()
                        if not pages:
                            continue
                        if None in pages:
                            continue

                        for p in pages:
                            P = crawler.crawl(p, bot=True)
                            if len(P.data) > 3:
                                filename = P.filename.split(':')[-1]
                                index = IndexPage(
                                    Wikisource, 'Índice:' + filename)
                                olditext = index.text
                                index.text = P.to_wikisource()
                                if olditext != index.text:
                                    print(index.text)
                                    index.save(
                                        f'Bot - Creando índices a partir de {self.FULLNAME}')

                        if len(pages) == 1:
                            at_text = rf'{{{{at|{pages[0].filename}{r[1]}}}}}'
                        else:
                            lista = [rf'{{{{at|{page.filename}|Volumen {i+1}}}}}' for i,
                                     page in enumerate(pages)]
                            at_text = ', '.join(lista)

                        text = re.sub(NEWXP, at_text, text, flags=re.I)

                    except KeyboardInterrupt:
                        continue
            if page.text != text:
                self.put_current(text, summary=self.opt.summary)

        except KeyboardInterrupt:
            return
