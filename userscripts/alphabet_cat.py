import pywikibot
import re
from pywikibot import pagegenerators
from pywikibot import Page, Site
from pywikibot.bot import ExistingPageBot  # ExistingPageBot #CurrentPageBot
import time


class MyBot(ExistingPageBot):

    update_options = {
        'always': False,
        'summary': 'Categorizing.', }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text
        X = re.search(r'(.) with people as an initial', page.title())[1]
        
        text = '{{LatinLetter|prefix=|suffix=with people as an initial}}\n' + \
            page.text + \
            f'\n[[Category:Latin letters with people as an initial|{X}]]'
        self.put_current(text, summary=self.opt.summary)
        time.sleep(30)


def main(*args: str) -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    # args += ('-commandline:argument',)
    options = {}
    args = pywikibot.handle_args(args)
    site = Site('commons', "commons")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    args = gen_factory.handle_args(args)
    for arg in args:
        opt, sep, value = arg.partition(':')
        if opt in ('-summary', '-text', '-custom'):
            options[opt[1:]] = value
        else:
            options[opt] = True

    ALPHABET = 'ABCEFGHIJKLMNOPQRSTUVWXYZ'
    CATS = [f'Category:{x} with people as an initial' for x in ALPHABET]

    gen = [Page(site, cat) for cat in CATS]

    MyBot(generator=gen, **options).run()  # guess what it does


if __name__ == '__main__':
    main()
