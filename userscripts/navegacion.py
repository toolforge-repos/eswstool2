import pywikibot, re, sys

site=pywikibot.Site('es', fam="wikisource")
site.login()

def process_list(text):
    fat = {}
    try:
        for n, line in enumerate(text.splitlines()):
            if line:
                if line[0] == '#':
                    big = line[1:].strip("[]{}* ")
                else:
                    big = line.strip("[]{}* ")

                splito = big.split('|')
                fat[n] = splito
        return fat
    except:
        raise Exception('Lista mal formateada')

def main(*args: str):

    for arg in sys.argv[1:]:    
        if arg[:5] == '-file':
            filename = arg[6:].strip('"')
    try:
        in_file = open(filename, 'r', encoding='utf8')
    except:
        raise Exception('Se necesita una lista formateada (-file:)')
    
    FILE = in_file.read()
    fat = process_list(FILE)
    pywikibot.output("Lista inicial procesada")
    processed = {}

    regexp = r"({{[Ee]ncabe(?:{{[^}]*?}}|[^}])*?)}}"

    for n, target in enumerate(fat):
        prev = ''
        sig=''
        if n != 0:
            prev = '[[{}{}]]'.format(fat[n-1][0],  '|'+fat[n-1][1] if len(fat[n-1])>1 else '')
        # else:
        #     prev = '[[{}|Portada]]'.format(big)
        
        actual = '[[{}{}]]'.format(fat[n][0],  '|'+fat[n][1] if len(fat[n])>1 else '') 
        
        if n < len(fat)-1:
            sig = '[[{}{}]]'.format(fat[n+1][0],  '|'+fat[n+1][1] if len(fat[n+1])>1 else '')
        T = pywikibot.Page(site, title=fat[n][0], ns=0)
        M = re.search(regexp, T.text, flags=re.S)
        if M:
            T.text = T.text[:M.end(1)]+'\n|anterior = {}\n|sección = {}\n|próximo = {}'.format(prev,actual,sig)+T.text[M.end(1):]            
            T.save('Añadiendo enlaces de navegación')
        else:
            pywikibot.output("ATENTO: {}".format(actual))

if __name__ == '__main__':
    main()


