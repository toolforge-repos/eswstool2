import pywikibot
from pywikibot import pagegenerators
from pywikibot import Site
from pywikibot.bot import CurrentPageBot  # ExistingPageBot #CurrentPageBot
import utils.template_utils as t_u
import UTILS


class MyBot(CurrentPageBot):

    def __init__(self, **kwargs):
        self.site = pywikibot.Site('wikidata', 'wikidata')
        self.repo = self.site.data_repository()
        self.IDs = {}
        super().__init__(**kwargs)

    update_options = {
        'always': True,
        'summary': 'Bot: agregando fecha de ultima-muerte',
    }

    def treat_page(self):
        """Load the given page, do some changes, and save it."""
        page = self.current_page
        text = page.text

        ultimamuerte = t_u.get_index_data(text, 'ultima-muerte') or 0

        if ultimamuerte:
            print('ya tiene ultima-muerte')
            return

        autores = []
        roles = ['Autor', 'Traductor', 'Editor', 'Ilustrador', 'Prologuista']

        for role in roles:
            r = t_u.get_index_data(text, role)
            if r:
                autores += [x.strip() for x in r.split(',')]

        try:
            for autor in autores:
                if autor:
                    QID = self.IDs.get(autor) or UTILS.search_ID(autor)
                    self.IDs[autor] = QID or 'X'
                    if QID and QID.startswith('Q'):
                        item = pywikibot.ItemPage(self.repo, QID)
                        if 'P570' in item.claims:
                            for c in item.claims['P570']:
                                if c.target and ultimamuerte < c.target.year:
                                    ultimamuerte = c.target.year
                        else:
                            print('no P570')
                            ultimamuerte = 0
                            break
                    else:
                        ultimamuerte = 0
                        break
            if ultimamuerte:
                text = t_u.set_index_data(
                    text, 'ultima-muerte', ultimamuerte, create=True)
                self.put_current(text, summary=self.opt.summary)
        except Exception:
            return


def main() -> None:
    """
    GRAN BLOQUE QUE MANEJA TODO, NO TOCAR
    """
    site = Site('es', fam="wikisource")
    gen_factory = pagegenerators.GeneratorFactory(site=site)
    gen_factory.handle_arg('-namespace:104')
    gen_factory.handle_arg(
        '-start:Índice:Biblioteca de Autores Españoles (Vol. 66).djvu')

    gen = gen_factory.getCombinedGenerator(preload=True)

    MyBot(generator=gen).run()  # guess what it does


if __name__ == '__main__':
    main()
